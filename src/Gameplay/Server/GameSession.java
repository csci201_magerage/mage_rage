package Gameplay.Server;

import java.util.Random;
import java.util.Stack;
import java.util.Vector;

import Gameplay.Cards.*;
import Networking.Server.ClientThread;

public class GameSession extends Thread {
	
	Vector<Player> Players;
	
	Vector<Card> Phase1SortedCards;
	Vector<Card> Phase2SortedCards;
	Vector<Card> Phase3SortedCards;
	
	Stack<SpellCard> drawPile;
	SpellCard[] discardPile;
	int discardCounter;
	
	int PlayersAlive;
	int turnCounter;
	
	public String messageFromClient;
	
	
	public GameSession(Vector<ClientThread> allUsers) //constructor
	{
		drawPile = new Stack<SpellCard>();
		PlayersAlive = 0;
		messageFromClient = "";
		discardCounter = 0;
		
		Phase1SortedCards = new Vector<Card>();
		Phase2SortedCards = new Vector<Card>();
		Phase3SortedCards = new Vector<Card>();
		
		//populating the discardPile with Spell Cards
		discardPile = new SpellCard[42];
		discardPile[0] = new Mist();
		discardPile[1] = new Mist();
		discardPile[2] = new MageRage();
		for (int i=3; i<8; i++)
		{
			discardPile[i] = new Heal();
		}
		discardPile[8] = new Teleport();
		discardPile[9] = new Teleport();
		for (int i=10; i<13; i++)
		{
			discardPile[i] = new Roots();
		}
		for (int i=13; i<17; i++)
		{
			discardPile[i] = new Gust();
		}
		for (int i=17; i<20; i++)
		{
			discardPile[i] = new Earthquake();
		}
		for (int i=20; i<24; i++)
		{
			discardPile[i] = new WaterJet();
		}
		for (int i=24; i<28; i++)
		{
			discardPile[i] = new Tornado();
		}
		for (int i=28; i<31; i++)
		{
			discardPile[i] = new FireBreath();
		}
		for (int i=31; i<34; i++)
		{
			discardPile[i] = new Lightning();
		}
		for (int i=34; i<37; i++)
		{
			discardPile[i] = new IceSpike();
		}
		discardPile[37] = new Comet();
		discardPile[38] = new Comet();
		for (int i=39; i<42; i++)
		{
			discardPile[i] = new Hailstorm();
		}
		//discardPile[43] = new Storm();
		//discardPile[44] = new Storm();
		
		//initializing Players
		Players = new Vector<Player>();
		
		for (int i=0; i<allUsers.size(); i++)
		{
			Player temp = new Player(allUsers.get(i));
			temp.setIndex(i);
			temp.clientThread.sendGameInfoToPlayer("receive_server_index:" + i);
			Players.add(temp);
			PlayersAlive++;
			//if allUsers.get(i) isGuest, send message to that client saying client is guest
			//when a player is created, send the index to client	
		}
		
		restockDrawPile();
		
		System.out.println("Players created: " + Players.size());
		
		//send user name to all players based on server index
		for (int i=0;i<allUsers.size();i++){
			for (int m=0;m<allUsers.size();m++){
				if (i!=m){
					Players.get(m).clientThread.sendGameInfoToPlayer("show_user_name:" + i + ":" + Players.get(i).clientThread.getUsername());
				}
			}
		}
		
		turnCounter = 0;
		
		start();	
	}
	
	public void removePlayer(int index)
	{
		Players.get(index).setIsDead();
		Players.remove(index);
		int temp = 0;
		for (int i=0; i<Players.size(); i++)
		{
			if (Players.get(i).getIsDead() == false)
			{
				temp++;
			}
		}
		PlayersAlive = temp;
	}
	
	public void restockDrawPile()
	{
		for(int i = 0; i < 42; i++) {
			Random random = new Random();
			int index = random.nextInt(42);
			SpellCard temp = discardPile[i];
			discardPile[i] = discardPile[index];
			discardPile[index] = temp;
		}
		
		for (int i=0; i < 42; i++)
		{
			drawPile.push(discardPile[i]);
			discardPile[i] = null;
		}
		
		System.out.println("Shuffled and Restocked drawPile");
	}
	
	public String receiveMessage()
	{
		messageFromClient = "";
		while (messageFromClient.equals(""))
		{
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		return messageFromClient;
	}
	
	public void sortActions(Vector<Card> phaseActions)
	{	
		Card temp;
		for (int i=0; i<PlayersAlive; i++)
		{
			for (int j=1; j<PlayersAlive; j++)
			{
				if (phaseActions.get(j-1).getPriority() > phaseActions.get(j).getPriority())
				{
					temp = phaseActions.get(j-1);
					phaseActions.set(j-1, phaseActions.get(j));
					phaseActions.set(j, temp);
				}
			}
		}
	}
	
	public void run()
	{
		System.out.println("Run method started");
		
		while (PlayersAlive > 1)
		{
			//Turn Start
			turnCounter++;
			
			//tell the clients what turn it is
			for (int i=0; i<Players.size(); i++)
			{
				Players.get(i).clientThread.sendGameInfoToPlayer("update_turn:" + turnCounter);
			}
			
			//preparations at turn start
			//tell clients to reset decision cards
			for (int i=0; i<Players.size(); i++)
			{
				if (Players.get(i).getIsDead() == false)
				{
					Players.get(i).clientThread.sendGameInfoToPlayer("reset_decision_cards");
				}
			}
			
			//deal Spell Cards
			for (int i=0; i<Players.size(); i++)
			{
				if (Players.get(i).getIsDead() == false)
				{
					int counter = Players.get(i).getSpellCardHand().size();
					while(counter<5)
					{
						SpellCard foo = drawPile.pop();
						Players.get(i).dealSpellCard(foo);
						System.out.println(i + ": " + foo.toString());
						Players.get(i).clientThread.sendGameInfoToPlayer("receive_spell_card:"+foo.cardMessage());
						counter++;
					}
				}
			}
			
			//tell each client to choose Spell Cards
			for (int i=0; i<Players.size(); i++)
			{
				if (Players.get(i).getIsDead() == false)
				{
					Players.get(i).clientThread.sendGameInfoToPlayer("choose_spell_cards");
				}
			}
			System.out.println("Passed choose_spell_cards");
			
			//wait for all the choices to come in
			int PlayersReady = 0;
			while (PlayersReady < PlayersAlive)
			{
				String temp = receiveMessage();
				String [] components = temp.split(":");
				int index = Integer.parseInt(components[0]);
				
				//transferring Players[index]'s SpellCards from SpellCardHand to SpellCardsChosen
				
				for (int i=0;i<5;i++)
				{
					if (Players.get(index).getSpellCardHand().get(i).toString().equals(components[1]))
					{
						Players.get(index).SpellCardsChosen.add(Players.get(index).getSpellCardHand().get(i));
						Players.get(index).removeFromHand(Players.get(index).getSpellCardHand().get(i));
						System.out.println("Found card 1");
						break;
					}
				}
	
				for (int i=0; i<4; i++)
				{
					if (Players.get(index).getSpellCardHand().get(i).toString().equals(components[2]))
					{
						Players.get(index).SpellCardsChosen.add(Players.get(index).getSpellCardHand().get(i));
						Players.get(index).removeFromHand(Players.get(index).getSpellCardHand().get(i));
						System.out.println("Found card 2");
						break;
					}
				}
				for (int i=0; i<3; i++)
				{
					if (Players.get(index).getSpellCardHand().get(i).toString().equals(components[3]))
					{
						Players.get(index).SpellCardsChosen.add(Players.get(index).getSpellCardHand().get(i));
						Players.get(index).removeFromHand(Players.get(index).getSpellCardHand().get(i));
						System.out.println("Found card 3");
						break;
					}
				}
				PlayersReady++;
				System.out.println(PlayersReady);
			}
			System.out.println("Broke out of PlayersReady < PlayersAlive loop");
			
			//send each player all Spell Cards chosen
			for (int i=0; i<Players.size(); i++)
			{
				if (Players.get(i).getIsDead() == false)
				{
					for (int j=0; j<Players.size(); j++)
					{
						if (Players.get(j).getIsDead() == false)
						{
							//send Players[j]'s Spell Card choices to Players[i]
							Players.get(i).clientThread.sendGameInfoToPlayer("show_spell_cards:" + j + ":" + 
							Players.get(j).SpellCardsChosen.get(0).toString() + ":" + Players.get(j).SpellCardsChosen.get(1).toString() + ":" +
							Players.get(j).SpellCardsChosen.get(2).toString());
						}
					}
				}
			}
			
			//tell clients to choose Decision Cards
			for (int i=0; i<Players.size(); i++)
			{
				if (Players.get(i).getIsDead() == false)
				{
					Players.get(i).clientThread.sendGameInfoToPlayer("choose_decision_cards");
				}
			}
			System.out.println("Passed choose_decision_cards");
			
			//store Decisions
			//wait for all the choices to come in
			PlayersReady = 0;
			while (PlayersReady < PlayersAlive)
			{
				String temp = receiveMessage();
				String [] components = temp.split(":");
				int index = Integer.parseInt(components[0]);
				System.out.println(Players.get(index).SpellCardsChosen.size());
				//Processing Phase 1 action
				if (components[1].equals("movement_fast"))
				{
					//add a Movement Fast to Phase1actions
					Card action = new MovementFast();
					Phase1SortedCards.add(action);
					Players.get(index).DecisionCardsChosen.add(components[1]);
				}
				else if (components[1].equals("movement_slow"))
				{
					//add a Movement Slow to Phase1actions
					Card action = new MovementSlow();
					Phase1SortedCards.add(action);
					Players.get(index).DecisionCardsChosen.add(components[1]);
				}
				else if (components[1].equals("spell_1"))
				{
					//add Players[index].SpellCardChosen.get(0) to Phase1actions
					Phase1SortedCards.add(Players.get(index).SpellCardsChosen.get(0));
					Players.get(index).DecisionCardsChosen.add(Players.get(index).SpellCardsChosen.get(0).toString());
				}
				else if (components[1].equals("spell_2"))
				{
					//add Players[index].SpellCardChosen.get(1) to Phase1actions
					Phase1SortedCards.add(Players.get(index).SpellCardsChosen.get(1));
					Players.get(index).DecisionCardsChosen.add(Players.get(index).SpellCardsChosen.get(1).toString());
				}
				else if (components[1].equals("spell_3"))
				{
					//add Players[index].SpellCardChosen.get(2) to Phase1actions
					Phase1SortedCards.add(Players.get(index).SpellCardsChosen.get(2));
					Players.get(index).DecisionCardsChosen.add(Players.get(index).SpellCardsChosen.get(2).toString());
				}
				System.out.println(Players.get(index).SpellCardsChosen.size());
				
				//Processing Phase 2 action
				if (components[3].equals("movement_fast"))
				{
					//add a Movement Fast to Phase1actions
					Card action = new MovementFast();
					Phase2SortedCards.add(action);
					Players.get(index).DecisionCardsChosen.add(components[3]);
				}
				else if (components[3].equals("movement_slow"))
				{
					//add a Movement Slow to Phase1actions
					Card action = new MovementSlow();
					Phase2SortedCards.add(action);
					Players.get(index).DecisionCardsChosen.add(components[3]);
				}
				else if (components[3].equals("spell_1"))
				{
					//add Players[index].SpellCardChosen.get(0) to Phase1actions
					Phase2SortedCards.add(Players.get(index).SpellCardsChosen.get(0));
					Players.get(index).DecisionCardsChosen.add(Players.get(index).SpellCardsChosen.get(0).toString());
				}
				else if (components[3].equals("spell_2"))
				{
					//add Players[index].SpellCardChosen.get(1) to Phase1actions
					Phase2SortedCards.add(Players.get(index).SpellCardsChosen.get(1));
					Players.get(index).DecisionCardsChosen.add(Players.get(index).SpellCardsChosen.get(1).toString());
				}
				else if (components[3].equals("spell_3"))
				{
					//add Players[index].SpellCardChosen.get(2) to Phase1actions
					Phase2SortedCards.add(Players.get(index).SpellCardsChosen.get(2));
					Players.get(index).DecisionCardsChosen.add(Players.get(index).SpellCardsChosen.get(2).toString());
				}
				System.out.println(Players.get(index).SpellCardsChosen.size());
				
				//Processing Phase 3 action
				if (components[5].equals("movement_fast"))
				{
					//add a Movement Fast to Phase1actions
					Card action = new MovementFast();
					Phase3SortedCards.add(action);
					Players.get(index).DecisionCardsChosen.add(components[5]);
				}
				else if (components[5].equals("movement_slow"))
				{
					//add a Movement Slow to Phase1actions
					Card action = new MovementSlow();
					Phase3SortedCards.add(action);
					Players.get(index).DecisionCardsChosen.add(components[5]);
				}
				else if (components[5].equals("spell_1"))
				{
					//add Players[index].SpellCardChosen.get(0) to Phase1actions
					Phase3SortedCards.add(Players.get(index).SpellCardsChosen.get(0));
					Players.get(index).DecisionCardsChosen.add(Players.get(index).SpellCardsChosen.get(0).toString());
				}
				else if (components[5].equals("spell_2"))
				{
					//add Players[index].SpellCardChosen.get(1) to Phase1actions
					Phase3SortedCards.add(Players.get(index).SpellCardsChosen.get(1));
					Players.get(index).DecisionCardsChosen.add(Players.get(index).SpellCardsChosen.get(1).toString());
				}
				else if (components[5].equals("spell_3"))
				{
					//add Players[index].SpellCardChosen.get(2) to Phase1actions
					Phase3SortedCards.add(Players.get(index).SpellCardsChosen.get(2));
					Players.get(index).DecisionCardsChosen.add(Players.get(index).SpellCardsChosen.get(2).toString());
				}
				System.out.println(Players.get(index).SpellCardsChosen.size());
				
				PlayersReady++;
				System.out.println("Players ready: " + PlayersReady);
			}
			System.out.println("Decisions are all in");
			
			//PHASE 1 STARTS HERE
			
			//sort Phase 1 Decisions
			sortActions(Phase1SortedCards);
			
			//output highest priority of first card
			for (int i=0; i<PlayersAlive; i++)
			{
				System.out.println(Phase1SortedCards.get(i).getPriority());
			}
			
			System.out.println("Passed sorting Phase 1");
			
			//show all players' Phase 1 Decision Cards
			for (int i=0; i<Players.size(); i++)
			{
				Players.get(i).clientThread.sendGameInfoToPlayer("phase1");
				if (Players.get(i).getIsDead() == false)
				{
					for (int j=0; j<Players.size(); j++)
					{
						if (Players.get(j).getIsDead() == false)
						{
							Players.get(i).clientThread.sendGameInfoToPlayer("show_decision_card:" + j + ":" + Players.get(j).DecisionCardsChosen.get(0));
						}
					}
				}
			}
			
			//call each action in priority order
			for (int i=0; i<Phase1SortedCards.size(); i++)
			{
				//send message to owner to prompt user input
				Players.get(Phase1SortedCards.get(i).owner).clientThread.sendGameInfoToPlayer(Phase1SortedCards.get(i).toString());
				//execute action
				Phase1SortedCards.get(i).action(receiveMessage(), Players);
				System.out.println(Phase1SortedCards.get(i).toString() + " executed");
				//tell clients to update what changed; happens inside action()
			}
			
			//unmist everyone
			for (int i=0; i<Players.size(); i++)
			{
				Players.get(i).setIsMist(false);
			}
			
			//PHASE 2 STARTS HERE
			
			//sort Phase 2 Decisions
			sortActions(Phase2SortedCards);
			
			//output highest priority of second card
			for (int i=0; i<PlayersAlive; i++)
			{
				System.out.println(Phase2SortedCards.get(i).getPriority());
			}
			
			System.out.println("Passed sorting Phase 2");
			
			
			//show all players' Phase 2 Decision Cards
			for (int i=0; i<Players.size(); i++)
			{
				Players.get(i).clientThread.sendGameInfoToPlayer("phase2");
				if (Players.get(i).getIsDead() == false)
				{
					for (int j=0; j<Players.size(); j++)
					{
						if (Players.get(j).getIsDead() == false)
						{
							Players.get(i).clientThread.sendGameInfoToPlayer("show_decision_card:" + j + ":" + Players.get(j).DecisionCardsChosen.get(1));
						}
					}
				}
			}
			
			//call each action in order
			for (int i=0; i<Phase2SortedCards.size(); i++)
			{
				//send message to owner to prompt user input
				Players.get(Phase2SortedCards.get(i).owner).clientThread.sendGameInfoToPlayer(Phase2SortedCards.get(i).toString());
				//execute action
				Phase2SortedCards.get(i).action(receiveMessage(), Players);
				System.out.println(Phase2SortedCards.get(i).toString() + " executed");
				//tell clients to update what changed; happens inside action()
			}
			
			//unmist everyone
			for (int i=0; i<Players.size(); i++)
			{
				Players.get(i).setIsMist(false);
			}
			
			//PHASE 3 STARTS HERE
			
			//sort Phase 3 Decisions
			sortActions(Phase3SortedCards);
			
			//output highest priority of third card
			for (int i=0; i<PlayersAlive; i++)
			{
				System.out.println(Phase3SortedCards.get(i).getPriority());
			}
			
			System.out.println("Passed sorting Phase 3");
			
			//show all players' Phase 3 Decision Cards
			for (int i=0; i<Players.size(); i++)
			{
				Players.get(i).clientThread.sendGameInfoToPlayer("phase3");
				if (Players.get(i).getIsDead() == false)
				{
					for (int j=0; j<Players.size(); j++)
					{
						if (Players.get(j).getIsDead() == false)
						{
							Players.get(i).clientThread.sendGameInfoToPlayer("show_decision_card:" + j + ":" + Players.get(j).DecisionCardsChosen.get(2));
						}
					}
				}
			}
			
			//call each action in order
			for (int i=0; i<Phase3SortedCards.size(); i++)
			{
				//send message to owner to prompt user input
				Players.get(Phase3SortedCards.get(i).owner).clientThread.sendGameInfoToPlayer(Phase3SortedCards.get(i).toString());
				//execute action
				Phase3SortedCards.get(i).action(receiveMessage(), Players);
				System.out.println(Phase3SortedCards.get(i).toString() + " executed");
				//tell clients to update what changed; happens inside action()
			}
					
			//unmist everyone
			for (int i=0; i<Players.size(); i++)
			{
				Players.get(i).setIsMist(false);
			}
			
			//unroot everyone
			for (int i=0; i<Players.size(); i++)
			{
				Players.get(i).setIsRooted(false);
			}
			
			discardCards();
			
			//If any players are guests, remove them
			for (int i=0; i<Players.size(); i++)
			{
				if (Players.get(i).isGuest())
				{
					//remove the player
					Players.get(i).clientThread.sendGameInfoToPlayer("trial_ended" + i);
					removePlayer(i);
				}
			}
			
			//End of turn, loop back to beginning
		}
	} //end of run method	
	
	public void discardCards() {
		for(int i = 0; i < Phase1SortedCards.size(); i++) {
			if(Phase1SortedCards.get(i) instanceof SpellCard) {
				discardPile[discardCounter] = (SpellCard)Phase1SortedCards.get(i);
				Phase1SortedCards.remove(i);
				discardCounter++;
			}
		}
		
		for(int i = 0; i < Phase2SortedCards.size(); i++) {
			if(Phase2SortedCards.get(i) instanceof SpellCard) {
				discardPile[discardCounter] = (SpellCard)Phase2SortedCards.get(i);
				Phase2SortedCards.remove(i);
				discardCounter++;
			}
		}
		
		for(int i = 0; i < Phase3SortedCards.size(); i++) {
			if(Phase3SortedCards.get(i) instanceof SpellCard) {
				discardPile[discardCounter] = (SpellCard)Phase3SortedCards.get(i);
				Phase3SortedCards.remove(i);
				discardCounter++;
			}
		}
		
		for(int i = 0; i < Players.size(); i++) {
			for(int k = 0; k < Players.get(i).getSpellCardHand().size(); k++) {
				discardPile[discardCounter] = Players.get(i).getSpellCardHand().get(k);
				Players.get(i).getSpellCardHand().remove(k);
				discardCounter++;
			}
			
			for(int k = 0; k < Players.get(i).SpellCardsChosen.size(); k++) {
				discardPile[discardCounter] = Players.get(i).SpellCardsChosen.get(k);
				Players.get(i).SpellCardsChosen.remove(k);
				discardCounter++;
			}
		}
	}
	
	/*
	private ArrayDeque<SpellCard> drawDeck;
	private ArrayDeque<SpellCard> usedCards;
	private SpellCard [] Cards;
	private Vector<Player> players;
	private int playersAlive;
	
	private Card[] phase1actions = new Card[6];
	private Card[] phase2actions = new Card[6];
	private Card[] phase3actions = new Card[6];
	private Card[] buffer = new Card[6];
	private int phase1counter;
	private int phase2counter;
	private int phase3counter;

	private String reader;
	
	public GameSession(Vector<ClientThread> allUsers)
	{
		
		Cards = new SpellCard[44];
		//populating the card deck, here we go...
		Cards[0] = new Mist();
		Cards[1] = new Mist();
		Cards[2] = new MageRage();
		for (int i=3; i<8; i++)
		{
			Cards[i] = new Heal();
		}
		Cards[8] = new Teleport();
		Cards[9] = new Teleport();
		for (int i=10; i<13; i++)
		{
			Cards[i] = new Roots();
		}
		Cards[13] = new Storm();
		Cards[14] = new Storm();
		for (int i=15; i<19; i++)
		{
			Cards[i] = new Gust();
		}
		for (int i=19; i<22; i++)
		{
			Cards[i] = new Earthquake();
		}
		for (int i=22; i<26; i++)
		{
			Cards[i] = new WaterJet();
		}
		for (int i=26; i<30; i++)
		{
			Cards[i] = new Tornado();
		}
		for (int i=30; i<33; i++)
		{
			Cards[i] = new FireBreath();
		}
		for (int i=33; i<36; i++)
		{
			Cards[i] = new Lightning();
		}
		for (int i=36; i<39; i++)
		{
			Cards[i] = new IceSpike();
		}
		Cards[39] = new Comet();
		Cards[40] = new Comet();
		for (int i=41; i<44; i++)
		{
			Cards[i] = new Hailstorm();
		}
		//finished populating the card deck
		
		reader = "";
		
		drawDeck = new ArrayDeque<SpellCard>(44);
		usedCards = new ArrayDeque<SpellCard>(44);
		
		//populates ArrayList<Player> players
		playersAlive = 0;
		
		players = new Vector<Player>();
		
		for (int i=0; i<allUsers.size(); i++)
		{
			Player temp = new Player(allUsers.get(i));
			temp.setIndex(i);
			temp.clientThread.sendGameInfoToPlayer("receive_server_index:" + i);
			players.add(temp);
			playersAlive++;
			//when a player is created, send the index to client	
		}
		
		System.out.println("Players created: " + players.size() );
		
		//send username to all players based on server index
		for (int i=0;i<allUsers.size();i++){
			for (int m=0;m<allUsers.size();m++){
				if (i!=m){
					players.get(m).clientThread.sendGameInfoToPlayer("show_user_name:" + i + ":" + players.get(i).clientThread.getUsername());
				}
			}
		}
				
		shuffleCards(); //shuffle the Cards array
		for (int i=0; i<44; i++)
		{
			
			drawDeck.push(Cards[i]); //put everything in the Cards array into the draw deck
			if (drawDeck.isEmpty())
			{
				System.out.println("ERROR: drawDeck is empty");
			}
		}
		System.out.println(reader);
		this.start(); //starting game
		
	}
	
	private void shuffleCards()
	{
		for (int i=43; i>-1; i--)
		{
			Random rand = new Random();
			int random = rand.nextInt(i + 1);
			SpellCard temp = Cards[i];
			Cards[i] = Cards[random];
			Cards[random] = temp;
		}
	}
	
	private void stockDrawDeck()
	{
		for (int i=0; i<44; i++)
		{
			Cards[i] = usedCards.pop();
		}
		
		shuffleCards();
		
		for (int i=0; i<44; i++)
		{
			drawDeck.push(Cards[i]);
		}
	}
	
	public void sortPhaseActions(Card[] phase)
	{
		PriorityQueue<Card> pq = new PriorityQueue<Card>(6, );
	}
	
	public void discardCard(SpellCard usedCard)
	{
		usedCards.push(usedCard);
	}
	
	public void updatePlayersAlive()
	{
		for (int i=0; i<players.size(); i++)
		{
			if (players.get(i).getIsDead())
			{
				playersAlive--;
			}
		}
	}
	
	private String receiveMessage()
	{
		
		while (reader.equals(""))
		{
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		String temp = reader;
		reader = "";
		System.out.println("Message received: " + temp);
		return temp;
	}
	
	public void setReader(String input)
	{
		this.reader = input;
		System.out.println(this.reader);
	}
	
	public void run() {
		System.out.println("Run method started");
		System.out.println(reader);
		//Pseudocode
		/*
		 while(at least 2 players alive)
			 Turn Start:
			 Give each player five Spell Cards
			 Server tells clients to choose spell cards
			 Clients send choices to server
			 When all clients are done, server tells clients all the spell cards
			   of each player
			 Clients send actions to server
			 When all clients are done, server starts processing
			 Server sorts events so that they are in order from first to last
			 Server executes events
			 Phase 1
			 Server tells client to prompt user for input
			 Once server gets input, continue with action
			 Move card to usedCards
			 Repeat for Phases 2, 3
			 
		*/
		
		/*
		for (int i=0; i<players.size(); i++)
		{
			players.get(i).clientThread.sendGameInfoToPlayer("test");
		}
		ArrayList<String> inputs = new ArrayList<String>(4);
		while (inputs.size() < players.size())
		{
			String test = receiveMessage();
			String [] parse = test.split(":");
			int integer = Integer.parseInt(parse[0]);
			inputs.set(integer, parse[1]);
		}
		for (int i=0; i<inputs.size(); i++)
		{
			System.out.println(inputs.get(i));
		}
		
		
		while (playersAlive > 1) //game loop
		{
			
			//server side operation
			//deal until all players have five Spell Cards
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					int counter = players.get(i).getHandSize();
					while(counter<5)
					{
						SpellCard foo = drawDeck.pop();
						System.out.println(foo.cardMessage());
						players.get(i).dealSpellCard(foo);
						players.get(i).clientThread.sendGameInfoToPlayer("receive_spell_card:"+foo.cardMessage());
						counter++;
					}
				}
			}
			/*
			for (int i=0; i<5; i++)
			{
				System.out.println(players.get(0).getSpellCardHand().get(i).cardMessage());
			}
			for (int i=0; i<5; i++)
			{
				System.out.println(players.get(1).getSpellCardHand().get(i).cardMessage());
			}
			
			//tell players to choose their Spell Cards
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					players.get(i).clientThread.sendGameInfoToPlayer("choose_spell_cards");
				}
			}
			//System.out.println(reader);
			int playersReady = 0;
			//process Spell Card choices
			while(playersReady < playersAlive)
			{
				System.out.println("Inside the while(playersReady < playersAlive) loop");
				System.out.println(reader);
				String [] temp = receiveMessage().split(":");
				int index = Integer.parseInt(temp[0]);
				System.out.println("Passed the receiveMessage()");
				//do things with message
		
				for (int i=0; i<players.get(index).getHandSize(); i++)
				{
					for (int j=0;j<3;j++){
					if (players.get(index).getSpellCardHand().get(i).cardMessage().equals(temp[j]))
						{
							players.get(index).SpellCardsChosen.add(players.get(index).getSpellCardHand().get(i));
							players.get(index).removeFromHand(players.get(index).getSpellCardHand().get(i));
						break;
						}
					}
				}
				/*
				for (int i=0; i<4; i++)
				{
					if (players.get(index).getSpellCardHand().get(i).cardMessage().equals(temp[2]))
					{
						players.get(index).SpellCardsChosen.add(players.get(index).getSpellCardHand().get(i));
						players.get(index).removeFromHand(players.get(index).getSpellCardHand().get(i));
						
						break;
					}
				}
				for (int i=0; i<3; i++)
				{
					if (players.get(index).getSpellCardHand().get(i).cardMessage().equals(temp[3]))
					{
						players.get(index).SpellCardsChosen.add(players.get(index).getSpellCardHand().get(i));
						players.get(index).removeFromHand(players.get(index).getSpellCardHand().get(i));
						
						break;
					}
				}
				
				
				playersReady++;
			}
			
			System.out.println(players.get(0).SpellCardsChosen.get(0));
			System.out.println(players.get(0).SpellCardsChosen.get(1));
			System.out.println(players.get(0).SpellCardsChosen.get(2));
			System.out.println(players.get(1).SpellCardsChosen.get(0));
			System.out.println(players.get(1).SpellCardsChosen.get(1));
			System.out.println(players.get(1).SpellCardsChosen.get(2));
			
			
			System.out.println("ALl players ready");
			
			//reveal all Spell Card choices to all players
			for (int i=0; i<players.size(); i++)
			{
				System.out.println("player" + i);
				if (players.get(i).getIsDead() == false)
				{
					//send all Spell Card choices to players[i]
					for (int j=0; j<players.size(); j++)
					{
						System.out.println("j" + j);
						if (players.get(j).getIsDead() == false && i!=j)
						{
							
							System.out.println(players.get(j).SpellCardsChosen.get(0).cardMessage());
							System.out.println(players.get(j).SpellCardsChosen.get(1).cardMessage());
							System.out.println(players.get(j).SpellCardsChosen.get(2).cardMessage());
							
							
							players.get(i).clientThread.sendGameInfoToPlayer(
									"show_spell_cards:" + j + ":" +
									players.get(j).getSpellCardChosen(0).cardMessage() + ":" + 
									players.get(j).getSpellCardChosen(1).cardMessage() + ":" +
									players.get(j).getSpellCardChosen(2).cardMessage());								
						}
					}
				}
			}
			
			
			//tell players to choose their actions
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					players.get(i).clientThread.sendGameInfoToPlayer("choose_decision_cards");
				}
			}
			
			playersReady = 0;
			
			//process actions until server gets them all TODO
			while(playersReady != playersAlive)
			{
				receiveMessage();
				//do things with message
				
				playersReady++;
			}
			
			//Sort Phase 1 actions here
			
			
			//Phase 1
			//Reveal all Phase 1 decisions
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					for (int j=0; j<players.size(); j++)
					{
						if (players.get(j).getIsDead() == false)
						{
							players.get(i).clientThread.sendGameInfoToPlayer("show_decision_card:" + j + ":" 
									+ player j's decision card);
						}
					}
				}
			}
			//execute Phase 1
			for (int i=0; i<phase1counter; i++)
			{
				//phase1actions[i].action(input, players);
			}
			for (int i=0; i<players.size(); i++)
			{
				players.get(i).setIsMist(false); //after Phase 1, make sure no one is still Misted
			}
				 
				 
			//Sort Phase 2 actions here
			//Phase 2
				 
			//Reveal all Phase 2 decisions
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					for (int j=0; j<players.size(); j++)
					{
						if (players.get(j).getIsDead() == false)
						{
							players.get(i).clientThread.sendGameInfoToPlayer("show_decision_card:" + j + ":" 
									/*+ player j's decision card);
						}
					}
				}
			}
				 
			for (int i=0; i<phase2counter; i++)
			{
				//phase2actions[i].action(input, players);
			}
			for (int i=0; i<players.size(); i++)
			{
				players.get(i).setIsMist(false); //after Phase 2, make sure no one is still Misted
			}
			
			
			//Sort Phase 3 actions here TODO
			
			
			//Phase 3
			//Reveal all Phase 3 actions
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					for (int j=0; j<players.size(); j++)
					{
						if (players.get(j).getIsDead() == false)
						{
							players.get(i).clientThread.sendGameInfoToPlayer("show_decision_card:" + j + ":" 
									/*+ player j's decision card);
						}
					}
				}
			}
			
			for (int i=0; i<phase2counter; i++)
			{
				
				//phase3actions[i].cardMessage();
				//phase3actions[i].action(input, players);
			}
			for (int i=0; i<players.size(); i++)
			{
				players.get(i).setIsMist(false); //after Phase 3, make sure no one is still Misted
			}
			for (int i=0; i<players.size(); i++)
			{
				players.get(i).setIsRooted(false); //at the end of the turn, make sure no one is Rooted
			}
			 
			//if drawDeck is empty, reshuffle into cardDeck
			if (drawDeck.isEmpty())
			{
				stockDrawDeck();
			}
		}
		 
	}
*/
}
