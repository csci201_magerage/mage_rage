package Gameplay.Server;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Vector;

import Networking.Server.ClientThread;
import Gameplay.Cards.SpellCard;

public class Player {
	private int health;
	private Vector<SpellCard> SpellCardHand;
	public Vector<SpellCard> SpellCardsChosen;
	public Vector<String> DecisionCardsChosen;
	private boolean guestMode;
	private boolean isDead;
	private boolean isRooted;
	private boolean isMist;
	private int index;
	
	public ClientThread clientThread;
	
	private int xposition;
	private int yposition;
	
	
	public Player(ClientThread ct)
	{
		health = 12;
		clientThread = ct;
		isDead = false;
		isRooted = false;
		isMist = false;
		
		if (ct.isGuest())
		{
			guestMode = true;
		}
		else
		{
			guestMode = false;
		}
		
		SpellCardHand = new Vector<SpellCard>();
		SpellCardsChosen = new Vector<SpellCard>();
		DecisionCardsChosen = new Vector<String>();
	}
	
	public boolean isGuest()
	{
		return guestMode;
	}
	
	public void setIsDead()
	{
		isDead = true;
	}
	
	public int getHealth()
	{
		return health;
	}
	
	public void takeDamage(int damage)
	{
		if (!isMist)
		{
			health -= damage;
			if(damage>0 && !guestMode){
				clientThread.updateDamageTaken(damage);
			}
			//send message saying "The Dude took damage"
			if (health <= 0)
			{
				isDead = true;
				//send message saying "The Dude died"
			}
		}
		else
		{
			//Player is misted!
		}
	}
	
	public Vector<SpellCard> getSpellCardHand()
	{
		return SpellCardHand;
	}
	
	public SpellCard getSpellCardChosen(int index)
	{
		return SpellCardsChosen.get(index);
	}
	
	public void dealSpellCard(SpellCard card)
	{	
		//draw a card and put it into SpellCardHand
		card.setOwner(index);
		SpellCardHand.add(card);
	}
	
	/*
	public void setSpellCardsChosen(SpellCard a, SpellCard b, SpellCard c)
	{
		SpellCardsChosen.set(0, a);
		SpellCardsChosen.set(1, b);
		SpellCardsChosen.set(2, c);
	}
	*/
	
	public boolean getIsDead()
	{
		return isDead;
	}
	
	public boolean getIsRooted()
	{
		return isRooted;
	}
	
	public void setIsRooted(boolean root)
	{
		isRooted = root;
	}
	
	public boolean getIsMist()
	{
		return isMist;
	}
	
	public void setIsMist(boolean mist)
	{
		isMist = mist;
	}
	
	public int getXPosition()
	{
		return xposition;
	}
	
	public int getYPosition()
	{
		return yposition;
	}
	
	public void setXPosition(int coordinate)
	{
		xposition = coordinate;
	}
	
	public void setYPosition(int coordinate)
	{
		yposition = coordinate;
	}
	
	public void setIndex(int i)
	{
		index = i;
	}
	
	public void removeFromHand(SpellCard card)
	{
		SpellCardHand.remove(card);
	}
	
	public int getHandSize()
	{
		return SpellCardHand.size();
	}
	
	/*
	public void setHandSize(int a)
	{
		handSize = a;
	}
	
	*/
}
