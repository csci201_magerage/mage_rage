package Gameplay.Client;

public class ClientGame{
	private Client client;
	
	private boolean receivedUserInput;
	private int[] serverIndices;
	private String message;
	
	public ClientGame(Client c) {
		this.client = c;
		
		this.serverIndices = new int[4];
	}
	
	public void sendMessage(StringBuilder sb) {
		String msg = this.serverIndices[0] + sb.toString();
		this.client.netClient.sendGameInfo(msg);
	}	
	
	public void receiveMessage(String msg){
		this.message = msg;
		this.receivedUserInput = false;
		
		String[] components = message.split(":");
		
		// receiving server index
		if(components[0].equals("receive_server_index")) {
			int index = Integer.parseInt(components[1]);
			for(int i = 0; i < 4; i++) {
				if(i == 0) {
					this.serverIndices[i] = index;
				} else {
					this.serverIndices[i] = this.serverIndices[i - 1] + 1;
					
					if(this.serverIndices[i] > 3) {
						this.serverIndices[i] -= 4;
					}
				}
			}
		}
		
		if(components[0].equals("phase1")) {
			this.client.gameWindow.updatePhaseLabel("PHASE 1");
		}
		
		if(components[0].equals("phase2")) {
			this.client.gameWindow.updatePhaseLabel("PHASE 2");
		}
		
		if(components[0].equals("phase3")) {
			this.client.gameWindow.updatePhaseLabel("PHASE 3");
		}
		
		if(components[0].equals("update_turn")) {
			int number = Integer.parseInt(components[1]);
			this.client.gameWindow.updateRoundLabel("ROUND " + number);
		}
		
		if(components[0].equals("activity")) {
			this.client.gameWindow.updateLogLabel(components[1]);
		}
		
		// receiving user name
		if(components[0].equals("show_user_name")) 
		{
			int index = Integer.parseInt(components[1]);
			// TODO - change mage colors
			this.client.gameWindow.setPlayerNameLabel(index, components[2]);
		}
		
		//receiving spell cards
		if (components[0].equals("receive_spell_card"))
		{
			this.client.gameWindow.setMainPlayerSpellCardLabel(components[1]);
		}
		
		if(components[0].equals("reset_decision_cards")) {
			this.client.gameWindow.setMainPlayerDecisionCardLabels();
		}
		
		// showing spell cards
		if(components[0].equals("show_spell_cards")) 
		{
			int index = Integer.parseInt(components[1]);
			for (int i = 0; i < 4; i++)
			{
				if (index == serverIndices[i])
				{
					if(i != 0) 
					{
						// TODO - check if this works properly with GUI
						this.client.gameWindow.setPlayerSpellCardLabel(i, components[2]);
						this.client.gameWindow.setPlayerSpellCardLabel(i, components[3]);
						this.client.gameWindow.setPlayerSpellCardLabel(i, components[4]);
					}

				}
			}
		}
		
		// showing decision cards
		if(components[0].equals("show_decision_card"))
		{
			int index = Integer.parseInt(components[1]);
			for (int i = 0; i < 4; i++)
			{
				if (index == serverIndices[i])
				{
					// TODO - check if this works properly with GUI
					this.client.gameWindow.setPlayerDecisionCardLabel(i, components[2]);
				}
			}
		}
		
		// choosing spell cards
		if(components[0].equals("choose_spell_cards")) 
		{
			this.client.gameWindow.promptUserForInput("spell_cards");
		}
		
		if(components[0].equals("trial_ended")) {
			this.client.gameWindow.promptUserTrialEnded();
		}
		
		// choosing decision cards
		if(components[0].equals("choose_decision_cards"))
		{
			this.client.gameWindow.promptUserForInput("decision_cards");
		}
				
		//updating game state
		else if (components[0].equals("update_health")) 
		{
			int index = Integer.parseInt(components[1]);
			int health = Integer.parseInt(components[2]);
			this.client.gameWindow.setPlayerHealthLabel(index, health);
		}
		else if (components[0].equals("update_position")) 
		{
			int index = Integer.parseInt(components[1]);
			int x = Integer.parseInt(components[2]);
			int y = Integer.parseInt(components[3]);
			this.client.gameWindow.setPlayerPosition(index, x, y);
		}
		else if (components[0].equals("update_isDead")) 
		{
			int index = Integer.parseInt(components[1]);
			for (int i = 0; i < 4; i++)
			{
				if (index == serverIndices[i])
				{
					// TODO - check if this works properly with GUI
					// setPlayerDead() should already update GUI appropriately based on the index
					this.client.gameWindow.setPlayerDead(i);
				}
			}
		}
		
		//animations
		else if (components[0].equals("animate")) //TODO
		{
			//I haven't worked out exactly what's gonna need to happen in these if statements -- Jake
			//animate one of the following
			if (components[1].equals("movement_fast"))
			{
				//components[2] is player index
			}
			else if (components[1].equals("mist"))
			{
				//components[2] is x position
				//components[3] is y position
			}
			else if (components[1].equals("mage_rage"))
			{
				//components[2] is 
			}
			else if (components[1].equals("heal"))
			{
				//components[2] is x position
				//components[3] is y position
			}
			else if (components[1].equals("teleport"))
			{
				
			}
			else if (components[1].equals("roots"))
			{
				//components[2] is x position
				//components[3] is y position
			}
			else if (components[1].equals("storm"))
			{
				//components[2] is x position
				//components[3] is y position
			}
			else if (components[1].equals("gust"))
			{
				
			}
			else if (components[1].equals("earthquake"))
			{
				//components[2] is x position
				//components[3] is y position
			}
			else if (components[1].equals("water_jet"))
			{
				
			}
			else if (components[1].equals("tornado"))
			{
				
			}
			else if (components[1].equals("fire_breath"))
			{
				
			}
			else if (components[1].equals("lightning"))
			{
				
			}
			else if (components[1].equals("ice_spike"))
			{
				
			}
			else if (components[1].equals("comet"))
			{
				
			}
			else if (components[1].equals("hailstorm"))
			{
				
			}
			else if (components[1].equals("movement_slow"))
			{
				
			}
		}
		
		//spells
		else if (components[0].equals("movement_fast")) 
		{
			this.client.gameWindow.showInputWindow("movement_fast");
			System.out.println("ClientGame: Waiting on movement fast input...");
			while(!this.receivedUserInput) {	
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			System.out.println("ClientGame: Received movement fast input");
			this.client.netClient.sendGameInfo(this.client.gameWindow.userInput1);
		}
		else if (components[0].equals("mist"))
		{
			this.client.netClient.sendGameInfo("unused");
		}
		else if (components[0].equals("mage_rage")) 
		{
			this.client.gameWindow.showInputWindow("mage_rage");
			System.out.println("ClientGame: Waiting on mage rage input...");
			while(!this.receivedUserInput) {	
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			System.out.println("ClientGame: Received mage rage input");
			this.client.netClient.sendGameInfo(this.client.gameWindow.userInput1 + ":" + this.client.gameWindow.userInput2);
		}
		else if (components[0].equals("heal"))
		{
			this.client.netClient.sendGameInfo("unused");
		}
		else if (components[0].equals("teleport")) 
		{
			this.client.gameWindow.showInputWindow("teleport");
			System.out.println("ClientGame: Waiting on teleport input...");
			while(!this.receivedUserInput) {	
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			System.out.println("ClientGame: Received teleport input");
			this.client.netClient.sendGameInfo(this.client.gameWindow.userInput1);			
		}
		else if (components[0].equals("roots"))
		{
			this.client.gameWindow.showInputWindow("roots");	
			System.out.println("ClientGame: Waiting on roots input...");
			while(!this.receivedUserInput) {	
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			System.out.println("ClientGame: Received roots input");
			this.client.netClient.sendGameInfo(this.client.gameWindow.userInput1);
		}
		else if (components[0].equals("storm")) 
		{
			this.client.gameWindow.showInputWindow("storm");
			System.out.println("ClientGame: Waiting on storm input...");
			while(!this.receivedUserInput) {	
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			System.out.println("ClientGame: Received storm input");
			this.client.netClient.sendGameInfo(this.client.gameWindow.userInput1);
		}
		else if (components[0].equals("gust")) 
		{
			this.client.gameWindow.showInputWindow("gust");
			System.out.println("ClientGame: Waiting on gust input...");
			while(!this.receivedUserInput) {	
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			System.out.println("ClientGame: Received gust input");
			this.client.netClient.sendGameInfo(this.client.gameWindow.userInput1 + ":" + this.client.gameWindow.userInput2);
		}
		else if (components[0].equals("earthquake"))
		{
			this.client.netClient.sendGameInfo("unused");
		}
		else if (components[0].equals("water_jet")) 
		{
			this.client.gameWindow.showInputWindow("water_jet");
			System.out.println("ClientGame: Waiting on water jet input...");
			while(!this.receivedUserInput) {	
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			System.out.println("ClientGame: Received water jet input");
			this.client.netClient.sendGameInfo(this.client.gameWindow.userInput1);
		}
		else if (components[0].equals("tornado")) 
		{
			this.client.gameWindow.showInputWindow("tornado");
			System.out.println("ClientGame: Waiting on tornado input...");
			while(!this.receivedUserInput) {	
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			System.out.println("ClientGame: Received tornado input");
			this.client.netClient.sendGameInfo(this.client.gameWindow.userInput1);
		}
		else if (components[0].equals("fire_breath")) 
		{
			this.client.gameWindow.showInputWindow("fire_breath");
			System.out.println("ClientGame: Waiting on fire breath input...");
			while(!this.receivedUserInput) {	
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			System.out.println("ClientGame: Received fire breath input");
			this.client.netClient.sendGameInfo(this.client.gameWindow.userInput1);
		}
		else if (components[0].equals("lightning")) 
		{
			this.client.gameWindow.showInputWindow("lightning");
			System.out.println("ClientGame: Waiting on lightning input...");
			while(!this.receivedUserInput) {	
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			System.out.println("ClientGame: Received lightning input");
			this.client.netClient.sendGameInfo(this.client.gameWindow.userInput1);
		}
		else if (components[0].equals("ice_spike"))
		{
			this.client.netClient.sendGameInfo("unused");
		}
		else if (components[0].equals("comet")) 
		{
			this.client.gameWindow.showInputWindow("comet");
			System.out.println("ClientGame: Waiting on comet input...");
			while(!this.receivedUserInput) {	
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			System.out.println("ClientGame: Received comet input");
			this.client.netClient.sendGameInfo(this.client.gameWindow.userInput1);
		}
		else if (components[0].equals("hailstorm"))
		{
			this.client.netClient.sendGameInfo("unused");
		}
		else if (components[0].equals("movement_slow")) 
		{
			this.client.gameWindow.showInputWindow("movement_slow");
			System.out.println("ClientGame: Waiting on movement slow input...");
			while(!this.receivedUserInput) {	
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
			System.out.println("ClientGame: Received movement slow input");
			this.client.netClient.sendGameInfo(this.client.gameWindow.userInput1);
		}
		
		/*Below is Christian's original code
		 * 
		 * 
		System.out.println("ClientGame: Received message = " + this.message);
		String[] components = message.split(":");
		
		int userIndex = Integer.parseInt(components[0]);
		
		System.out.println("ClientGame: Received user index = " + userIndex);
		
		for(int i = 0; i < 4; i++) {
			if(this.serverIndices[i] == userIndex) {
				userIndex = i;
			}
		}
		
		System.out.println("ClientGame: Created new user index = " + userIndex);
		
		// TODO - GAME UPDATES
		if (components[1].equals("server_index")){
			this.serverIndex = Integer.parseInt(components[2]);
			
			for(int i = 0; i < 4; i++) {
				if(i == 0) {
					this.serverIndices[i] = this.serverIndex;
				} else {
					this.serverIndices[i] = this.serverIndices[i - 1] + 1;
					
					if(this.serverIndices[i] > 3) {
						this.serverIndices[i] -= 4;
					}
				}
			}
		}
		
		if(components[1].equals("show_user_name")) {
			c.gameWindow.setPlayerNameLabel(userIndex, components[2]);
		}

		if(components[1].equals("show_received_spell_card")) {
			c.gameWindow.setMainPlayerSpellCardLabel(components[2]);
		}

		if(components[1].equals("show_chosen_spell_card")) { 
			c.gameWindow.setPlayerSpellCardLabel(userIndex, components[2]);
		}

		if(components[1].equals("show_chosen_decision_card")) { 
			c.gameWindow.setPlayerDecisionCardLabel(userIndex, components[2]);
		}


		// TODO - USER UPDATES
		if(components[1].equals("show_health")) {
			int health = Integer.parseInt(components[2]);
			c.gameWindow.setPlayerHealthLabel(userIndex, health);
		}

		if(components[1].equals("show_user_moved")) {
			int x = Integer.parseInt(components[2].charAt(0) + "");
			int y = Integer.parseInt(components[2].charAt(1) + "");
			c.gameWindow.setPlayerPosition(userIndex, x, y);
		}

		if(components[1].equals("show_user_dead")) {
			c.gameWindow.setPlayerDead(userIndex);
		}


		// TODO - CARD UPDATES
		if(components[1].equals("movement_fast")) {
			c.gameWindow.showInputWindow("movement_fast");
			System.out.println("ClientGame: Waiting on movement fast input...");
			while(!this.receivedUserInput) {	}
			System.out.println("ClientGame: Received movement fast input");
			c.netClient.sendGameInfo(this.serverIndex + ":" + c.gameWindow.userInput1);
		}

		if(components[1].equals("movement_slow")) {
			c.gameWindow.showInputWindow("movement_slow");
			System.out.println("ClientGame: Waiting on movement slow input...");
			while(!this.receivedUserInput) {	}
			System.out.println("ClientGame: Received movement slow input");
			c.netClient.sendGameInfo(this.serverIndex + ":" + c.gameWindow.userInput1);
		}

		if(components[1].equals("mist")) {
			c.netClient.sendGameInfo(this.serverIndex + ":" + "mist");
		}

		if(components[1].equals("mage_rage")) {
			c.gameWindow.showInputWindow("mage_rage");
			System.out.println("ClientGame: Waiting on mage rage input...");
			while(!this.receivedUserInput) {	}
			System.out.println("ClientGame: Received mage rage input");
			c.netClient.sendGameInfo(this.serverIndex + ":" + c.gameWindow.userInput1 + ":" + c.gameWindow.userInput2);
		}

		if(components[1].equals("heal")) {
			c.netClient.sendGameInfo("heal");
			int health = Integer.parseInt(components[2]);
			c.gameWindow.setPlayerHealthLabel(userIndex, health);
		}

		if(components[1].equals("telport")) {
			c.gameWindow.showInputWindow("teleport");
			System.out.println("ClientGame: Waiting on teleport input...");
			while(!this.receivedUserInput) {	}
			System.out.println("ClientGame: Received teleport input");
			c.netClient.sendGameInfo(this.serverIndex + ":" + c.gameWindow.userInput1);
		}

		if(components[1].equals("roots")) {
			c.gameWindow.showInputWindow("roots");	
			System.out.println("ClientGame: Waiting on roots input...");
			while(!this.receivedUserInput) {	}
			System.out.println("ClientGame: Received roots input");
			c.netClient.sendGameInfo(this.serverIndex + ":" + c.gameWindow.userInput1);
		}

		if(components[1].equals("storm")) {
			c.gameWindow.showInputWindow("storm");
			System.out.println("ClientGame: Waiting on storm input...");
			while(!this.receivedUserInput) {	}
			System.out.println("ClientGame: Received storm input");
			c.netClient.sendGameInfo(this.serverIndex + ":" + c.gameWindow.userInput1);
		}

		if(components[1].equals("gust")) {
			c.gameWindow.showInputWindow("gust");
			System.out.println("ClientGame: Waiting on gust input...");
			while(!this.receivedUserInput) {	}
			System.out.println("ClientGame: Received gust input");
			c.netClient.sendGameInfo(this.serverIndex + ":" + c.gameWindow.userInput1 + ":" + c.gameWindow.userInput2);
		}

		if(components[1].equals("earthquake")) {
			c.netClient.sendGameInfo(this.serverIndex + ":" + "earthquake");
		}

		if(components[1].equals("water_jet")) {
			c.gameWindow.showInputWindow("water_jet");
			System.out.println("ClientGame: Waiting on water jet input...");
			while(!this.receivedUserInput) {	}
			System.out.println("ClientGame: Received water jet input");
			c.netClient.sendGameInfo(this.serverIndex + ":" + c.gameWindow.userInput1);
		}

		if(components[1].equals("tornado")) {
			c.gameWindow.showInputWindow("tornado");
			System.out.println("ClientGame: Waiting on tornado input...");
			while(!this.receivedUserInput) {	}
			System.out.println("ClientGame: Received tornado input");
			c.netClient.sendGameInfo(this.serverIndex + ":" + c.gameWindow.userInput1);
		}

		if(components[1].equals("fire_breath")) {
			c.gameWindow.showInputWindow("fire_breath");
			System.out.println("ClientGame: Waiting on fire breath input...");
			while(!this.receivedUserInput) {	}
			System.out.println("ClientGame: Received fire breath input");
			c.netClient.sendGameInfo(this.serverIndex + ":" + c.gameWindow.userInput1);
		}

		if(components[1].equals("lightning")) {
			c.gameWindow.showInputWindow("lightning");
			System.out.println("ClientGame: Waiting on lightning input...");
			while(!this.receivedUserInput) {	}
			System.out.println("ClientGame: Received lightning input");
			c.netClient.sendGameInfo(this.serverIndex + ":" + c.gameWindow.userInput1);
		}

		if(components[1].equals("ice_spike")) {
			c.netClient.sendGameInfo(this.serverIndex + ":" + "ice_spike");
		}

		if(components[1].equals("comet")) {
			c.gameWindow.showInputWindow("comet");
			System.out.println("ClientGame: Waiting on comet input...");
			while(!this.receivedUserInput) {	}
			System.out.println("ClientGame: Received comet input");
			c.netClient.sendGameInfo(this.serverIndex + ":" + c.gameWindow.userInput1);
		}

		if(components[1].equals("hailstorm")) {
			c.netClient.sendGameInfo(this.serverIndex + ":" + "hailstorm");
		}
		*/
	}
	

	public void receivedUserInput() {
		this.receivedUserInput = true;
	}
}
