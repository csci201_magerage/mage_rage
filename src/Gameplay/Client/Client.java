package Gameplay.Client;

import Graphics.GUI.GamePlayGUI;
import Graphics.GUI.LobbyGUI;
import Graphics.GUI.LoginGUI;
import Graphics.GUI.MainScreenGUI;
import Graphics.GUI.RegistrationGUI;
import Networking.Client.NetClient;

public class Client extends Thread {
	public NetClient netClient;

	public MainScreenGUI mainWindow;
	public LoginGUI loginWindow;
	public LobbyGUI lobbyWindow;
	public GamePlayGUI gameWindow;
	
	public RegistrationGUI registrationWindow;
	
	public ClientGame clientGame;
	
	private boolean gameStarted;
	
	public Client() {
		this.netClient = new NetClient(this);
		this.clientGame = new ClientGame(this);
		this.mainWindow = new MainScreenGUI(this);	
		gameStarted = false;
	}
	
	private void closePreviousWindow(){
		if (mainWindow.isVisible()){
			mainWindow.setVisible(false);
		}
		
		if (registrationWindow != null){
			if (registrationWindow.isVisible()){
				registrationWindow.setVisible(false);
			}
		}
		if (loginWindow != null){
			if (loginWindow.isVisible()){
				loginWindow.setVisible(false);
			}
		}
		if (lobbyWindow!=null){
			if (lobbyWindow.isVisible()){
				lobbyWindow.setVisible(false);
			}
		}
		if (gameWindow != null){
			if (gameWindow.isVisible()){
				gameWindow.setVisible(false);
			}
		}
		
	}
	
	
	public void gotoMainScreen(){
		closePreviousWindow();
		mainWindow.setVisible(true);
	}
	
	public void gotoLoginWindow(){
		closePreviousWindow();
		if (loginWindow==null){
			loginWindow = new LoginGUI(this);
		}
		loginWindow.setVisible(true);
	}
	
	public void gotoLobbyWindow(){
		closePreviousWindow();
		if (lobbyWindow == null){
			lobbyWindow = new LobbyGUI(this);
		}
		lobbyWindow.setVisible(true);
		lobbyWindow.updateAll();
	}
	
	public void gotoGameWindow(){
		closePreviousWindow();
		if (gameWindow  == null){
			gameWindow = new GamePlayGUI(this);
		}
		gameWindow.setVisible(true);
	}
	
	public void gotoRegistrationWindow(){
		closePreviousWindow();
		if (registrationWindow == null){
			registrationWindow = new RegistrationGUI(this);
		}
		registrationWindow.setVisible(true);
	}

	public void GameStart(){
		gameStarted = true;
	}
	
	public void GameFinish(){
		gameStarted = false;
	}
	
	public boolean isGameStarted(){
		return gameStarted;
	}
	
	public static void main (String [] args){
		new Client();
	} 
}
	
