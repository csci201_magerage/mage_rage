package Gameplay.Cards;
import java.util.Vector;

import Gameplay.Server.Player;

public class Hailstorm extends SpellCard{

	public Hailstorm()
	{
		super();
		priority = 16;
		//cardFront = cardFrontImage.jpg
		message = "hailstorm";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//input string is unused
		for(int i=0; i<players.size(); i++)
		{
			if (!players.get(i).getIsDead()) //if player at players[i] is alive
			{
				players.get(i).takeDamage(1);
				//update all players with changes
				for (int j=0; j<players.size(); j++)
				{
					players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
				}
			}
		}
	}
}
