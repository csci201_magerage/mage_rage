package Gameplay.Cards;

import java.util.Vector;

import Gameplay.Server.Player;

public class WaterJet extends SpellCard {
	
	public WaterJet()
	{
		super();
		priority = 10;
		message = "water_jet";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//input string is direction player chooses ("nw", "ne", "sw", "se")
		if (input.equals("nw"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					if ( (players.get(i).getXPosition() - players.get(i).getYPosition() == players.get(owner).getXPosition() - players.get(owner).getYPosition()) 
							&& players.get(owner).getXPosition() < players.get(owner).getXPosition()) 
					{
						players.get(i).takeDamage(3);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else if (input.equals("ne"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					if ( (players.get(i).getXPosition() + players.get(i).getYPosition() == players.get(owner).getXPosition() + players.get(owner).getYPosition()) 
							&& players.get(owner).getXPosition() > players.get(owner).getXPosition()) 
					{
						players.get(i).takeDamage(3);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else if (input.equals("sw"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					if ( (players.get(i).getXPosition() + players.get(i).getYPosition() == players.get(owner).getXPosition() + players.get(owner).getYPosition()) 
							&& players.get(owner).getXPosition() < players.get(owner).getXPosition()) 
					{
						players.get(i).takeDamage(3);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else if (input.equals("se"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					if ( (players.get(i).getXPosition() - players.get(i).getYPosition() == players.get(owner).getXPosition() - players.get(owner).getYPosition()) 
							&& players.get(owner).getXPosition() > players.get(owner).getXPosition()) 
					{
						players.get(i).takeDamage(3);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		
	}
}
