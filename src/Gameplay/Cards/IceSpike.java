package Gameplay.Cards;

import java.util.Vector;

import Gameplay.Server.Player;

public class IceSpike extends SpellCard {
	
	public IceSpike()
	{
		super();
		priority = 14;
		message = "ice_spike";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//input is unused
		for (int i=0; i<players.size(); i++)
		{
			if (players.get(i).getIsDead() == false)
			{
				if (Math.abs(players.get(i).getXPosition() - players.get(owner).getXPosition()) + 
						Math.abs(players.get(i).getYPosition() - players.get(owner).getYPosition()) == 3) 
				{
					players.get(i).takeDamage(3);
					//update all players with changes
					for (int j=0; j<players.size(); j++)
					{
						players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
					}
				}
			}
		}
	}
}