package Gameplay.Cards;
import java.util.Vector;

import Gameplay.Server.Player;

public class Heal extends SpellCard{

	public Heal()
	{
		super();
		priority = 4;
		message = "heal";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//input string is unused
		if (players.get(owner).getHealth() < 10)
		{
			players.get(owner).takeDamage(-3);
		}
		else if (players.get(owner).getHealth() == 10)
		{
			players.get(owner).takeDamage(-2);
		}
		else if (players.get(owner).getHealth() == 11)
		{
			players.get(owner).takeDamage(-1);
		}
		else if (players.get(owner).getHealth() == 12){
		}
		else
		{
			System.out.println("ERROR: Player health is greater than 12");
			//should not get here
		}
		//send message saying 'players.get(owner) healed 3 damage!'
		//update all players with changes
		for (int j=0; j<players.size(); j++)
		{
			players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + owner + ":" + players.get(owner).getHealth());
		}
	}
}
