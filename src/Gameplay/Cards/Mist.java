package Gameplay.Cards;

import java.util.Vector;

import Gameplay.Server.Player;

public class Mist extends SpellCard{
	
	public Mist()
	{
		super();
		priority = 2;
		message = "mist";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//input is unused
		players.get(owner).setIsMist(true);
	}
}
