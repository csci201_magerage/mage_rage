package Gameplay.Cards;

import java.util.Vector;

import Gameplay.Server.Player;

public class Gust extends SpellCard {

	public Gust()
	{
		super();
		priority = 8;
		message = "gust";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//input format is <player index:direction> e.g. "2:w" to move Player 2 west/left
		String [] inputs = input.split(":");
		int target = Integer.parseInt(inputs[0]);
		if (players.get(target).getIsMist() == false)
		{
			if (inputs[1].equals("n"))
			{
				players.get(target).setYPosition(players.get(target).getYPosition() - 1);
				if (players.get(target).getYPosition() < 0)
				{
					players.get(target).setYPosition(0);
				}
				//update all players with changes
				for (int j=0; j<players.size(); j++)
				{
					players.get(j).clientThread.sendGameInfoToPlayer("update_position:" + target + ":" + players.get(target).getXPosition() + ":" + players.get(target).getYPosition());
				}
			}
			else if (inputs[1].equals("s"))
			{
				players.get(target).setYPosition(players.get(target).getYPosition() + 1);
				if (players.get(target).getYPosition() > 6)
				{
					players.get(target).setYPosition(6);
				}
				//update all players with changes
				for (int j=0; j<players.size(); j++)
				{
					players.get(j).clientThread.sendGameInfoToPlayer("update_position:" + target + ":" + players.get(target).getXPosition() + ":" + players.get(target).getYPosition());
				}
			}
			else if (inputs[1].equals("w"))
			{
				players.get(target).setXPosition(players.get(target).getXPosition() - 1);
				if (players.get(target).getXPosition() < 0)
				{
					players.get(target).setXPosition(0);
				}
				//update all players with changes
				for (int j=0; j<players.size(); j++)
				{
					players.get(j).clientThread.sendGameInfoToPlayer("update_position:" + target + ":" + players.get(target).getXPosition() + ":" + players.get(target).getYPosition());
				}
			}
			else if (inputs[1].equals("e"))
			{
				players.get(target).setXPosition(players.get(target).getXPosition() + 1);
				if (players.get(target).getXPosition() > 6)
				{
					players.get(target).setXPosition(6);
				}
				//update all players with changes
				for (int j=0; j<players.size(); j++)
				{
					players.get(j).clientThread.sendGameInfoToPlayer("update_position:" + target + ":" + players.get(target).getXPosition() + ":" + players.get(target).getYPosition());
				}
			}
			else
			{
				System.out.println("Error: inputs[1] is equal to " + inputs[1]);
				//program should not get here
			}
		}
	}
}
