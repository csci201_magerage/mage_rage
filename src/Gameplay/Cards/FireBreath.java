package Gameplay.Cards;

import java.util.Vector;

import Gameplay.Server.Player;

public class FireBreath extends SpellCard {
	
	public FireBreath()
	{
		super();
		priority = 12;
		message = "fire_breath";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//input is direction
		if (input.equals("n"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					if (Math.abs(players.get(i).getXPosition() - players.get(owner).getXPosition()) <= 1 && 
							(players.get(i).getYPosition() == players.get(owner).getYPosition() - 2) || 
							(players.get(i).getYPosition() == players.get(owner).getYPosition() - 1 && 
							players.get(i).getXPosition() == players.get(owner).getXPosition()) ) 
					{
						players.get(i).takeDamage(4);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else if (input.equals("s"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false && this.owner != i)
				{
					if (Math.abs(players.get(i).getXPosition() - players.get(owner).getXPosition()) <= 1 && 
							(players.get(i).getYPosition() == players.get(owner).getYPosition() + 2) || 
							(players.get(i).getYPosition() == players.get(owner).getYPosition() + 1 && 
							players.get(i).getXPosition() == players.get(owner).getXPosition()) ) 
					{
						players.get(i).takeDamage(4);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else if (input.equals("e"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false && this.owner != i)
				{
					if (Math.abs(players.get(i).getYPosition() - players.get(owner).getYPosition()) <= 1 && 
							(players.get(i).getXPosition() == players.get(owner).getXPosition() + 2) || 
							(players.get(i).getXPosition() == players.get(owner).getXPosition() + 1 && 
							players.get(i).getYPosition() == players.get(owner).getYPosition()) ) 
					{
						players.get(i).takeDamage(4);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else if (input.equals("w"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false && this.owner != i)
				{
					if (Math.abs(players.get(i).getYPosition() - players.get(owner).getYPosition()) <= 1 && 
							(players.get(i).getXPosition() == players.get(owner).getXPosition() - 2) || 
							(players.get(i).getXPosition() == players.get(owner).getXPosition() - 1 && 
							players.get(i).getYPosition() == players.get(owner).getYPosition()) ) 
					{
						players.get(i).takeDamage(4);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else
		{
			System.out.println("Error: incorrect input in FireBreath.action()");
			//it should not get here!
		}
	}
}
