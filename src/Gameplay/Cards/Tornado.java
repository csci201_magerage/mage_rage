package Gameplay.Cards;

import java.util.Vector;

import Gameplay.Server.Player;

public class Tornado extends SpellCard {
	
	
	public Tornado()
	{
		super();
		priority = 11;
		message = "tornado";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//input string is direction player chooses ("n", "e", "w", "s")
		if (input.equals("n"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					if ( (players.get(i).getXPosition() == players.get(owner).getXPosition()) 
							&& players.get(i).getYPosition() < players.get(owner).getYPosition()) 
					{
						players.get(i).takeDamage(3);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else if (input.equals("e"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					if ( (players.get(i).getYPosition() == players.get(owner).getYPosition()) 
							&& players.get(i).getXPosition() > players.get(owner).getXPosition()) 
					{
						players.get(i).takeDamage(3);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else if (input.equals("w"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					if ( (players.get(i).getYPosition() == players.get(owner).getYPosition()) 
							&& players.get(i).getXPosition() < players.get(owner).getXPosition()) 
					{
						players.get(i).takeDamage(3);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else if (input.equals("s"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					if ( (players.get(i).getXPosition() == players.get(owner).getXPosition()) 
							&& players.get(i).getYPosition() > players.get(owner).getYPosition()) 
					{
						players.get(i).takeDamage(3);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		
	}
}
