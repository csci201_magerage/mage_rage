package Gameplay.Cards;
import java.util.Vector;

import Gameplay.Server.Player;

public abstract class SpellCard extends Card{
	
	public SpellCard()
	{
		//cardBack = spellCardBack.jpg
	}
	
	public abstract void action(String input, Vector<Player> players);
	
	
}
