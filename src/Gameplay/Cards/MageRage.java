package Gameplay.Cards;

import java.util.Vector;

import Gameplay.Server.Player;

public class MageRage extends SpellCard {

	public MageRage()
	{
		super();
		priority = 3;
		message = "mage_rage";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//expected input is coordinates|direction
		String[] inputs = input.split(":");
		if (players.get(owner).getIsRooted() == false) //if players.get(owner) is not rooted
		{
			int num = Integer.parseInt(inputs[0]); 
			int y = num % 10;
			int x = (num - y) / 10;
			players.get(owner).setXPosition(x); //change x position
			players.get(owner).setYPosition(y); //change y position
			//update all players with changes
			for (int j=0; j<players.size(); j++)
			{
				players.get(j).clientThread.sendGameInfoToPlayer("update_position:" + owner + ":" + players.get(owner).getXPosition() + ":" + players.get(owner).getYPosition());
			}
			if (inputs[1].equals("n")) //if players.get(owner) attacks the north square
			{
				for (int i=0; i<players.size(); i++) //check all players
				{
					if (players.get(i).getIsDead() == false) //as long as they're not dead
					{
						if (players.get(i).getXPosition() == x-1 && players.get(i).getYPosition() == y) 
						{
							players.get(i).takeDamage(5);
							//update all players with changes
							for (int j=0; j<players.size(); j++)
							{
								players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
							}
						}
					}
				}
			}
			else if (inputs[1].equals("s")) //if players.get(owner) attacks the south square
			{
				for (int i=0; i<players.size(); i++) //check all players
				{
					if (players.get(i).getIsDead() == false) //as long as they're not dead
					{
						if (players.get(i).getXPosition() == x+1 && players.get(i).getYPosition() == y) 
						{
							players.get(i).takeDamage(5);
							//update all players with changes
							for (int j=0; j<players.size(); j++)
							{
								players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
							}
						}
					}
				}
			}
			else if (inputs[1].equals("w")) //if players.get(owner) attacks the west square
			{
				for (int i=0; i<players.size(); i++) //check all players
				{
					if (players.get(i).getIsDead() == false) //as long as they're not dead
					{
						if (players.get(i).getXPosition() == x && players.get(i).getYPosition() == y-1) 
						{
							players.get(i).takeDamage(5);
							//update all players with changes
							for (int j=0; j<players.size(); j++)
							{
								players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
							}
						}
					}
				}
			}
			else if (inputs[1].equals("e")) //if player attacks the east square
			{
				for (int i=0; i<players.size(); i++) //check all players
				{
					if (players.get(i).getIsDead() == false) //as long as they're not dead
					{
						if (players.get(i).getXPosition() == x && players.get(i).getYPosition() == y+1) 
						{
							players.get(i).takeDamage(5);
							//update all players with changes
							for (int j=0; j<players.size(); j++)
							{
								players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
							}
						}
					}
				}
			}
			
		}
	}
}
