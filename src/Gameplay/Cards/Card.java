package Gameplay.Cards;
import java.util.Vector;

import javax.swing.ImageIcon;

import Gameplay.Server.Player;

public abstract class Card{
	protected int priority;
	public int owner;
	protected String message;
	
	protected ImageIcon cardFront;
	protected ImageIcon cardBack;
	
	public abstract void action(String input, Vector<Player> players);
	
	public int getPriority()
	{
		return priority;
	}
	
	public ImageIcon getCardFront()
	{
		return cardFront;
	}
	
	public ImageIcon getCardBack()
	{
		return cardBack;
	}
	
	public String cardMessage()
	{
		return message;
	}
	
	public String toString(){
		return message;
	}
	
	public void setOwner(int i)
	{
		owner = i;
	}
}
