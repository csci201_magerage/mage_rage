package Gameplay.Cards;

import java.util.Vector;

import Gameplay.Server.Player;

public class Lightning extends SpellCard {
	
	public Lightning()
	{
		super();
		priority = 13;
		message = "lightning";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//expected input is valid coordinates of space for Lightning to hit
		int num = Integer.parseInt(input);
		int y = (num % 10);
		int x = (num - y) / 10;
		for (int i=0; i<players.size(); i++)
		{
			if (players.get(i).getIsDead() == false)
			{
				if (players.get(i).getXPosition() == x && players.get(i).getYPosition() == y) 
				{
					players.get(i).takeDamage(2);
					//update all players with changes
					for (int j=0; j<players.size(); j++)
					{
						players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
					}
				}
			}
		}
	}
}
