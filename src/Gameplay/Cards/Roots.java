package Gameplay.Cards;

import java.util.Vector;

import Gameplay.Server.Player;

public class Roots extends SpellCard{
	
	public Roots()
	{
		super();
		priority = 6;
		message = "roots";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//input is unused
		players.get(owner).setIsRooted(true);
	}
}
