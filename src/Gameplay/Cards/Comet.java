package Gameplay.Cards;

import java.util.Vector;

import Gameplay.Server.Player;

public class Comet extends SpellCard {
	
	public Comet()
	{
		super();
		priority = 15;
		message = "comet";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//input is direction
		if (input.equals("nw"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false)
				{
					if (players.get(i).getXPosition() < players.get(owner).getXPosition() &&
							players.get(i).getXPosition() > players.get(owner).getXPosition() - 3 &&
							players.get(i).getYPosition() < players.get(owner).getYPosition() &&
							players.get(i).getYPosition() > players.get(owner).getYPosition() - 3) 
					{
						players.get(i).takeDamage(4);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else if (input.equals("ne"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false && this.owner != i)
				{
					if (players.get(i).getXPosition() > players.get(owner).getXPosition() &&
							players.get(i).getXPosition() < players.get(owner).getXPosition() + 3 &&
							players.get(i).getYPosition() < players.get(owner).getYPosition() &&
							players.get(i).getYPosition() > players.get(owner).getYPosition() - 3) 
					{
						players.get(i).takeDamage(4);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else if (input.equals("sw"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false && this.owner != i)
				{
					if (players.get(i).getXPosition() < players.get(owner).getXPosition() &&
							players.get(i).getXPosition() > players.get(owner).getXPosition() - 3 &&
							players.get(i).getYPosition() > players.get(owner).getYPosition() &&
							players.get(i).getYPosition() < players.get(owner).getYPosition() + 3) 
					{
						players.get(i).takeDamage(4);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else if (input.equals("se"))
		{
			for (int i=0; i<players.size(); i++)
			{
				if (players.get(i).getIsDead() == false && this.owner != i)
				{
					if (players.get(i).getXPosition() > players.get(owner).getXPosition() &&
							players.get(i).getXPosition() < players.get(owner).getXPosition() + 3 &&
							players.get(i).getYPosition() > players.get(owner).getYPosition() &&
							players.get(i).getYPosition() < players.get(owner).getYPosition() + 3) 
					{
						players.get(i).takeDamage(4);
						//update all players with changes
						for (int j=0; j<players.size(); j++)
						{
							players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
						}
					}
				}
			}
		}
		else
		{
			System.out.println("Error: incorrect input in Comet.action()");
			//it should not get here!
		}
	}
}
