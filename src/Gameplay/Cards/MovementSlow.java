package Gameplay.Cards;

import java.util.Vector;

import Gameplay.Server.Player;

public class MovementSlow extends Card {

	public MovementSlow()
	{
		super();
		priority = 17;
		message = "movement_slow";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//expected input is valid space coordinates
		if (players.get(owner).getIsRooted() == false)
		{
			int num = Integer.parseInt(input);
			int y = num % 10;
			int x = (num - y) / 10;
			players.get(owner).setXPosition(x);
			players.get(owner).setYPosition(y);
			//update all players with changes
			for (int j=0; j<players.size(); j++)
			{
				players.get(j).clientThread.sendGameInfoToPlayer("update_position:" + owner + ":" + players.get(owner).getXPosition() + ":" + players.get(owner).getYPosition());
			}
		}
	}
}
