package Gameplay.Cards;

import java.util.Vector;

import Gameplay.Server.Player;

public class Earthquake extends SpellCard {
	
	public Earthquake()
	{
		super();
		priority = 9;
		message = "earthquake";
	}
	
	public void action(String input, Vector<Player> players)
	{
		//input is unused
		for (int i=0; i<players.size(); i++)
		{
			if (players.get(i).getIsDead() == false && owner != i)
			{
				if (Math.abs(players.get(i).getXPosition() - players.get(owner).getXPosition()) + 
						Math.abs(players.get(i).getYPosition() - players.get(owner).getYPosition()) <= 2) 
				{
					players.get(i).takeDamage(3);
					//update all players with changes
					for (int j=0; j<players.size(); j++)
					{
						players.get(j).clientThread.sendGameInfoToPlayer("update_health:" + i + ":" + players.get(i).getHealth());
					}
				}
			}
		}
	}
}
