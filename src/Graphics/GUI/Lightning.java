package Graphics.GUI;

import javax.swing.ImageIcon;

public class Lightning extends Thread{
	private static ImageIcon [] a;
	private ImageIcon currentImage;
	private boolean flag;
	
	
	public Lightning(){
		flag = true;
		a = new ImageIcon[24];
		a[0] = new ImageIcon("images/lightning1.png");
		a[1] = new ImageIcon("images/lightning2.png");
		a[2] = new ImageIcon("images/lightning3.png");
		a[3] = new ImageIcon("images/lightning4.png");
		a[4] = new ImageIcon("images/lightning5.png");
		a[5] = new ImageIcon("images/lightning6.png");
		a[6] = new ImageIcon("images/lightning7.png");
		a[7] = new ImageIcon("images/lightning8.png");
		a[8] = new ImageIcon("images/lightning9.png");
		a[9] = new ImageIcon("images/lightning10.png");
		a[10] = new ImageIcon("images/lightning11.png");
		a[11] = new ImageIcon("images/lightning12.png");
		a[12] = new ImageIcon("images/lightning13.png");
		a[13] = new ImageIcon("images/lightning14.png");
		a[14] = new ImageIcon("images/lightning15.png");
		a[15] = new ImageIcon("images/lightning16.png");
		a[16] = new ImageIcon("images/lightning17.png");
		a[17] = new ImageIcon("images/lightning18.png");
		a[18] = new ImageIcon("images/lightning19.png");
		a[19] = new ImageIcon("images/lightning20.png");
		a[20] = new ImageIcon("images/lightning21.png");
		a[21] = new ImageIcon("images/lightning22.png");
		a[22] = new ImageIcon("images/lightning23.png");
		a[23] = new ImageIcon("images/lightning24.png");


		currentImage = a[0];
	}
	
	public ImageIcon currentImage(){
		return currentImage;
	}
	
	public void run (){
		while (flag){
			try{
				Thread.sleep(150);
				if (currentImage == a[0])
					currentImage = a[1];
				else if (currentImage == a[1])
					currentImage = a[2];
				else if (currentImage == a[2])
					currentImage = a[3];
				else if (currentImage == a[3])
					currentImage = a[4];
				else if (currentImage == a[4])
					currentImage = a[5];
				else if (currentImage == a[5])
					currentImage = a[6];
				else if (currentImage == a[6])
					currentImage = a[7];
				else if (currentImage == a[7])
					currentImage = a[8];
				else if (currentImage == a[8])
					currentImage = a[9];
				else if (currentImage == a[9])
					currentImage = a[10];
				else if (currentImage == a[10])
					currentImage = a[11];
				else if (currentImage == a[11])
					currentImage = a[12];
				else if (currentImage == a[12])
					currentImage = a[13];
				else if (currentImage == a[13])
					currentImage = a[14];
				else if (currentImage == a[14])
					currentImage = a[15];
				else if (currentImage == a[15])
					currentImage = a[16];
				else if (currentImage == a[16])
					currentImage = a[17];
				else if (currentImage == a[17])
					currentImage = a[18];
				else if (currentImage == a[18])
					currentImage = a[19];
				else if (currentImage == a[19])
					currentImage = a[20];
				else if (currentImage == a[20])
					currentImage = a[21];
				else if (currentImage == a[21])
					currentImage = a[22];
				else if (currentImage == a[22])
					currentImage = a[23];

				else
					currentImage = a[0];
			}catch(InterruptedException IE){
				
			}
		}
	}

}
