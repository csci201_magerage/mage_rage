package Graphics.GUI;

import javax.swing.ImageIcon;

public class Fire extends Thread{
	private static ImageIcon [] a;
	private ImageIcon currentImage;
	private boolean flag;
	
	
	public Fire(){
		flag = true;
		a = new ImageIcon[23];
		a[0] = new ImageIcon("images/fire1.png");
		a[1] = new ImageIcon("images/fire2.png");
		a[2] = new ImageIcon("images/fire3.png");
		a[3] = new ImageIcon("images/fire4.png");
		a[4] = new ImageIcon("images/fire5.png");
		a[5] = new ImageIcon("images/fire6.png");
		a[6] = new ImageIcon("images/fire7.png");
		a[7] = new ImageIcon("images/fire8.png");
		a[8] = new ImageIcon("images/fire9.png");
		a[9] = new ImageIcon("images/fire10.png");
		a[10] = new ImageIcon("images/fire11.png");
		a[11] = new ImageIcon("images/fire12.png");
		a[12] = new ImageIcon("images/fire13.png");
		a[13] = new ImageIcon("images/fire14.png");
		a[14] = new ImageIcon("images/fire15.png");
		a[15] = new ImageIcon("images/fire16.png");
		a[16] = new ImageIcon("images/fire17.png");
		a[17] = new ImageIcon("images/fire18.png");
		a[18] = new ImageIcon("images/fire19.png");
		a[19] = new ImageIcon("images/fire20.png");
		a[20] = new ImageIcon("images/fire21.png");
		a[21] = new ImageIcon("images/fire22.png");
		a[22] = new ImageIcon("images/fire23.png");




		currentImage = a[0];
	}
	
	public ImageIcon currentImage(){
		return currentImage;
	}
	
	public void run (){
		while (flag){
			try{
				Thread.sleep(150);
				if (currentImage == a[0])
					currentImage = a[1];
				else if (currentImage == a[1])
					currentImage = a[2];
				else if (currentImage == a[2])
					currentImage = a[3];
				else if (currentImage == a[3])
					currentImage = a[4];
				else if (currentImage == a[4])
					currentImage = a[5];
				else if (currentImage == a[5])
					currentImage = a[6];
				else if (currentImage == a[6])
					currentImage = a[7];
				else if (currentImage == a[7])
					currentImage = a[8];
				else if (currentImage == a[8])
					currentImage = a[9];
				else if (currentImage == a[9])
					currentImage = a[10];
				else if (currentImage == a[10])
					currentImage = a[11];
				else if (currentImage == a[11])
					currentImage = a[12];
				else if (currentImage == a[12])
					currentImage = a[13];
				else if (currentImage == a[13])
					currentImage = a[14];
				else if (currentImage == a[14])
					currentImage = a[15];
				else if (currentImage == a[15])
					currentImage = a[16];
				else if (currentImage == a[16])
					currentImage = a[17];
				else if (currentImage == a[17])
					currentImage = a[18];
				else if (currentImage == a[18])
					currentImage = a[19];
				else if (currentImage == a[19])
					currentImage = a[20];
				else if (currentImage == a[20])
					currentImage = a[21];
				else if (currentImage == a[21])
					currentImage = a[22];


				else
					currentImage = a[0];
			}catch(InterruptedException IE){
				
			}
		}
	}

}
