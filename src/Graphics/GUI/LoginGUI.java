package Graphics.GUI;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;




import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Gameplay.Client.Client;



public class LoginGUI extends JFrame{
	private static final long serialVersionUID = 1L;

	//Components
	private JLabel MRtitle;
	
	private JLabel usernameLabel, passwordLabel;
	private JTextField usernameField, passwordField;
	
	private JButton loginButton, exitButton;
	private JCheckBox guestCheck;
	
	//Layout related
	private JPanel usernamePanel, passwordPanel, checkboxPanel, buttonPanel;
	
	private boolean isGuest;
	
	//Client
	public Client c;
	
	public LoginGUI(Client c){
		super("Mage Rage - Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.c = c;
		initialize();
		addActions();
		createGUI();
		
	}
	
	private void initialize(){
		setSize(400,300);
		setResizable(false);
		setLocationRelativeTo(null);
		
		MRtitle = new JLabel(ImageLibrary.MRTitle);
		
		usernameLabel = new JLabel("Username:");
		usernameField = new JTextField(20);
		passwordLabel = new JLabel("Password:");
		passwordField = new JPasswordField(20);
		
		guestCheck = new JCheckBox("Login as a guest (spectator)");
		
		loginButton = new JButton("Login");
		exitButton = new JButton("Back");
		
		usernamePanel = new JPanel();
		passwordPanel = new JPanel();
		checkboxPanel = new JPanel();
		buttonPanel = new JPanel();
		
		isGuest = false;
	}
	
	private void addActions(){
		loginButton.addActionListener(new LoginButtonAction(this));		
		
		
		guestCheck.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				isGuest = guestCheck.isSelected();
				if (isGuest){
					usernameField.setEnabled(false);
					passwordField.setEnabled(false);
				}else{
					usernameField.setEnabled(true);
					passwordField.setEnabled(true);
				}
				
			}
		});
		
		exitButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {			
				c.gotoMainScreen();
			}			
		});
	}
	
	private void createGUI(){
		usernamePanel.add(usernameLabel);
		usernamePanel.add(usernameField);
		passwordPanel.add(passwordLabel);
		passwordPanel.add(passwordField);
		
		checkboxPanel.add(guestCheck);
		
		buttonPanel.add(loginButton);
		buttonPanel.add(exitButton);
		
		MRtitle.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		setLayout(new BoxLayout(this.getContentPane(),BoxLayout.Y_AXIS));
		add(Box.createRigidArea(new Dimension(0,30)));
		add(MRtitle);
		add(Box.createRigidArea(new Dimension(0,20)));
		add(usernamePanel);
		add(passwordPanel);
		add(checkboxPanel);
		add(buttonPanel);
		
	}
	
	class LoginButtonAction implements ActionListener{
		LoginGUI parent;
		
		public LoginButtonAction(LoginGUI parent){
			this.parent = parent;
		}
		
		public void actionPerformed(ActionEvent e) {
			if (isGuest){
				if (!c.netClient.isActive()){
					JOptionPane.showMessageDialog(null, "Cannot connect to server", "Error", JOptionPane.ERROR_MESSAGE);
				}else{
					c.netClient.sendUP("guest","guest");
				}
				
				
			}else{		
				if (usernameField.getText().equals("")){
					JOptionPane.showMessageDialog(null, "Please enter your username", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				if (passwordField.getText().equals("")){
					JOptionPane.showMessageDialog(null, "Please enter your password", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
			
				if (!c.netClient.isActive()){
					JOptionPane.showMessageDialog(null, "Cannot connect to server", "Error", JOptionPane.ERROR_MESSAGE);
				}else{
					c.netClient.sendUP(usernameField.getText(),passwordField.getText());	
				}
			
		
		}
		}
	}
	
	public void showFail(){
		JOptionPane.showMessageDialog(this, "Login Failed! Please check your username and password.", "Error", JOptionPane.ERROR_MESSAGE);
		return;
	}
	
	
}
