package Graphics.GUI;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import com.sun.glass.events.WindowEvent;

import Gameplay.Client.Client;

public class MainScreenGUI extends JFrame{

	private static final long serialVersionUID = 1;
	private JPanel centerPanel;
	private JButton logIn, registration, rules, exit, options, credits;
	private JLabel water, earth, fire, ice, lightning, mainImage, rulePic;
	private Water waterThread;
	private Lightning lightningThread;
	private Earth earthThread;
	private Ice iceThread;
	private Fire fireThread;
	private ImageIcon pic, in;
	private boolean mute;
	private Client c;
	private PlayMidiAudio play;
	private Sequencer sequencer;
	
	
	private class animations extends Thread{
		public animations(){
			this.start();
		}
		
		public void run(){
			while(true){
				water.repaint();
				centerPanel.invalidate();
				centerPanel.repaint();		
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		}
		
	}
	
	public MainScreenGUI(Client c){
		super("Mage Rage - Main");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		try {
			UIManager.setLookAndFeel( UIManager.getCrossPlatformLookAndFeelClassName() );
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.c = c;
		//setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		initialize();
		addActions();
		createGUI();
		
		this.setVisible(true);
	}
	
	private void initialize(){
		centerPanel = new JPanel();
		logIn = new JButton("Login");
		credits = new JButton("Credits");
		registration = new JButton("Registration");
		rules = new JButton("Rules");
		exit = new JButton("Exit");
		options = new JButton("Mute Music");	
		rulePic = new JLabel();
		waterThread = new Water();
		Thread t1 = new Thread(waterThread);
		mute = false;
		try {
			play = new PlayMidiAudio();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t1.start();
		
		lightningThread = new Lightning();
		Thread t2 = new Thread(lightningThread);
		t2.start();
		
		earthThread = new Earth();
		Thread t3 = new Thread(earthThread);
		t3.start();
		
		fireThread = new Fire();
		Thread t4 = new Thread(fireThread);
		t4.start();
		
		iceThread = new Ice();
		Thread t5 = new Thread(iceThread);
		t5.start();
		
		pic = new ImageIcon("images/pentagon.png");
		
		water = new JLabel (){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				g.drawImage(waterThread.currentImage().getImage(), 0, 0, null);
				super.paintComponent(g);
			}
		};
		
		mainImage = new JLabel (){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				g.drawImage(pic.getImage(), 0, 0, null);
				super.paintComponent(g);
			}
		};
		
		lightning = new JLabel (){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				g.drawImage(lightningThread.currentImage().getImage(), 0, 0, null);
				super.paintComponent(g);
			}
		};
		
		earth = new JLabel (){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				g.drawImage(earthThread.currentImage().getImage(), 0, 0, null);
				super.paintComponent(g);
			}
		};
				
		fire = new JLabel (){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				g.drawImage(fireThread.currentImage().getImage(), 0, 0, null);
				super.paintComponent(g);
			}
		};
		
		ice = new JLabel (){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				g.drawImage(iceThread.currentImage().getImage(), 0, 0, null);
				super.paintComponent(g);
			}
		};
	}

	private void addActions(){
		logIn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				c.gotoLoginWindow();
			}
		});
				
		registration.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				c.gotoRegistrationWindow();
			}
		});
		
		options.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(!mute){
					sequencer.stop();
					mute = true;
				}
				else{
					sequencer.start();
					mute = false;
				}
			}
		});
		
		rules.addActionListener( new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				
			    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
			    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			        try {
			            desktop.browse((new URL("http://www.cwf123.com/magerage/MageRageRules.html").toURI()));
			        } catch (Exception e) {
			            e.printStackTrace();
			        }
			    }
			}
		});
		
		credits.addActionListener( new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				JDialog jd = new JDialog();
				jd.setLayout(new BoxLayout(jd.getContentPane(), BoxLayout.Y_AXIS));
				jd.setSize(300,300);
				jd.setLocationRelativeTo(null);
				jd.setResizable(false);
				jd.setTitle("Credits");
				jd.setLayout(new BorderLayout());
				JTextArea text = new JTextArea();
				text.setEditable(false);
				text.append("Created for CSCI 201 Final Project\n Designers \n Jake, Martin, Guayo, Bartow, Jeff \n Programmers \n Joseph, Martin, Alex, Christian, Jake, TJ");
				jd.add(text,BorderLayout.CENTER);
				jd.setModal(true);
				jd.setVisible(true);				
			}
		});
		
		exit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		addWindowListener(new WindowAdapter(){
            @SuppressWarnings("unused")
			public void windowClosing(WindowEvent e){

                System.out.println("call");
            	c.netClient.closeConnection();
                System.exit(0);
            }
        });
		
	}
	
	private void createGUI(){
		setSize(605,620);
		setResizable(false);
		setLocationRelativeTo(null);
				
		centerPanel.setLayout(null);

//		if (!mute)
//			play.start();
//		else
//			play.stopping();
//		
		
		 try {
			sequencer = MidiSystem.getSequencer();
	        sequencer.open();
	        InputStream is = new BufferedInputStream(new FileInputStream(new File("images/Video_Game_Themes_-_Final_Fantasy_2.mid")));
	        sequencer.setSequence(is);
	        sequencer.start();
		} catch (MidiUnavailableException  | IOException | InvalidMidiDataException e1) {
			e1.printStackTrace();
		}
	        
	        
		
		Dimension a = registration.getPreferredSize();
		logIn.setBounds(245, 160, a.width, a.height);
		centerPanel.add(logIn);
		
		registration.setBounds(245, 210, a.width, a.height);		
		centerPanel.add(registration);
		
		rules.setBounds(245, 260, a.width, a.height);
		centerPanel.add(rules);
		
		options.setBounds(245, 310, a.width, a.height);
		centerPanel.add(options);
		
		credits.setBounds(245, 360, a.width, a.height);
		centerPanel.add(credits);
		
		exit.setBounds(245, 410, a.width, a.height);
		centerPanel.add(exit);
		
		water.setPreferredSize(new Dimension(100, 100));
		Dimension c = water.getPreferredSize();
		water.setBounds(22, 200, c.width, c.height);
		centerPanel.add(water);

		lightning.setPreferredSize(new Dimension(100, 100));
		Dimension d = lightning.getPreferredSize();
		lightning.setBounds(250, 30, d.width, d.height);
		centerPanel.add(lightning);
		
		earth.setBounds(480, 200, d.width, d.height);
		centerPanel.add(earth);
		
		fire.setBounds(380, 460, d.width, d.height);
		centerPanel.add(fire);
		
		ice.setBounds(120, 460, d.width, d.height);
		centerPanel.add(ice);
		
		mainImage.setPreferredSize(new Dimension(600, 600));
		Dimension e = mainImage.getPreferredSize();
		mainImage.setBounds(0, 0, e.width, e.height);
		centerPanel.add(mainImage);
		
		add(centerPanel, BorderLayout.CENTER);
		
		new animations();

	}
	
}
