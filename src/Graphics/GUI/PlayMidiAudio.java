package Graphics.GUI;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequencer;

import Gameplay.Server.Player;

public class PlayMidiAudio extends Thread{

	private Sequencer sequencer;
	
    public PlayMidiAudio() throws Exception {

        sequencer = MidiSystem.getSequencer();
        sequencer.open();
        InputStream is = new BufferedInputStream(new FileInputStream(new File("images/Video_Game_Themes_-_Final_Fantasy_2.mid")));
        sequencer.setSequence(is);
    }
    
    public void run(){
        sequencer.start();
    }
    
    public void stopping(){
    	sequencer.stop();
    }
}
