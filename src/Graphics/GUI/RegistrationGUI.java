package Graphics.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.sun.glass.events.WindowEvent;

import Gameplay.Client.Client;

public class RegistrationGUI extends JFrame{

	private static final long serialVersionUID = 1L;
	private JPanel centerPanel;
	private JButton confirm, cancel;
	private JTextField usernameField, passwordField, emailField;
	private JLabel usernameLabel, passwordLabel, emailLabel, agreeLabel;
	private JCheckBox agree;
	private JLabel water, earth, fire, ice, lightning, mainImage;
	private Water waterThread;
	private Lightning lightningThread;
	private Earth earthThread;
	private Ice iceThread;
	private Fire fireThread;
	private ImageIcon pic;
	
	//Client
	public Client c;
	
	public RegistrationGUI( Client c){
		super("Mage Rage - Registration");
		this.c = c;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initialize();
		addActions();
		createGUI();
	}
	
	private class animations extends Thread{
		public animations(){
			this.start();
		}
		
		public void run(){
			while(true){
				water.repaint();
				centerPanel.invalidate();
				centerPanel.repaint();		
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		}
		
	}
	
	private void initialize(){
		centerPanel = new JPanel();
		confirm = new JButton("Confirm");
		cancel = new JButton("Cancel");
		usernameField = new JTextField(15);
		passwordField = new JTextField(15);
		emailField = new JTextField(15);
		agree = new JCheckBox();
		usernameLabel = new JLabel("Username: ");
		passwordLabel = new JLabel("Password: ");
		emailLabel = new JLabel("Email: ");
		agreeLabel = new JLabel("I agree with the terms and license of this game");
		
		waterThread = new Water();
		Thread t1 = new Thread(waterThread);
		t1.start();

		lightningThread = new Lightning();
		Thread t2 = new Thread(lightningThread);
		t2.start();
		
		earthThread = new Earth();
		Thread t3 = new Thread(earthThread);
		t3.start();
		
		fireThread = new Fire();
		Thread t4 = new Thread(fireThread);
		t4.start();
		
		iceThread = new Ice();
		Thread t5 = new Thread(iceThread);
		t5.start();
		
		pic = new ImageIcon("images/pentagon.png");
		
		water = new JLabel (){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				g.drawImage(waterThread.currentImage().getImage(), 0, 0, null);
				super.paintComponent(g);
			}
		};
		
		mainImage = new JLabel (){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				g.drawImage(pic.getImage(), 0, 0, null);
				super.paintComponent(g);
			}
		};
		
		lightning = new JLabel (){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				g.drawImage(lightningThread.currentImage().getImage(), 0, 0, null);
				super.paintComponent(g);
			}
		};
		
		earth = new JLabel (){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				g.drawImage(earthThread.currentImage().getImage(), 0, 0, null);
				super.paintComponent(g);
			}
		};
				
		fire = new JLabel (){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				g.drawImage(fireThread.currentImage().getImage(), 0, 0, null);
				super.paintComponent(g);
			}
		};
		
		ice = new JLabel (){
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g){
				g.drawImage(iceThread.currentImage().getImage(), 0, 0, null);
				super.paintComponent(g);
			}
		};
	}
	
	public void run (){
		while(true){
			water.repaint();
			centerPanel.invalidate();
			centerPanel.repaint();		
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	
	}
	
	private void addActions(){
		confirm.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				//System.out.println("INSERT INTO wp_users(user_login, user_email, user_pass) Values(");
				if(agree.isSelected() == false){
					JOptionPane.showMessageDialog(null, "You did not Agree to our terms!", "Oops!",JOptionPane.PLAIN_MESSAGE);
				}else if(usernameField.getText().equals("")){
					JOptionPane.showMessageDialog(null, "You did not enter a username!", "Oops!",JOptionPane.PLAIN_MESSAGE);
				}else if(passwordField.getText().equals("") || emailField.getText().equals("")){
					JOptionPane.showMessageDialog(null, "You left a field empty!", "Oops!",JOptionPane.PLAIN_MESSAGE);
				}else{
					c.netClient.sendReg(usernameField.getText(), passwordField.getText(), emailField.getText());				
				}
				//System.out.println(usernameField.getText());

			}
		});
				
		cancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				c.gotoMainScreen();
			}
		});
		
		/*
		addWindowListener(new WindowAdapter(){
            @SuppressWarnings("unused")
			public void windowClosing(WindowEvent e){
            	c.netClient.closeConnection();
                System.exit(0);
            }
        });
		*/
	}
	
	private void createGUI(){
		setSize(605,620);
		setResizable(false);
		setLocationRelativeTo(null);
		
		centerPanel.setLayout(null);
		
		Dimension label4Dimension = emailLabel.getPreferredSize();
		emailLabel.setBounds(165, 275, label4Dimension.width, label4Dimension.height);
		centerPanel.add(emailLabel);
		
		Dimension label5Dimension = usernameLabel.getPreferredSize();
		usernameLabel.setBounds(165, 225, label5Dimension.width, label5Dimension.height);
		centerPanel.add(usernameLabel);
		
		Dimension label6Dimension = passwordLabel.getPreferredSize();
		passwordLabel.setBounds(165, 325, label6Dimension.width, label6Dimension.height);
		centerPanel.add(passwordLabel);
		
		Dimension label7Dimension = agreeLabel.getPreferredSize();
		agreeLabel.setBounds(180, 375, label7Dimension.width, label7Dimension.height);
		centerPanel.add(agreeLabel);
		
		Dimension field3Dimension = emailField.getPreferredSize();
		emailField.setBounds(250, 275, field3Dimension.width, field3Dimension.height);
		centerPanel.add(emailField);
		
		Dimension field4Dimension = usernameField.getPreferredSize();
		usernameField.setBounds(250, 225, field4Dimension.width, field4Dimension.height);
		centerPanel.add(usernameField);
		
		Dimension field5Dimension = passwordField.getPreferredSize();
		passwordField.setBounds(250, 325, field5Dimension.width, field5Dimension.height);
		centerPanel.add(passwordField);
		
		Dimension field6Dimension = agree.getPreferredSize();
		agree.setBounds(150, 375, field6Dimension.width, field6Dimension.height);
		centerPanel.add(agree);
		
		Dimension field7Dimension = confirm.getPreferredSize();
		confirm.setBounds(200, 405, field7Dimension.width, field7Dimension.height);
		centerPanel.add(confirm);
		
		Dimension field8Dimension = cancel.getPreferredSize();
		cancel.setBounds(300, 405, field8Dimension.width, field8Dimension.height);
		centerPanel.add(cancel);
		
		water.setPreferredSize(new Dimension(100, 100));
		Dimension c = water.getPreferredSize();
		water.setBounds(22, 200, c.width, c.height);
		centerPanel.add(water);

		lightning.setPreferredSize(new Dimension(100, 100));
		Dimension d = lightning.getPreferredSize();
		lightning.setBounds(250, 30, d.width, d.height);
		centerPanel.add(lightning);
		
		earth.setBounds(480, 200, d.width, d.height);
		centerPanel.add(earth);
		
		fire.setBounds(380, 460, d.width, d.height);
		centerPanel.add(fire);
		
		ice.setBounds(120, 460, d.width, d.height);
		centerPanel.add(ice);
		
		mainImage.setPreferredSize(new Dimension(600, 600));
		Dimension e = mainImage.getPreferredSize();
		mainImage.setBounds(0, 0, e.width, e.height);
		centerPanel.add(mainImage);
		
		add(centerPanel, BorderLayout.CENTER);
		
		new animations();
	}
	
	public void showFailDialog(){
		JOptionPane.showMessageDialog(this, "Username Already Exists! Please choose another.", "Oops!",JOptionPane.PLAIN_MESSAGE);
		return;
	}
	
	public void registerSuccess(){
		JOptionPane.showMessageDialog(null, "You have succcessfully created a new account!", "Welcome", JOptionPane.PLAIN_MESSAGE);
		c.gotoMainScreen();
		return;
	}
	
}
