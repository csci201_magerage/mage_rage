package Graphics.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import com.sun.glass.events.WindowEvent;

import Gameplay.Client.Client;
import Networking.Client.RoomInfo;
import Networking.Client.UserInfo;

public class LobbyGUI extends JFrame{

	private static final long serialVersionUID = 1;
	private JPanel centerPanel;
	private JButton join, createGame, profile, exit, send, refresh, privateMsg;
	private JLabel inactive, active, welcome, currentPlayer;
	private JLabel [] games;
	private JRadioButton [] selection;
	private ButtonGroup selectionGroup;
	private ImageIcon background;
	private JCheckBox inactiveBox, activeBox;
	private JTextArea lobbyChat;
	private JTextField lobbyChatField;
	private JScrollPane scrollPane;
	
	private DefaultTableModel tableData;
	private JTable userTable;
	private JScrollPane userScrollPane;
	
	private RoomInfo[] roomList;
	private UserInfo[] userList;
	
	private Client c;
	private boolean waiting;
	
	private StatsDialog sDialog;
	
	public LobbyGUI(Client c){
		super("Mage Rage - Lobby");

		this.c = c;
		waiting = false;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initialize();
		addActions();
		createGUI();
		
		
	}
	
	
	
	public void initialize(){
		join = new JButton("JOIN");
		createGame = new JButton("CREATE");
		profile = new JButton("PROFILE");
		exit = new JButton("EXIT");
		send = new JButton("Send");
		refresh = new JButton ("REFRESH");
		privateMsg = new JButton("SEND MESSAGE");
		
		currentPlayer = new JLabel("Current Players in Lobby:");
		inactive = new JLabel("Inactive Games");
		welcome = new JLabel("Welcome to Mage Rage " + c.netClient.getUsername());
		selection = new JRadioButton[4];
		games = new JLabel[4];
		games[0] = new JLabel();
		games[1] = new JLabel();
		games[2] = new JLabel();
		games[3] = new JLabel();

		String [] columnNames = {"Username"};
		Object[][] data = {{""},
		};
	
		tableData = new DefaultTableModel(data,columnNames){
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column)
			    {
			      return false;
			    }
		};
		userTable = new JTable(tableData);
		userScrollPane = new JScrollPane(userTable);
		//userTable.setSize(new Dimension(300,30));
		
		active = new JLabel("Active Games");
		inactiveBox = new JCheckBox();
		activeBox = new JCheckBox();

		
		lobbyChat = new JTextArea(7,55);
		lobbyChat.setText("");
		lobbyChat.setEditable(false);
		lobbyChat.setLineWrap(true);
		scrollPane = new JScrollPane(lobbyChat, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		lobbyChatField = new JTextField(50);
		try {
			BufferedImage pic = ImageIO.read(new File("images/LobbyFrame.png"));
			background = new ImageIcon(pic);
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		centerPanel = new JPanel(){
			private static final long serialVersionUID = 1L;
			public void paintComponent(Graphics g){
			     super.paintComponent(g);
			     g.drawImage(background.getImage(), 0, 0, this);
			}
		};
		
		Border blackline = BorderFactory.createLineBorder(Color.red);
		TitledBorder title;
		title = BorderFactory.createTitledBorder(
				blackline, "Lobby Chat");
		title.setTitleJustification(TitledBorder.LEFT);
		lobbyChat.setBorder(title);
		
		//Selections
		selection[0] = new JRadioButton(ImageLibrary.PurpleMage);
		selection[1] = new JRadioButton(ImageLibrary.BlueMage);
		selection[2] = new JRadioButton(ImageLibrary.RedMage);
		selection[3] = new JRadioButton(ImageLibrary.GreenMage);
		selectionGroup = new ButtonGroup();
		for (int i=0;i<4;i++){
			selectionGroup.add(selection[i]);
			selection[i].setSelectedIcon(ImageLibrary.Tick);
			selection[i].setOpaque(false);
		}
		
		
		
		inactiveBox.doClick();
		activeBox.doClick();
		updateRoomList();
		updateUserList();
	}
	
	public void addActions(){
		
		//Active and inactive boxes
		activeBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				for (int i=0; i<roomList.length; i++){
					if (activeBox.isSelected()){
						if (roomList[i].isActive()){
							games[i].setVisible(true);
							selection[i].setVisible(true);
						}
					}else if(!activeBox.isSelected()){
						if (roomList[i].isActive()){
							games[i].setVisible(false);
							selection[i].setVisible(false);
						}
					}
				}
				
			}
				
		});
		
		inactiveBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
	
				for (int i=0; i<roomList.length; i++){
					if (inactiveBox.isSelected()){
						if (!roomList[i].isActive()){
						games[i].setVisible(true);
						selection[i].setVisible(true);
						}
					}else if (!inactiveBox.isSelected()){
						if (!roomList[i].isActive()){
						games[i].setVisible(false);
						selection[i].setVisible(false);
						}
					}
				}
			}			
		});
		
		//game labels
		for (int i=0;i<4;i++){
			games[i].addMouseListener(new gameLabelListener(i));
		}
		
		//Chat related
		send.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if (!lobbyChatField.getText().equals("")){
					String toSend = lobbyChatField.getText();
					if (toSend.charAt(0) == '['){
						String[] subStr = toSend.split("]");
						subStr[0] = subStr[0].replace("[", "");
						if (subStr[0].equals(c.netClient.getUsername())){
							lobbyChat.append("[SYSTEM] You can't send message to yourself.");
						}else{
							
							String combinedMsg = "";
							for (int i=1;i<subStr.length;i++){
								combinedMsg = combinedMsg + subStr[i];
							}
							c.netClient.sendPrivateMessage(subStr[0], combinedMsg);
						}
					}else{
						c.netClient.sendPublicMessage(lobbyChatField.getText());
						lobbyChatField.setText("");
					}
				}
			}			
		});
		
		lobbyChatField.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				send.doClick();
			}
			
		});
		
		

		//Buttons
		refresh.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				updateRoomList();
				updateUserList();
			}
		});

		join.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
			int index = -1;
			for (int i=0;i<4;i++){
				if (selection[i].isSelected()){
					index = i;
					break;
				}
			}
			if (index == -1){
				JOptionPane.showMessageDialog(null, "Select a game to join.", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			String result = c.netClient.joinRoom(index);
			if (result.equals("SUCCESS")){
				startWaiting();
			}else if(result.equals("ROOMACTIVE")){
				JOptionPane.showMessageDialog(null, "This game is active!", "Error", JOptionPane.ERROR_MESSAGE);
				updateRoomList();
				return;
			}else if(result.equals("ROOMFULL")){
				JOptionPane.showMessageDialog(null, "This game is full!", "Error", JOptionPane.ERROR_MESSAGE);
				updateRoomList();
				return;
			}
			}
		});
		
		profile.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if (c.netClient.isGuest()){
					JOptionPane.showMessageDialog(null, "You must register to have a profile!", "Error", JOptionPane.ERROR_MESSAGE);
				}else{
					sDialog = new StatsDialog();
					c.netClient.requestUserData();
				}
			
			}
					
		});
		
		createGame.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				int index = -1;
				for (int i=0;i<4;i++){
					if (selection[i].isSelected()){
						index = i;
						break;
					}
				}
				if (index == -1){
					JOptionPane.showMessageDialog(null, "Select a room to create.", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				String result = c.netClient.createRoom(index);
				System.out.println(result);
				if (result.equals("SUCCESS")){
					c.gotoGameWindow();
				}else if(result.equals("ROOMEXISTS")){
					JOptionPane.showMessageDialog(null, "A game has already been created!", "Error", JOptionPane.ERROR_MESSAGE);
					updateRoomList();
					return;
				}
				}			
		});
		
		exit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if (waiting){
					int index = -1;
					for (int i=0;i<4;i++){
						if (selection[i].isSelected()){
							index = i;
							break;
						}
					}
					c.netClient.leaveRoom(index);
					stopWaiting();
				}else{
					c.gotoMainScreen();
				}
				
			}
			
			}
		);
		
		
		privateMsg.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				Object tableSelection = userTable.getValueAt(userTable.getSelectedRow(), userTable.getSelectedColumn());
				String targetUser;
				if (tableSelection instanceof String){
					targetUser = (String)tableSelection;
					lobbyChatField.setText("[" + targetUser + "]");
				}
			}			
		});
				
	}
		
	public void createGUI(){
		setSize(1000,600);
		setResizable(false);
		setLocationRelativeTo(null);
				
		centerPanel.setLayout(null);

		this.getRootPane().setDefaultButton(send);

		userScrollPane.setPreferredSize(new Dimension(200,400));
		Dimension u = userScrollPane.getPreferredSize();
		userScrollPane.setBounds(740,90,u.width,u.height);
		
		
		Dimension a = welcome.getPreferredSize();
		welcome.setBounds(720, 30, a.width, a.height);
		currentPlayer.setBounds(720, 60, a.width, a.height);
		
		//Buttons
		Dimension b = refresh.getPreferredSize();
		refresh.setBounds(50, 320, b.width, b.height);	
		join.setBounds(170, 320, b.width, b.height);
		//createGame.setBounds(290, 320, b.width, b.height);
		profile.setBounds(290, 320, b.width, b.height);
		exit.setBounds(410, 320, b.width, b.height);
		
		//Active and inactive labels & boxes
		Dimension d = active.getPreferredSize();
		active.setBounds(180, 20, d.width, d.height);
		Dimension d1 = inactive.getPreferredSize();
		inactive.setBounds(320, 20, d1.width, d1.height);
		Dimension d2 = inactiveBox.getPreferredSize();
		activeBox.setBounds(155, 20, d2.width, d2.height); 
		inactiveBox.setBounds(295, 20, d2.width, d2.height); 

		Dimension f = lobbyChat.getPreferredSize();
		scrollPane.setBounds(75, 380, f.width, f.height);
		centerPanel.add((scrollPane));	

		Dimension f1 = lobbyChatField.getPreferredSize();
		lobbyChatField.setBounds(75, 535, f1.width, f1.height);
		centerPanel.add(lobbyChatField);	
		Dimension f2 = send.getPreferredSize();
		send.setBounds(640, 532, f2.width, f2.height);
		centerPanel.add(send);
		
		selection[0].setPreferredSize(new Dimension(50, 50));
		Dimension l = selection[0].getPreferredSize();
		selection[0].setBounds(50, 50, l.width, l.height);
		selection[1].setBounds(50, 110, l.width, l.height);
		selection[2].setBounds(50, 170, l.width, l.height);
		selection[3].setBounds(50, 230, l.width, l.height);

		Border border = BorderFactory.createLineBorder(Color.BLACK);
	    games[0].setBorder(border);
	    games[1].setBorder(border);
	    games[2].setBorder(border);
	    games[3].setBorder(border);

		games[0].setPreferredSize(new Dimension(400, 50));
		Dimension g = games[0].getPreferredSize();
		games[0].setBounds(110, 50, g.width, g.height);
		games[1].setBounds(110, 110, g.width, g.height);
		games[2].setBounds(110, 170, g.width, g.height);
		games[3].setBounds(110, 230, g.width, g.height);

		privateMsg.setBounds(780,500,privateMsg.getPreferredSize().width,privateMsg.getPreferredSize().height);
		
		centerPanel.add(userScrollPane);
		centerPanel.add(currentPlayer);
		centerPanel.add(welcome);
		centerPanel.add(join);
		centerPanel.add(createGame);
		centerPanel.add(profile);
		centerPanel.add(refresh);
		centerPanel.add(inactive);
		centerPanel.add(active);
		centerPanel.add(exit);
		centerPanel.add(inactiveBox);
		centerPanel.add(activeBox);
		centerPanel.add(selection[0]);
		centerPanel.add(selection[1]);
		centerPanel.add(selection[2]);
		centerPanel.add(selection[3]);
		centerPanel.add(games[0]);
		centerPanel.add(games[1]);
		centerPanel.add(games[2]);
		centerPanel.add(games[3]);
		centerPanel.add(privateMsg);

		add(centerPanel, BorderLayout.CENTER);

	}
	
	
	
	public void addPublicMessage(String username, String msg){
		lobbyChat.append("[Lobby]"+ username + ": "+ msg + "\n");
	}
	
	public void addPrivateMessage(String username, String msg){
		lobbyChat.append("[PM]" + username + ": " + msg + "\n");
	}
	
	public void updateRoomList(){
		if (!waiting){
			c.netClient.requestRoomList();
		}
	}
	
	public void updateUserList(){
			c.netClient.requestUserList();
	}
	
	public void updateRoomListGUI(RoomInfo[] roomList){
		//this.roomList = roomList;
		for (int i=0;i<roomList.length;i++){
			if (roomList[i].getPlayerCount() == 0){
				games[i].setText("Empty");
			}else{
				if (roomList[i].isActive()){
					games[i].setText("Players:" + roomList[i].getPlayerCount() + " Game has started");
				}else{
					games[i].setText("Players:" + roomList[i].getPlayerCount() + " Waiting for players");
				}
			}
		}
	}
	
	public void updateUserListGUI(UserInfo[] userList){
		//this.userList = userList;
		tableData.setRowCount(0);
		if (!waiting){
			for (int i=0;i<userList.length;i++){
				tableData.addRow(new Object[]{userList[i].getUsername()});
			}
		}else{
			
			for (int i=0;i<userList.length;i++){
				games[i].setText("Player" + (i+1) + ": " + userList[i].getUsername());
			}
			for (int i=userList.length;i<4;i++){
				games[i].setText("Waiting for a player to join");
			}
		}
	}
	
	
	
	
	
	public void updateAll(){
		welcome.setText("Welcome to Mage Rage " + c.netClient.getUsername());
		updateRoomList();
		updateUserList();
	}

	
	private void startWaiting(){
		
		waiting = true;
		join.setEnabled(false);
		activeBox.setEnabled(false);
		inactiveBox.setEnabled(false);
		for (int i=0;i<4;i++){
			selection[i].setEnabled(false);
			games[i].setVisible(true);
		}
		updateUserList();
	}
	
	
	private void stopWaiting(){
		waiting = false;
		join.setEnabled(true);
		activeBox.setEnabled(true);
		inactiveBox.setEnabled(true);
		for (int i=0;i<4;i++){
			selection[i].setEnabled(true);
		}
	}
	
	public void startGame(){
		//System.out.println("Starting game");
		stopWaiting();
		c.gotoGameWindow();
		
	}
	
	
	//Customize actionlisteners
	class gameLabelListener extends MouseAdapter{
		int index;
		
		public gameLabelListener(int index){
			this.index = index;
		}
		
		public void mouseClicked(MouseEvent me){
			selection[index].doClick();
		}
	
	}
	
	class StatsDialog  extends JDialog{
		private static final long serialVersionUID = 1L;;
		private JTextArea content;
		
		public StatsDialog(){
			setSize(400,300);
			setLocationRelativeTo(null);
			setResizable(false);
			content = new JTextArea(40,30);
			content.setEditable(false);
			add(content);
			this.setVisible(true);
			//content.append("GROUPWORK");
		}
		
		public void addLine(String str){
			content.append(str + "\n");
		}
	
	}
	
	public void addLineToDialog(String str){
		if (sDialog != null){
			sDialog.addLine(str);
		}
	}
	
	
}