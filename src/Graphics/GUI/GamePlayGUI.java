package Graphics.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import com.sun.glass.events.WindowEvent;

import Gameplay.Client.Client;


public class GamePlayGUI extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private JPanel centerPanel, gridPanel, mainPlayerPanelAA, mainPlayerPanelBB, panelC, panelD, player1Panel, player2Panel, player3Panel, player1aPanel, player2aPanel, player3aPanel, mainPlayerPanelA, mainPlayerPanelB, mainPlayerPanelC, mainPlayerPanelD;
	private ImageIcon gameBoard, background, piece, podium, playerPiece1, playerPiece2, playerPiece3, playerPiece4, dice, cardBackImage, cardFrontImage, cardBackImage1, cardFrontImage1 , cardBackImage2, cardFrontImage2;
	private ImageIcon pieceInRange, podiumInRange, player1InRange, player2InRange, player3InRange, player4InRange;
	private JLabel cardBack, cardFront;
	private JLabel [] playerLabels, player1Labels, player2Labels, player3Labels, player1aLabels, player2aLabels, player3aLabels, mainPlayerChosenA, mainPlayerChosenB;
	private JLabel [][] boardLabels;
	private JButton playerButton1, playerButton2, playerButton3, playerButton4, diceButton, send, confirm;
	private JLabel [] mainPlayerLabelsA, mainPlayerLabelsB;
	private JComboBox<String> select1, select2, select3; 
	private JTextArea gameChat;
	private JTextField gameChatField;
	private JScrollPane scrollPane;
	private Client c;
	private JLabel[] playerLabelsA[];
	private JLabel[] playerLabelsB[];
	private int[] playerCountersA;
	private int[] playerCountersB;
	private int mainPlayerCounter;
	private JLabel [] playerHealth;
	private ImageIcon [] healthLabel; 
	private ImageIcon[] playerPieces; 
	private int player1x, player1y, player2x, player2y, player3x, player3y, player4x, player4y;
	private String choicing;
	private JLabel roundLabel, phaseLabel, logLabel;
	
	private ImageIcon comet, earthquake, firebreath, gust, hailstorm, heal, icespike, lightning, magerage, mist, roots, storm, teleport, tornado, waterjet;
	private ImageIcon movefast, moveslow, spell1, spell2, spell3;
	private ShrinkIcon dice1, dice2, dice3, dice4, dice5, dice6;
	private ShrinkIcon[] diceImages;
	private int count1, count2;
	private JButton cancel;
	
	public MakeMovesGUI inputWindow;
	public String userInput1;
	public String userInput2;
	
	public GamePlayGUI(Client c){
		super("Mage Rage - Playing");
		this.c = c;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initialize();
		addActions();
		createGUI();
	}
	
	//ANIMATION FUNCTIONS START
	public JLabel getBoardTile(int x, int y){
		if(validTile(x,y)){
			return boardLabels[x][y];
		}
		else{
			return null;
		}
	}
	
	public void setTargeted(JLabel tile){
		if(tile.getIcon().equals(piece)){
			tile.setIcon(pieceInRange);
		}
		else if(tile.getIcon().equals(podium)){
			tile.setIcon(podiumInRange);
		}
		else if(tile.getIcon().equals(playerPiece1)){
			tile.setIcon(player1InRange);
		}
		else if(tile.getIcon().equals(playerPiece2)){
			tile.setIcon(player2InRange);
		}
		else if(tile.getIcon().equals(playerPiece3)){
			tile.setIcon(player3InRange);
		}
		else if(tile.getIcon().equals(playerPiece4)){
			tile.setIcon(player4InRange);
		}
	}
	
	public void setUntargeted(JLabel tile){
		if(tile.getIcon().equals(pieceInRange)){
			tile.setIcon(piece);
		}
		else if(tile.getIcon().equals(podiumInRange)){
			tile.setIcon(podium);
		}
		else if(tile.getIcon().equals(player1InRange)){
			tile.setIcon(playerPiece1);
		}
		else if(tile.getIcon().equals(player2InRange)){
			tile.setIcon(playerPiece2);
		}
		else if(tile.getIcon().equals(player3InRange)){
			tile.setIcon(playerPiece3);
		}
		else if(tile.getIcon().equals(player4InRange)){
			tile.setIcon(playerPiece4);
		}
	}
	
	public boolean validTile(int x, int y){
		return (x>=0&&x<7&&y>=0&&y<7);
	}
	//ANIMATION FUNCTIONS END
	
	private void initialize(){
		roundLabel = new JLabel("Round Number 1");
		phaseLabel = new JLabel("Phase 1");
		logLabel = new JLabel("USERNAME1 attacked USERNAME2 with Hailstorm");

		inputWindow = null;
		choicing = "Player 1";
		count1 = count2 = 0;
		cancel = new JButton("Cancel your choices");
		cancel.setEnabled(false);
		playerCountersA = new int[4];
		playerCountersA[0] = 0;
		playerCountersA[1] = 0;
		playerCountersA[2] = 0;
		playerCountersA[3] = 0;
		
		playerCountersB = new int[4];
		playerCountersB[0] = 0;
		playerCountersB[1] = 0;
		playerCountersB[2] = 0;
		playerCountersB[3] = 0;
		
		mainPlayerCounter = 0;
		
		userInput1 = "null";
		userInput2 = "null";
		
		playerHealth = new JLabel[4];
		healthLabel = new ImageIcon[12];
		piece = new ImageIcon("images/piece.png");
		pieceInRange = new ImageIcon("images/piece_inRange.png");
		podium = new ImageIcon("images/podium.png");
		podiumInRange = new ImageIcon("images/podium_inRange.png");
		playerPiece1 = new ImageIcon("images/MR_RedMage1.png");
		player1InRange = new ImageIcon("images/MR_RedMage1_inRange.png");
		playerPiece2 = new ImageIcon("images/MR_PurpleMage1.png");
		player2InRange = new ImageIcon("images/MR_PurpleMage1_inRange.png");
		playerPiece3 = new ImageIcon("images/MR_BlueMage1.png");
		player3InRange = new ImageIcon("images/MR_BlueMage1_inRange.png");
		playerPiece4 = new ImageIcon("images/MR_GreenMage1.png");
		player4InRange = new ImageIcon("images/MR_GreenMage1_inRange.png");
		playerPieces = new ImageIcon[4];
		playerPieces[0] = playerPiece1;
		playerPieces[1] = playerPiece2;
		playerPieces[2] = playerPiece3;
		playerPieces[3] = playerPiece4;
		playerButton1 = new JButton(playerPiece1);
		playerButton2 = new JButton(playerPiece2);
		playerButton3 = new JButton(playerPiece3);
		playerButton4 = new JButton(playerPiece4);		
		
		healthLabel[0] = new ImageIcon("images/health1.png");
		healthLabel[1] = new ImageIcon("images/health2.png");
		healthLabel[2] = new ImageIcon("images/health3.png");
		healthLabel[3] = new ImageIcon("images/health4.png");
		healthLabel[4] = new ImageIcon("images/health5.png");
		healthLabel[5] = new ImageIcon("images/health6.png");
		healthLabel[6] = new ImageIcon("images/health7.png");
		healthLabel[7] = new ImageIcon("images/health8.png");
		healthLabel[8] = new ImageIcon("images/health9.png");
		healthLabel[9] = new ImageIcon("images/health10.png");
		healthLabel[10] = new ImageIcon("images/health11.png");
		healthLabel[11] = new ImageIcon("images/health12.png");
		playerHealth[0] = new JLabel(healthLabel[11]);
		playerHealth[1] = new JLabel(healthLabel[10]);
		playerHealth[2] = new JLabel(healthLabel[9]);
		playerHealth[3] = new JLabel(healthLabel[8]);

		dice1 = new ShrinkIcon("images/dice/side1.jpg");
		dice2 = new ShrinkIcon("images/dice/side2.jpg");
		dice3 = new ShrinkIcon("images/dice/side3.jpg");
		dice4 = new ShrinkIcon("images/dice/side4.jpg");
		dice5 = new ShrinkIcon("images/dice/side5.jpg");
		dice6 = new ShrinkIcon("images/dice/side6.jpg");
		
		diceImages = new ShrinkIcon[6];
		diceImages[0] = dice1;
		diceImages[1] = dice2;
		diceImages[2] = dice3;
		diceImages[3] = dice4;
		diceImages[4] = dice5;
		diceImages[5] = dice6;

		comet = new ShrinkIcon("images/spells/comet.png");
		earthquake = new ShrinkIcon("images/spells/earthquake.png");
		firebreath = new ShrinkIcon("images/spells/firebreath.png");
		gust = new ShrinkIcon("images/spells/gust.png");
		hailstorm = new ShrinkIcon("images/spells/hailstorm.png");
		heal = new ShrinkIcon("images/spells/heal.png");
		icespike = new ShrinkIcon("images/spells/icespike.png");
		lightning = new ShrinkIcon("images/spells/lightning.png");
		magerage = new ShrinkIcon("images/spells/magerage.png");
		mist = new ShrinkIcon("images/spells/mist.png");
		roots = new ShrinkIcon("images/spells/roots.png");
		storm = new ShrinkIcon("images/spells/storm.png");
		teleport = new ShrinkIcon("images/spells/teleport.png");
		tornado = new ShrinkIcon("images/spells/tornado.png");
		waterjet = new ShrinkIcon("images/spells/waterjet.png");
		
		movefast = new ShrinkIcon("images/decisions/movefast.png");
		moveslow = new ShrinkIcon("images/decisions/moveslow.png");
		spell1 = new ShrinkIcon("images/decisions/spell1.png");
		spell2 = new ShrinkIcon("images/decisions/spell2.png");
		spell3 = new ShrinkIcon("images/decisions/spell3.png");
		
		mainPlayerLabelsA = new JLabel[5];
		mainPlayerLabelsB = new JLabel[5];
		mainPlayerChosenA = new JLabel[3];
		mainPlayerChosenB = new JLabel[3];
		
		panelC = new JPanel();
		panelD = new JPanel();
		
		
		confirm = new JButton("Confirm Your Cards");
		confirm.setEnabled(false);


		send = new JButton("Send");
		cardBackImage = new ImageIcon("images/MR_DecisionCardBack.png");
		cardFrontImage = new ImageIcon("images/MR_SpellCardBack.png");
		cardBackImage1 = new ImageIcon("images/mage.png");
		cardFrontImage1 = new ImageIcon("images/decision.png");
		cardBackImage2 = new ImageIcon("images/mage2.png");
		cardFrontImage2 = new ImageIcon("images/decision2.png");
		gameChat = new JTextArea(5, 39);
		gameChat.setEditable(false);
		scrollPane = new JScrollPane(gameChat, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		gameChatField = new JTextField(30);
	
		Border blackline, blackline2;
		blackline2= BorderFactory.createLineBorder(Color.black);
		TitledBorder title;
		title = BorderFactory.createTitledBorder(
				blackline2, "Game Chat");
		title.setTitleJustification(TitledBorder.LEFT);
		
		blackline = BorderFactory.createLineBorder(Color.black);
		
		gameChat.setBorder(title);
		//gameChatField.setBorder(blackline);
		player1Panel = new JPanel();
		player2Panel = new JPanel();
		player3Panel = new JPanel();
		player1aPanel = new JPanel();
		player2aPanel = new JPanel();
		player3aPanel = new JPanel();
		mainPlayerPanelA = new JPanel();
		mainPlayerPanelB = new JPanel();
		mainPlayerPanelAA = new JPanel();
		mainPlayerPanelBB = new JPanel();
		mainPlayerPanelC= new JPanel();
		mainPlayerPanelD = new JPanel();
		mainPlayerPanelA.setLayout(new GridLayout(1,3));
		mainPlayerPanelB.setLayout(new GridLayout(1,3));
		mainPlayerPanelAA.setLayout(new GridLayout(1,2));
		mainPlayerPanelBB.setLayout(new GridLayout(1,2));
		mainPlayerPanelC.setLayout(new GridLayout(3,1));
		mainPlayerPanelD.setLayout(new GridLayout(3,1));

		player1Panel.setLayout(new GridLayout(3,1));
		player2Panel.setLayout(new GridLayout(3,1));
		player3Panel.setLayout(new GridLayout(3,1));
		player1aPanel.setLayout(new GridLayout(3,1));
		player2aPanel.setLayout(new GridLayout(3,1));
		player3aPanel.setLayout(new GridLayout(3,1));
		player1Labels = new JLabel[3];
		player2Labels = new JLabel[3];
		player3Labels = new JLabel[3];
		player1aLabels = new JLabel[3];
		player2aLabels = new JLabel[3];
		player3aLabels = new JLabel[3];

		
		for (int i=0; i<3; i++){
			player1Labels[i] = new JLabel(cardBackImage);
			player2Labels[i] = new JLabel(cardBackImage);
			player3Labels[i] = new JLabel(cardBackImage);
			mainPlayerChosenA[i] = new JLabel(cardBackImage);
			mainPlayerChosenA[i].setBorder(BorderFactory.createLineBorder(Color.RED));

			player1Labels[i].setBorder(BorderFactory.createLineBorder(Color.RED));
			player2Labels[i].setBorder(BorderFactory.createLineBorder(Color.RED));
			player3Labels[i].setBorder(BorderFactory.createLineBorder(Color.RED));
		}
		
		
		for (int i=0; i<3; i++){
			player1aLabels[i] = new JLabel(cardFrontImage);
			player2aLabels[i] = new JLabel(cardFrontImage);
			player3aLabels[i] = new JLabel(cardFrontImage);
			mainPlayerChosenB[i] = new JLabel(cardFrontImage);
			mainPlayerChosenB[i].setBorder(BorderFactory.createLineBorder(Color.BLUE));

			player1aLabels[i].setBorder(BorderFactory.createLineBorder(Color.BLUE));
			player2aLabels[i].setBorder(BorderFactory.createLineBorder(Color.BLUE));
			player3aLabels[i].setBorder(BorderFactory.createLineBorder(Color.BLUE));
		}

		for (int i=0; i<3; i++){
			player1Panel.add(player1Labels[i]);
			player2Panel.add(player2Labels[i]);
			player3Panel.add(player3Labels[i]);
			player1aPanel.add(player1aLabels[i]);
			player2aPanel.add(player2aLabels[i]);
			player3aPanel.add(player3aLabels[i]);		
			mainPlayerPanelC.add(mainPlayerChosenA[i]);
			mainPlayerPanelD.add(mainPlayerChosenB[i]);
		}
				
		for (int i=0; i<5; i++){
			mainPlayerLabelsA[i] = new JLabel(cardFrontImage);
			mainPlayerLabelsB[i] = new JLabel(cardBackImage);
			mainPlayerLabelsA[i].setBorder(BorderFactory.createLineBorder(Color.BLUE));
			mainPlayerLabelsB[i].setBorder(BorderFactory.createLineBorder(Color.RED));
		}
		
		mainPlayerPanelA.add(mainPlayerLabelsA[0]);
		mainPlayerPanelA.add(mainPlayerLabelsA[1]);
		mainPlayerPanelA.add(mainPlayerLabelsA[2]);

		mainPlayerPanelB.add(mainPlayerLabelsB[0]);
		mainPlayerPanelB.add(mainPlayerLabelsB[1]);
		mainPlayerPanelB.add(mainPlayerLabelsB[2]);

		mainPlayerPanelAA.add(mainPlayerLabelsA[3]);
		mainPlayerPanelAA.add(mainPlayerLabelsA[4]);

		mainPlayerPanelBB.add(mainPlayerLabelsB[3]);
		mainPlayerPanelBB.add(mainPlayerLabelsB[4]);

		gridPanel = new JPanel();
		boardLabels = new JLabel[7][7];
		gridPanel.setLayout(new GridLayout(7,7));
		for (int i=0; i<7; i++){
			for(int j=0; j<7; j++){
				boardLabels[i][j] = new JLabel(piece);
				gridPanel.add(boardLabels[i][j]);
			}
		}
		
		cardFront = new JLabel(cardFrontImage);
		cardBack = new JLabel(cardBackImage);
		
		boardLabels[1][1].setIcon(playerPieces[0]);
		player1x = 1;
		player1y = 1;
		boardLabels[1][5].setIcon(playerPieces[1]);
		player2x = 1;
		player2y = 5;
		boardLabels[5][1].setIcon(playerPieces[2]);
		player3x = 5;
		player3y = 1;
		boardLabels[5][5].setIcon(playerPieces[3]);
		player4x = 5;
		player4y = 5;
		
		gameBoard = new ImageIcon("images/MR_Board2.png");
		background = new ImageIcon("images/background.png");
		dice = new ImageIcon("images/dice.png");

		diceButton = new JButton(dice1);
		
		playerLabels = new JLabel[4];
		playerLabels[0] = new JLabel("You");
		playerLabels[1] = new JLabel("Alex");
		playerLabels[2] = new JLabel("WenFei");
		playerLabels[3] = new JLabel("Jake");
	
		String [] choices = {"Player 1", "Player 2", "Player 3", "Player 4"};
		select1 = new JComboBox<String>(choices);
		select2 = new JComboBox<String>(choices);
		select3 = new JComboBox<String>(choices);
		
		playerLabelsA = new JLabel[4][];
		playerLabelsA[0] = mainPlayerChosenB;
		playerLabelsA[1] = player1aLabels;
		playerLabelsA[2] = player2aLabels;
		playerLabelsA[3] = player3aLabels;
		
		playerLabelsB = new JLabel[4][];
		playerLabelsB[0] = mainPlayerChosenA;
		playerLabelsB[1] = player1Labels;
		playerLabelsB[2] = player2Labels;
		playerLabelsB[3] = player3Labels;

		centerPanel = new JPanel(){
			public void paintComponent(Graphics g){
			     super.paintComponent(g);
			     g.drawImage(background.getImage(), 0, 0, this);
			     //g.drawImage(gameBoard.getImage(), 350, 70, this);
			}
		};
	}
	
	private void addActions(){
		
		select1.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent ie){
				if(ie.getStateChange() == ItemEvent.SELECTED){
					choicing = (String) ie.getItem();
					if (choicing.equals("Player 1")){
						setNameAndMage(0);
					}
					else if (choicing.equals("Player 2")){
						setNameAndMage(1);
					}
					else if (choicing.equals("Player 3")){
						setNameAndMage(2);
					}
					else if (choicing.equals("Player 4")){
						setNameAndMage(3);
					}
				}
			}
		});
		
		confirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				// TODO - Send Cards to Server (A = Decision, B = Spell)
				StringBuilder sb = new StringBuilder();
				
				for(int i = 0; i < 3; i++) {
					if(mainPlayerChosenA[i].getIcon() == movefast) {
						sb.append(":movement_fast");
					} else if(mainPlayerChosenA[i].getIcon() == moveslow) {
						sb.append(":movement_slow");
					} else if(mainPlayerChosenA[i].getIcon() == spell1) {
						sb.append(":spell_1");
					} else if(mainPlayerChosenA[i].getIcon() == spell2) {
						sb.append(":spell_2");
					} else if(mainPlayerChosenA[i].getIcon() == spell3) {
						sb.append(":spell_3");
					}
					
					if(mainPlayerChosenB[i].getIcon() == comet) {
						sb.append(":comet");
					} else if(mainPlayerChosenB[i].getIcon() == earthquake) {
						sb.append(":earthquake");
					} else if(mainPlayerChosenB[i].getIcon() == firebreath) {
						sb.append(":fire_breath");
					} else if(mainPlayerChosenB[i].getIcon() == gust) {
						sb.append(":gust");
					} else if(mainPlayerChosenB[i].getIcon() == hailstorm) {
						sb.append(":hailstorm");
					} else if(mainPlayerChosenB[i].getIcon() == heal) {
						sb.append(":heal");
					} else if(mainPlayerChosenB[i].getIcon() == icespike) {
						sb.append(":ice_spike");
					} else if(mainPlayerChosenB[i].getIcon() == lightning) {
						sb.append(":lightning");
					} else if(mainPlayerChosenB[i].getIcon() == magerage) {
						sb.append(":mage_rage");
					} else if(mainPlayerChosenB[i].getIcon() == mist) {
						sb.append(":mist");
					} else if(mainPlayerChosenB[i].getIcon() == roots) {
						sb.append(":roots");
					} else if(mainPlayerChosenB[i].getIcon() == storm) {
						sb.append(":storm");
					} else if(mainPlayerChosenB[i].getIcon() == teleport) {
						sb.append(":teleport");
					} else if(mainPlayerChosenB[i].getIcon() == tornado) {
						sb.append(":tornado");
					} else if(mainPlayerChosenB[i].getIcon() == waterjet) {
						sb.append(":water_jet");
					}
				}
				
				GamePlayGUI.this.c.clientGame.sendMessage(sb);
				GamePlayGUI.this.confirm.setEnabled(false);
			}
		});
		
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				for (int i=0; i<5; i++){
					mainPlayerLabelsA[i].setEnabled(true);		
					mainPlayerLabelsB[i].setEnabled(true);
				}
				for (int i=0; i<3; i++){
					mainPlayerChosenA[i].setIcon(cardBackImage);		
					mainPlayerChosenB[i].setIcon(cardFrontImage);
				}
				count1 = 0;
				count2 = 0;
				GamePlayGUI.this.cancel.setEnabled(false);
				GamePlayGUI.this.confirm.setEnabled(false);
			}
		});
		
		
		diceButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				Random random = new Random();
				int side = random.nextInt(6);
				GamePlayGUI.this.diceButton.setIcon(diceImages[side]);
			}
		});
		
		
		for (int i=0; i<5; i++){
			final int a = i;
			mainPlayerLabelsB[i].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			// TODO - Decision Cards
			mainPlayerLabelsB[i].addMouseListener(new MouseAdapter(){
				public void mouseClicked(MouseEvent e) {
					if (count2 < 3){
						GamePlayGUI.this.cancel.setEnabled(true);
						mainPlayerChosenA[count2].setIcon(mainPlayerLabelsB[a].getIcon());
						mainPlayerLabelsB[a].setEnabled(false);		
						count2++;
					}
					
					if(count2 == 3) {
						GamePlayGUI.this.confirm.setEnabled(true);
					}
					
					
				}
			});
			
			mainPlayerLabelsA[i].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			// TODO - Spell Cards
			mainPlayerLabelsA[i].addMouseListener(new MouseAdapter(){
				public void mouseClicked(MouseEvent e) {
					if (count1 < 3){
						GamePlayGUI.this.cancel.setEnabled(true);
						mainPlayerChosenB[count1].setIcon(mainPlayerLabelsA[a].getIcon());
						mainPlayerLabelsA[a].setEnabled(false);
						count1++;
					}		
					
					if(count1 == 3) {
						GamePlayGUI.this.confirm.setEnabled(true);
					}
				}
				
//				public void mouseReleased(MouseEvent e) {
//				    if(SwingUtilities.isRightMouseButton(e)){
//				    	ImageIcon x = (ImageIcon) mainPlayerLabelsA[a].getIcon();
//				    	System.out.println(x);
//				        new showCard(x);
//				    }
//				} 
			});
			
			
			send.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					if (!gameChatField.getText().equals("")){
						String toSend = gameChatField.getText();
						if (toSend.charAt(0) == '['){
							String[] subStr = toSend.split("]");
							subStr[0] = subStr[0].replace("[", "");
							if (subStr[0].equals(c.netClient.getUsername())){
								gameChat.append("[SYSTEM] You can't send message to yourself.");
							}else{
								
								String combinedMsg = "";
								for (int i=1;i<subStr.length;i++){
									combinedMsg = combinedMsg + subStr[i];
								}
								c.netClient.sendPrivateMessage(subStr[0], combinedMsg);
							}
						}else{
							c.netClient.sendRoomMessage(gameChatField.getText());
							gameChatField.setText("");
						}
					}
				}			
			});
			
		}
		
		addWindowListener(new WindowAdapter(){
            @SuppressWarnings("unused")
			public void windowClosing(WindowEvent e){
            	c.netClient.closeConnection();
                System.exit(0);
            }
        });
	}
	
	
	
	private void setNameAndMage(int index){
		if (index == 0){
			centerPanel.remove(playerButton2);
			centerPanel.remove(playerLabels[1]);
			centerPanel.remove(player1Panel);
			centerPanel.remove(player1aPanel);
			centerPanel.remove(playerHealth[1]);
			
			centerPanel.remove(playerButton3);
			centerPanel.remove(playerLabels[2]);
			centerPanel.remove(player2Panel);
			centerPanel.remove(player2aPanel);
			centerPanel.remove(playerHealth[2]);
			
			centerPanel.remove(playerButton4);
			centerPanel.remove(playerLabels[3]);
			centerPanel.remove(player3Panel);
			centerPanel.remove(player3aPanel);
			centerPanel.remove(playerHealth[3]);
			centerPanel.invalidate();
			centerPanel.revalidate();
			centerPanel.repaint();

			centerPanel.add(playerButton1);
			centerPanel.add(playerLabels[0]);
			centerPanel.add(mainPlayerPanelC);
			centerPanel.add(mainPlayerPanelD);
			centerPanel.add(playerHealth[0]);
		}
		else if (index == 1){
			centerPanel.remove(playerButton1);
			centerPanel.remove(playerLabels[0]);
			centerPanel.remove(mainPlayerPanelC);
			centerPanel.remove(mainPlayerPanelD);
			centerPanel.remove(playerHealth[0]);
			
			centerPanel.remove(playerButton3);
			centerPanel.remove(playerLabels[2]);
			centerPanel.remove(player2Panel);
			centerPanel.remove(player2aPanel);
			centerPanel.remove(playerHealth[2]);
			
			centerPanel.remove(playerButton4);
			centerPanel.remove(playerLabels[3]);
			centerPanel.remove(player3Panel);
			centerPanel.remove(player3aPanel);
			centerPanel.remove(playerHealth[3]);
			centerPanel.invalidate();
			centerPanel.revalidate();
			centerPanel.repaint();

			centerPanel.add(playerButton2);
			centerPanel.add(playerLabels[1]);
			centerPanel.add(player1Panel);
			centerPanel.add(player1aPanel);
			centerPanel.add(playerHealth[1]);
		}
		else if (index == 2){
			centerPanel.remove(playerButton1);
			centerPanel.remove(playerLabels[0]);
			centerPanel.remove(mainPlayerPanelC);
			centerPanel.remove(mainPlayerPanelD);
			centerPanel.remove(playerHealth[0]);
			
			centerPanel.remove(playerButton2);
			centerPanel.remove(playerLabels[1]);
			centerPanel.remove(player1Panel);
			centerPanel.remove(player1aPanel);
			centerPanel.remove(playerHealth[1]);
			
			centerPanel.remove(playerButton4);
			centerPanel.remove(playerLabels[3]);
			centerPanel.remove(player3Panel);
			centerPanel.remove(player3aPanel);
			centerPanel.remove(playerHealth[3]);
			centerPanel.invalidate();
			centerPanel.revalidate();
			centerPanel.repaint();
			
			centerPanel.add(playerButton3);
			centerPanel.add(playerLabels[2]);
			centerPanel.add(player2Panel);
			centerPanel.add(player2aPanel);
			centerPanel.add(playerHealth[2]);
		}
		else if (index == 3){
			centerPanel.remove(playerButton1);
			centerPanel.remove(playerLabels[0]);
			centerPanel.remove(mainPlayerPanelC);
			centerPanel.remove(mainPlayerPanelD);
			centerPanel.remove(playerHealth[0]);
			
			centerPanel.remove(playerButton2);
			centerPanel.remove(playerLabels[1]);
			centerPanel.remove(player1Panel);
			centerPanel.remove(player1aPanel);
			centerPanel.remove(playerHealth[1]);
			
			centerPanel.remove(playerButton3);
			centerPanel.remove(playerLabels[2]);
			centerPanel.remove(player2Panel);
			centerPanel.remove(player2aPanel);
			centerPanel.remove(playerHealth[2]);
			centerPanel.invalidate();
			centerPanel.revalidate();
			centerPanel.repaint();
			
			centerPanel.add(playerButton4);
			centerPanel.add(playerLabels[3]);
			centerPanel.add(player3Panel);
			centerPanel.add(player3aPanel);
			centerPanel.add(playerHealth[3]);
		}
		}
	
	private void createGUI(){
		setSize(1300, 730);
		setResizable(false);
		setLocationRelativeTo(null);
		centerPanel.setLayout(null);
		panelC.setLayout(null);

		this.getRootPane().setDefaultButton(send);

				
		playerLabels[0].setPreferredSize(new Dimension(100, 10));
		Dimension b = playerLabels[0].getPreferredSize();
		playerLabels[1].setBounds(20, 20, b.width, b.height);
		playerLabels[2].setBounds(20, 20, b.width, b.height);
		playerLabels[3].setBounds(20, 20, b.width, b.height);
		playerLabels[0].setBounds(20, 20, b.width, b.height);

		Dimension c = select1.getPreferredSize();
		select1.setBounds(150, 20, c.width, c.height);
//		select2.setBounds(1000, 50, c.width, c.height);
//		select3.setBounds(1000, 80, c.width, c.height);
		
		playerButton1.setPreferredSize(new Dimension(63, 63));
		Dimension d = playerButton1.getPreferredSize();
		playerButton2.setBounds(80, 20, d.width, d.height);
		playerButton3.setBounds(80, 20, d.width, d.height);
		playerButton4.setBounds(80, 20, d.width, d.height);
		playerButton1.setBounds(80, 20, d.width, d.height);
		
//		Dimension d1 = mainPlayerButtonA[0].getPreferredSize();
		mainPlayerLabelsA[0].setPreferredSize(new Dimension(115, 80));
		mainPlayerPanelA.setPreferredSize(new Dimension(300, 125));
		mainPlayerPanelAA.setPreferredSize(new Dimension(200, 125));


		Dimension d1 = mainPlayerPanelA.getPreferredSize();
		mainPlayerPanelA.setBounds(850, 30, d1.width, d1.height);
		mainPlayerPanelB.setBounds(850, 285, d1.width, d1.height);

		Dimension d2 = mainPlayerPanelAA.getPreferredSize();
		mainPlayerPanelAA.setBounds(850, 155, d2.width, d2.height);
		mainPlayerPanelBB.setBounds(850, 410, d2.width, d2.height);
		
		centerPanel.add(mainPlayerPanelAA);
		centerPanel.add(mainPlayerPanelBB);


		diceButton.setPreferredSize(new Dimension(150, 120));
		Dimension eq = diceButton.getPreferredSize();
		diceButton.setBounds(545, 20, eq.width, eq.height);

		Dimension cb = cardBack.getPreferredSize();
		cardBack.setBounds(400, 20, cb.width, cb.height);
		cardFront.setBounds(700, 20, cb.width, cb.height);

		Dimension f = gameChat.getPreferredSize();
		scrollPane.setBounds(842, 555, f.width, f.height);
		centerPanel.add(scrollPane);
		
		Dimension f1 = gameChatField.getPreferredSize();
		gameChatField.setBounds(850, 661, f1.width, f1.height);
		
		Dimension f2 = send.getPreferredSize();
		send.setBounds(1200, 658, f2.width, f2.height);
		centerPanel.add(send);
		
		Dimension f3 = confirm.getPreferredSize();
		confirm.setBounds(1100, 500, f3.width, f3.height);
		centerPanel.add(confirm);
		Dimension f10 = cancel.getPreferredSize();
		cancel.setBounds(1100, 450, f10.width, f10.height);
		centerPanel.add(cancel);
		
		Dimension f4 = playerHealth[0].getPreferredSize();
		playerHealth[1].setBounds(30, 40, f4.width, f4.height);
		playerHealth[2].setBounds(30, 40, f4.width, f4.height);
		playerHealth[3].setBounds(30, 40, f4.width, f4.height);
		playerHealth[0].setBounds(30, 40, f4.width, f4.height);
		
		/*
		Dimension f5 = roundLabel.getPreferredSize();
		roundLabel.setBounds(400, 30, f5.width, f5.height);
		Dimension f6 = phaseLabel.getPreferredSize();
		phaseLabel.setBounds(400, 60, f6.width, f6.height);
		Dimension f7 = logLabel.getPreferredSize();
		logLabel.setBounds(400, 90, f7.width, f7.height);
		 */

		centerPanel.add(roundLabel);
		centerPanel.add(phaseLabel);
		centerPanel.add(logLabel);

		centerPanel.add(mainPlayerPanelA);
		centerPanel.add(mainPlayerPanelB);
		centerPanel.add(mainPlayerPanelC);
		centerPanel.add(mainPlayerPanelD);

		centerPanel.add(gameChatField);
		centerPanel.add(playerHealth[0]);
//		centerPanel.add(playerHealth[1]);
//		centerPanel.add(playerHealth[2]);
//		centerPanel.add(playerHealth[3]);

//		centerPanel.add(cardBack);
//		centerPanel.add(cardFront);
//		centerPanel.add(diceButton);
		centerPanel.add(select1);
//		centerPanel.add(select2);
//		centerPanel.add(select3);
		
		centerPanel.add(playerButton1);
//		centerPanel.add(playerButton2);
//		centerPanel.add(playerButton3);
//		centerPanel.add(playerButton4);


		centerPanel.add(playerLabels[0]);
//		centerPanel.add(playerLabels[1]);
//		centerPanel.add(playerLabels[2]);
//		centerPanel.add(playerLabels[3]);

		boardLabels[1][1].setIcon(playerPiece1);
		boardLabels[1][5].setIcon(playerPiece2);
		boardLabels[5][1].setIcon(playerPiece3);
		boardLabels[5][5].setIcon(playerPiece4);

		
		Dimension e = gridPanel.getPreferredSize();
		gridPanel.setBounds(400, 249, e.width, e.height);
		centerPanel.add(gridPanel);
		
		Dimension e1 = player1Panel.getPreferredSize();
		player1Panel.setBounds(30, 85, e1.width, e1.height);
//		centerPanel.add(player1Panel);
		player1aPanel.setBounds(200, 85, e1.width, e1.height);
//		centerPanel.add(player1aPanel);

		
		player2Panel.setBounds(30, 85, e1.width, e1.height);
//		centerPanel.add(player2Panel);
		player2aPanel.setBounds(200, 85, e1.width, e1.height);
//		centerPanel.add(player2aPanel);
		player3Panel.setBounds(30, 85, e1.width, e1.height);
//		centerPanel.add(player3Panel);
		player3aPanel.setBounds(200, 85, e1.width, e1.height);
//		centerPanel.add(player3aPanel);	
//		
		Dimension e5 = mainPlayerPanelC.getPreferredSize();
		mainPlayerPanelC.setBounds(30, 85, e1.width, e1.height);
		centerPanel.add(mainPlayerPanelC);		
		mainPlayerPanelD.setBounds(200, 85, e1.width, e1.height);
		centerPanel.add(mainPlayerPanelD);		
		
		add(centerPanel, BorderLayout.CENTER);
	}
	
	// FOR CLIENT
	public void setPlayerSpellCardLabel(int index1, String card) {
		int index2 = playerCountersA[index1];
		
		if(card.equals("comet")) {
			playerLabelsA[index1][index2].setIcon(comet);
		} else if(card.equals("earthquake")) {
			playerLabelsA[index1][index2].setIcon(earthquake);
		} else if(card.equals("fire_breath")) {
			playerLabelsA[index1][index2].setIcon(firebreath);
		} else if(card.equals("gust")) {
			playerLabelsA[index1][index2].setIcon(gust);
		} else if(card.equals("hailstorm")) {
			playerLabelsA[index1][index2].setIcon(hailstorm);
		} else if(card.equals("heal")) {
			playerLabelsA[index1][index2].setIcon(heal);
		} else if(card.equals("ice_spike")) {
			playerLabelsA[index1][index2].setIcon(icespike);
		} else if(card.equals("lightning")) {
			playerLabelsA[index1][index2].setIcon(lightning);
		} else if(card.equals("mage_rage")) {
			playerLabelsA[index1][index2].setIcon(magerage);
		} else if(card.equals("mist")) {
			playerLabelsA[index1][index2].setIcon(mist);
		} else if(card.equals("roots")) {
			playerLabelsA[index1][index2].setIcon(roots);
		} else if(card.equals("storm")) {
			playerLabelsA[index1][index2].setIcon(storm);
		} else if(card.equals("teleport")) {
			playerLabelsA[index1][index2].setIcon(teleport);
		} else if(card.equals("tornado")) {
			playerLabelsA[index1][index2].setIcon(tornado);
		} else if(card.equals("water_jet")) {
			playerLabelsA[index1][index2].setIcon(waterjet);
		}
		
		if(playerCountersA[index1] == 2) {
			playerCountersA[index1] = 0;
		} else {
			playerCountersA[index1]++;
		}
	}
	
	// FOR CLIENT
	public void setPlayerDecisionCardLabel(int index1, String card) {
		int index2 = playerCountersB[index1];
		
		if(card.equals("movefast")) {
			playerLabelsB[index1][index2].setIcon(movefast);
		} else if(card.equals("moveslow")) {
			playerLabelsB[index1][index2].setIcon(moveslow);
		} else if(card.equals("spell1")) {
			playerLabelsB[index1][index2].setIcon(spell1);
		} else if(card.equals("spell2")) {
			playerLabelsB[index1][index2].setIcon(spell2);
		} else if(card.equals("spell3")) {
			playerLabelsB[index1][index2].setIcon(spell3);
		}
		
		if(playerCountersB[index1] == 2) {
			playerCountersB[index1] = 0;
		} else {
			playerCountersB[index1]++;
		}
	}
	
	// FOR CLIENT
	public void setMainPlayerSpellCardLabel(String card) {
		int index = mainPlayerCounter;
		
		if(card.equals("comet")) {
			mainPlayerLabelsA[index].setIcon(comet);
		} else if(card.equals("earthquake")) {
			mainPlayerLabelsA[index].setIcon(earthquake);
		} else if(card.equals("fire_breath")) {
			mainPlayerLabelsA[index].setIcon(firebreath);
		} else if(card.equals("gust")) {
			mainPlayerLabelsA[index].setIcon(gust);
		} else if(card.equals("hailstorm")) {
			mainPlayerLabelsA[index].setIcon(hailstorm);
		} else if(card.equals("heal")) {
			mainPlayerLabelsA[index].setIcon(heal);
		} else if(card.equals("ice_spike")) {
			mainPlayerLabelsA[index].setIcon(icespike);
		} else if(card.equals("lightning")) {
			mainPlayerLabelsA[index].setIcon(lightning);
		} else if(card.equals("mage_rage")) {
			mainPlayerLabelsA[index].setIcon(magerage);
		} else if(card.equals("mist")) {
			mainPlayerLabelsA[index].setIcon(mist);
		} else if(card.equals("roots")) {
			mainPlayerLabelsA[index].setIcon(roots);
		} else if(card.equals("storm")) {
			mainPlayerLabelsA[index].setIcon(storm);
		} else if(card.equals("teleport")) {
			mainPlayerLabelsA[index].setIcon(teleport);
		} else if(card.equals("tornado")) {
			mainPlayerLabelsA[index].setIcon(tornado);
		} else if(card.equals("water_jet")) {
			mainPlayerLabelsA[index].setIcon(waterjet);
		} 
		
		if(mainPlayerCounter == 4) {
			mainPlayerCounter = 0;
		} else {
			mainPlayerCounter++;
		}
	}
	
	// FOR CLIENT
	public void setMainPlayerDecisionCardLabels() {
		mainPlayerLabelsB[0].setIcon(movefast);
		mainPlayerLabelsB[1].setIcon(moveslow);
		mainPlayerLabelsB[2].setIcon(spell1);
		mainPlayerLabelsB[3].setIcon(spell2);
		mainPlayerLabelsB[4].setIcon(spell3);
		
		for(int i = 0; i < 3; i++) {
			this.mainPlayerChosenA[i].setIcon(this.cardFrontImage);
			this.mainPlayerChosenB[i].setIcon(this.cardBackImage);
			this.player1aLabels[i].setIcon(this.cardFrontImage);
			this.player1Labels[i].setIcon(this.cardBackImage);
			this.player2aLabels[i].setIcon(this.cardFrontImage);
			this.player2Labels[i].setIcon(this.cardBackImage);
			this.player3aLabels[i].setIcon(this.cardFrontImage);
			this.player3Labels[i].setIcon(this.cardBackImage);
		}
	}
	
	// FOR CLIENT
	public void setPlayerNameLabel(int index, String name) {
		playerLabels[index].setText(name);
	}
	
	// FOR CLIENT
	public void setPlayerHealthLabel(int index, int health) {
		playerHealth[index].setIcon(healthLabel[health - 1]);
	}
	
	// FOR CLIENT
	public void setPlayerPosition(int index, int x, int y) {
		setPlayerPreviousPosition(index);
		boardLabels[x][y].setIcon(playerPieces[index]);
		if(index == 0) {
			player1x = x;
			player1y = y;
		} else if(index == 1) {
			player2x = x;
			player2y = y;
		} else if(index == 2) {
			player3x = x;
			player3y = y;
		} else if(index == 3) {
			player4x = x;
			player4y = y;
		}
	}
	
	// FOR CLIENT
	public void setPlayerDead(int index) {
		setPlayerPreviousPosition(index);
		if(index == 0) {
			confirm.setEnabled(false);
		}
	}
	
	// FOR CLIENT
	public void setPlayerPreviousPosition(int index) {
		if(index == 0){
			if((player1x == 1 && player1y == 1) || (player1x == 1 && player1y == 5) || (player1x == 5 && player1y == 1) || (player1x == 5 && player1y == 5)) {
				boardLabels[player1x][player1y].setIcon(podium);
			} else {
				boardLabels[player1x][player1y].setIcon(piece);
			}
		} else if(index == 1) {
			if((player2x == 1 && player2y == 1) || (player2x == 1 && player2y == 5) || (player2x == 5 && player2y == 1) || (player2x == 5 && player2y == 5)) {
				boardLabels[player2x][player2y].setIcon(podium);
			} else {
				boardLabels[player2x][player2y].setIcon(piece);
			}
		} else if(index == 2) {
			if((player3x == 1 && player3y == 1) || (player3x == 1 && player3y == 5) || (player3x == 5 && player3y == 1) || (player3x == 5 && player3y == 5)) {
				boardLabels[player3x][player3y].setIcon(podium);
			} else {
				boardLabels[player3x][player3y].setIcon(piece);
			}
		} else if(index == 3) {
			if((player4x == 1 && player4y == 1) || (player4x == 1 && player4y == 5) || (player4x == 5 && player4y == 1) || (player4x == 5 && player4y == 5)) {
				boardLabels[player4x][player4y].setIcon(podium);
			} else {
				boardLabels[player4x][player4y].setIcon(piece);
			}
		}
	}
	
	// FOR CLIENT
	public void showInputWindow(String card) {
		inputWindow = new MakeMovesGUI(card);
	}
	
	// FOR CLIENT
	public void promptUserForInput(String card) {
		if(card.equals("spell_cards")) {
			JOptionPane.showMessageDialog(GamePlayGUI.this, "Please select your Spell Cards (Blue Cards).", "Start Selection", JOptionPane.PLAIN_MESSAGE, null);
		}
		
		if(card.equals("decision_cards")) {
			JOptionPane.showMessageDialog(GamePlayGUI.this, "Please select your Decision Cards (Red Cards).", "Start Selection", JOptionPane.PLAIN_MESSAGE, null);
		}
	}
	
	public void promptUserTrialEnded() {
		JOptionPane.showMessageDialog(GamePlayGUI.this, "Your trial has ended. Please register to play some more!", "Trial Over", JOptionPane.PLAIN_MESSAGE, null);
	}
	
	// FOR CLIENT
	public void updatePhaseLabel(String message) {
		phaseLabel.setText(message);
	}
	
	public void updateRoundLabel(String message) {
		roundLabel.setText(message);
	}
	
	public void updateLogLabel(String message) {
		logLabel.setText(message);
	}
		
	public static void main (String [] args){
		Client c = new Client();;
		GamePlayGUI msg = new GamePlayGUI(c);
		msg.setVisible(true);
		msg.setPlayerSpellCardLabel(1, "hailstorm");
		msg.setPlayerDecisionCardLabel(1, "moveslow");
		msg.setMainPlayerSpellCardLabel("roots");
		msg.setMainPlayerSpellCardLabel("hailstorm");
		msg.setMainPlayerSpellCardLabel("gust");
		msg.setMainPlayerSpellCardLabel("mist");
		msg.setMainPlayerSpellCardLabel("icespike");
		msg.setMainPlayerDecisionCardLabels();
		msg.setPlayerNameLabel(1, "Christian");
		msg.setPlayerHealthLabel(0, 12);
		msg.setPlayerHealthLabel(1, 12);
		msg.setPlayerHealthLabel(2, 12);
		msg.setPlayerHealthLabel(3, 12);
		//msg.showInputWindow("mage_rage");
	}
	
	public class MakeMovesGUI extends JFrame{
		private static final long serialVersionUID = 1;
		private JComboBox<String> mageNames, car, dia, podium;
		private JComboBox<Integer> rowCoor, colCoor;
		private JButton confirm;
		private JLabel player, attack, r, c, teleport;
		private String card;


		public MakeMovesGUI(String card){
			super("Mage Rage - Spell Card Input");
			setSize(300, 200);
			setLocation(500, 250);
			setAlwaysOnTop(true);
			String [] mages = {"Purple mage", "Blue mage", "Green mage"};
			String [] cardinals = {"N", "S", "E", "W"};
			String [] diagonals = {"NW", "NE", "SW", "SE"};
			String [] podiums = {"Top Left", "Top Right", "Bottom Left", "Bottom Right"};
			
			this.card = card;

			Integer [] row = {0,1,2,3,4,5,6};
			Integer [] col = {0,1,2,3,4,5,6};
			
			confirm = new JButton("Confirm");
			player = new JLabel("Select a Mage:");
			attack = new JLabel("Select a Direction:");
			r = new JLabel("Select a Row Coordinate:");
			c = new JLabel("Select a Column Coordinate:");
			teleport = new JLabel("Select a Podium:");

			mageNames = new JComboBox<String>(mages);
			car = new JComboBox<String>(cardinals);
			dia = new JComboBox<String>(diagonals);
			podium = new JComboBox<String>(podiums);
			rowCoor = new JComboBox<Integer>(row);
			colCoor = new JComboBox<Integer>(col);
			
			JPanel mainPanel = new JPanel();
			JPanel jp = new JPanel();
			jp.setLayout(new BorderLayout());
			jp.add(confirm, BorderLayout.SOUTH);
			
			if(card.equals("movement_fast") || card.equals("movement_slow") || card.equals("storm") || card.equals("lightning")) {
				mainPanel.add(r);
				mainPanel.add(rowCoor);
				mainPanel.add(c);
				mainPanel.add(colCoor);
			} else if(card.equals("tornado") || card.equals("fire_breath")) {
				mainPanel.add(attack);
				mainPanel.add(car);
			} else if(card.equals("water_jet") || card.equals("comet")) {
				mainPanel.add(attack);
				mainPanel.add(dia);
			} else if(card.equals("roots")) {
				mainPanel.add(player);
				mainPanel.add(mageNames);
			} else if(card.equals("gust")) {
				mainPanel.add(player);
				mainPanel.add(mageNames);
				mainPanel.add(attack);
				mainPanel.add(car);
			} else if(card.equals("mage_rage")) {
				mainPanel.add(r);
				mainPanel.add(rowCoor);
				mainPanel.add(c);
				mainPanel.add(colCoor);
				mainPanel.add(attack);
				mainPanel.add(car);
			} else if(card.equals("teleport")) {
				mainPanel.add(teleport);
				mainPanel.add(podium);
			}
			
			add(mainPanel, BorderLayout.CENTER);
			add(jp, BorderLayout.SOUTH);

			createActions();
			setVisible(true);
		}
		
		
		private void createActions(){
			confirm.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ae){
					StringBuilder sb = new StringBuilder();
					
					if(MakeMovesGUI.this.card.equals("movement_fast") || MakeMovesGUI.this.card.equals("movement_slow") || MakeMovesGUI.this.card.equals("storm") || MakeMovesGUI.this.equals("lightning")) {
						sb.append(MakeMovesGUI.this.rowCoor.getSelectedItem());
						sb.append(MakeMovesGUI.this.colCoor.getSelectedItem());
						GamePlayGUI.this.userInput1 = sb.toString();
						
						int x = (int)MakeMovesGUI.this.rowCoor.getSelectedItem();
						int y = (int)MakeMovesGUI.this.colCoor.getSelectedItem();
						
						if(MakeMovesGUI.this.card.equalsIgnoreCase("movement_fast")) {
							if(Math.abs(x - GamePlayGUI.this.player1x) + Math.abs(y - GamePlayGUI.this.player1y) <= 2) {
								GamePlayGUI.this.c.clientGame.receivedUserInput();
								MakeMovesGUI.this.setVisible(false);
								MakeMovesGUI.this.dispose();
							} else {
								JOptionPane.showMessageDialog(GamePlayGUI.this, "Invalid Selection", "Please select valid coordinates!", JOptionPane.PLAIN_MESSAGE, null);
							}
						}
						
						if(MakeMovesGUI.this.card.equalsIgnoreCase("movement_slow")) {
							if(Math.abs(x - GamePlayGUI.this.player1x) + Math.abs(y - GamePlayGUI.this.player1y) <= 4) {
								GamePlayGUI.this.c.clientGame.receivedUserInput();
								MakeMovesGUI.this.setVisible(false);
								MakeMovesGUI.this.dispose();
							} else {
								JOptionPane.showMessageDialog(GamePlayGUI.this, "Invalid Selection", "Please select valid coordinates!", JOptionPane.PLAIN_MESSAGE, null);
							}
						}
						
						if(MakeMovesGUI.this.card.equalsIgnoreCase("lightning")) {
							if(Math.abs(x - GamePlayGUI.this.player1x) + Math.abs(y - GamePlayGUI.this.player1y) <= 3) {
								GamePlayGUI.this.c.clientGame.receivedUserInput();
								MakeMovesGUI.this.setVisible(false);
								MakeMovesGUI.this.dispose();
							} else {
								JOptionPane.showMessageDialog(GamePlayGUI.this, "Invalid Selection", "Please select valid coordinates!", JOptionPane.PLAIN_MESSAGE, null);
							}
						}
						
					} else if(card.equals("tornado") || card.equals("fire_breath")) {
						if(MakeMovesGUI.this.car.getSelectedItem().equals("N")) {
							GamePlayGUI.this.userInput1 = "n";
						} else if(MakeMovesGUI.this.car.getSelectedItem().equals("S")) {
							GamePlayGUI.this.userInput1 = "s";
						} else if(MakeMovesGUI.this.car.getSelectedItem().equals("E")) {
							GamePlayGUI.this.userInput1 = "e";
						} else if(MakeMovesGUI.this.car.getSelectedItem().equals("W")) {
							GamePlayGUI.this.userInput1 = "w";
						}
						
						GamePlayGUI.this.c.clientGame.receivedUserInput();
						MakeMovesGUI.this.setVisible(false);
						MakeMovesGUI.this.dispose();
					} else if(card.equals("water_jet") || card.equals("comet")) {
						if(MakeMovesGUI.this.car.getSelectedItem().equals("NW")) {
							GamePlayGUI.this.userInput1 = "nw";
						} else if(MakeMovesGUI.this.car.getSelectedItem().equals("NE")) {
							GamePlayGUI.this.userInput1 = "ne";
						} else if(MakeMovesGUI.this.car.getSelectedItem().equals("SW")) {
							GamePlayGUI.this.userInput1 = "sw";
						} else if(MakeMovesGUI.this.car.getSelectedItem().equals("SE")) {
							GamePlayGUI.this.userInput1 = "se";
						}
						
						GamePlayGUI.this.c.clientGame.receivedUserInput();
						MakeMovesGUI.this.setVisible(false);
						MakeMovesGUI.this.dispose();
					} else if(card.equals("roots")) {
						// TODO
						// might have to remove the "+ 1" from the index to sync up with server indices
						GamePlayGUI.this.userInput1 = Integer.toString(MakeMovesGUI.this.mageNames.getSelectedIndex() + 1);
						
						GamePlayGUI.this.c.clientGame.receivedUserInput();
						MakeMovesGUI.this.setVisible(false);
						MakeMovesGUI.this.dispose();
					} else if(card.equals("gust")) {
						GamePlayGUI.this.userInput1 = Integer.toString(MakeMovesGUI.this.mageNames.getSelectedIndex() + 1);

						if(MakeMovesGUI.this.car.getSelectedItem().equals("N")) {
							GamePlayGUI.this.userInput2 = "n";
						} else if(MakeMovesGUI.this.car.getSelectedItem().equals("S")) {
							GamePlayGUI.this.userInput2 = "s";
						} else if(MakeMovesGUI.this.car.getSelectedItem().equals("E")) {
							GamePlayGUI.this.userInput2 = "e";
						} else if(MakeMovesGUI.this.car.getSelectedItem().equals("W")) {
							GamePlayGUI.this.userInput2 = "w";
						}
						
						GamePlayGUI.this.c.clientGame.receivedUserInput();
						MakeMovesGUI.this.setVisible(false);
						MakeMovesGUI.this.dispose();
					} else if(card.equals("mage_rage")) {
						sb.append(MakeMovesGUI.this.rowCoor.getSelectedItem());
						sb.append(MakeMovesGUI.this.colCoor.getSelectedItem());
						GamePlayGUI.this.userInput1 = sb.toString();
						
						int x = (int)MakeMovesGUI.this.rowCoor.getSelectedItem();
						int y = (int)MakeMovesGUI.this.colCoor.getSelectedItem();
						
						if(Math.abs(x - GamePlayGUI.this.player1x) + Math.abs(y - GamePlayGUI.this.player1y) <= 2) {
							GamePlayGUI.this.c.clientGame.receivedUserInput();
							MakeMovesGUI.this.setVisible(false);
							MakeMovesGUI.this.dispose();
						} else {
							JOptionPane.showMessageDialog(GamePlayGUI.this, "Invalid Selection", "Please select valid coordinates!", JOptionPane.PLAIN_MESSAGE, null);
						}
						
						if(MakeMovesGUI.this.car.getSelectedItem().equals("N")) {
							GamePlayGUI.this.userInput2 = "n";
						} else if(MakeMovesGUI.this.car.getSelectedItem().equals("S")) {
							GamePlayGUI.this.userInput2 = "s";
						} else if(MakeMovesGUI.this.car.getSelectedItem().equals("E")) {
							GamePlayGUI.this.userInput2 = "e";
						} else if(MakeMovesGUI.this.car.getSelectedItem().equals("W")) {
							GamePlayGUI.this.userInput2 = "w";
						}
					
					} else if(card.equals("teleport")) {
						if(MakeMovesGUI.this.podium.getSelectedItem().equals("Top Left")) {
							GamePlayGUI.this.userInput1 = "11";
						} else if(MakeMovesGUI.this.podium.getSelectedItem().equals("Top Right")) {
							GamePlayGUI.this.userInput1 = "15";
						} else if(MakeMovesGUI.this.podium.getSelectedItem().equals("Bottom Left")) {
							GamePlayGUI.this.userInput1 = "51";
						} else if(MakeMovesGUI.this.podium.getSelectedItem().equals("Bottom Right")) {
							GamePlayGUI.this.userInput1 = "55";
						}
						
						GamePlayGUI.this.c.clientGame.receivedUserInput();
						MakeMovesGUI.this.setVisible(false);
						MakeMovesGUI.this.dispose();
					}					
				}
			});
					
		}
	}
	
	//Chat methods
	public void addRoomMessage(String username, String msg){
		gameChat.append("[ROOM]" + username + ": " + msg + "\n");
		
	}
	
	public void addPrivateMessage(String username, String msg){
		gameChat.append("[PM]" + username + ": " + msg + "\n");
	}
	
	
	public class showCard extends JFrame{

		private static final long serialVersionUID = 1;
		private JLabel item;
		private JPanel p;
		
		public showCard(ImageIcon i){
			super("Mage Rage - Card View");
			setSize(400, 400);
			setLocation(500, 250);
			p = new JPanel();
			
			
			try{
				BufferedImage image = ImageIO.read(new File("images/spells/roots.png"));
				item = new JLabel(new ImageIcon(image));
				p.add(item);
			} catch (IOException ex){
		    	   System.out.println("No image found");
			}
			
//			p.add(item);
			add(p, BorderLayout.CENTER);			
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setVisible(true);
		}
		
	}
	
}