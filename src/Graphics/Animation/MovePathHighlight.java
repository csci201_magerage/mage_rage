package Graphics.Animation;

import java.util.ArrayList;

import javax.swing.JLabel;

import Graphics.GUI.GamePlayGUI;

public class MovePathHighlight extends Animation{
	
	public enum Movement {slow, fast}
	private Movement movementType;
	private int playerX, playerY;
	
	public MovePathHighlight(GamePlayGUI g, Movement type, int x, int y){
		super(g);
		movementType = type;
		playerX = x;
		playerY = y;
	}
	
	public void runAnimation(){
		ArrayList<JLabel> highlighted;
		highlighted = getHighlights();
		
		for(int i=0;i<highlighted.size();i++){
			game.setTargeted(highlighted.get(i));
		}
		
		try{
			wait();
		}catch(InterruptedException e){
			System.err.println("InterruptedException: "+e.getMessage());
		}
		
		for(int i=0;i<highlighted.size();i++){
			game.setUntargeted(highlighted.get(i));
		}
	}
	
	public ArrayList<JLabel> getHighlights(){
		ArrayList<JLabel> highlights = new ArrayList<JLabel>();
		int x = playerX, y = playerY;
		
		if(movementType==Movement.slow){
			for(int i=x-4; i<x+5; i++){
				for(int j=(y-4+Math.abs(x-i));j<(y+5-Math.abs(x-i));j++){
					JLabel nextLabel = game.getBoardTile(i, j);
					if(nextLabel!=null){
						highlights.add(nextLabel);
					}
				}
			}
		}
		else if(movementType==Movement.fast){
			for(int i=x-2; i<x+3; i++){
				for(int j=(y-2+Math.abs(x-i));j<(y+3-Math.abs(x-i));j++){
					JLabel nextLabel = game.getBoardTile(i, j);
					if(nextLabel!=null){
						highlights.add(nextLabel);
					}
				}
			}
		}
		
		return highlights;
	}
	
	public void inputReceived(){
		notify();
	}

}
