package Graphics.Animation;

import java.util.ArrayList;

import Graphics.GUI.GamePlayGUI;

public class Animator implements Runnable{

	private static ArrayList<String> animRequests;
	private GamePlayGUI game;
	private static boolean stillAnimating;
	private ArrayList<Animation> runningAnims;
	
	public Animator(GamePlayGUI thisGame){
		game = thisGame;
		stillAnimating = true;
		animRequests = new ArrayList<String>();
	}
	
	public void startAnim(String animation){
		
	}
	
	public void requestAnim(String animation){
		synchronized(animRequests){
			animRequests.add(animation);
		}
		notify();
	}
	
	public static void close(){
		stillAnimating = false;
	}
	
	public void run(){
		while(stillAnimating){
			if(!animRequests.isEmpty()){
				startAnim(animRequests.get(0));
				synchronized(animRequests){
					animRequests.remove(0);
				}
			}
			else{
				try{
					wait();
				}catch(InterruptedException e){
					System.err.println("Animator InterruptedException: "+e.getMessage());
				}
			}
		}
	}
}
