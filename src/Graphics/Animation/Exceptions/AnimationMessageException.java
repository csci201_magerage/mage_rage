package Graphics.Animation.Exceptions;

public class AnimationMessageException extends Exception{

	String message;
	
	public AnimationMessageException(String incorrectMessage){
		super();
		message = incorrectMessage;
	}
	
	public String getMessage(){
		return "Animation message \""+message+"\" could not be understood.";
	}
}
