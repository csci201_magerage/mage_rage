package Graphics.Animation;

import Graphics.GUI.GamePlayGUI;

public abstract class Animation implements Runnable {

	GamePlayGUI game;
	
	public Animation(GamePlayGUI g){
		game = g;
	}
	
	public void run(){
		runAnimation();
	}
	
	public abstract void runAnimation();
}
