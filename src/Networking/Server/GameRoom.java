package Networking.Server;

import java.util.Vector;
//This the game room containing all the client threads, client does not have access to this.
public class GameRoom {
	private Vector<ClientThread> players;
	private Vector<ClientThread> spectators;
	private boolean active;
	
	
	public GameRoom(){
		active = false;
		players = new Vector<ClientThread>();
		spectators = new Vector<ClientThread>();
	}

	public void joinRoom(ClientThread newPlayer){
		players.add(newPlayer);
	}

	public void leaveRoom(ClientThread leaving){
		players.remove(leaving);
	}
	
	public void addSpectator(ClientThread toAdd){
		spectators.add(toAdd);
	}
	
	public void removeSpectator(ClientThread toRemove){
		spectators.remove(toRemove);
	}
	
	public int playerCount(){
		return players.size();
	}
	
	public Vector<ClientThread> getPlayers(){
		return players;
	}
	
	public boolean isActive(){
		return active;		
	}
	
	public void setActive(){
		active = true;
	}
	
	public void deActive(){
		active = false;
	}
	
	
}
