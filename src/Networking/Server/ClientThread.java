package Networking.Server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;


public class ClientThread extends Thread {

	private NetServer parent;
	private Socket s;
	private Object line;
	private boolean waiting;
	
	private String username;
	private boolean guestMode;
	private int roomIndex;
	
	private ObjectInputStream reader;
	private ObjectOutputStream sender;
	
	public ClientThread(Socket s, NetServer parent) {
		this.parent = parent;
		this.s = s;
		guestMode = false;
		waiting = true;
		line = null;
		try {
			reader = new ObjectInputStream(s.getInputStream());
			sender = new ObjectOutputStream(s.getOutputStream());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.start();
		
		
	}
	
	public String getUsername(){
		return username;
	}
	
	public void sendToClient(Object obj){
		try {
			sender.writeObject(obj);
			sender.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//sender.println();
	}
	
	public void setUsername(String str){
		this.username = str;
	}
	
	private void handShake(){
		sendToClient("treasure_tower_controls_river_monster");
	}
	
	public boolean isGuest()
	{
		return guestMode;
	}
	
	private void ClientLogin(String username, String password){		
		parent.Login(this, username, password);
		if (username.equals("guest") && password.equals("guest")){
			//For testing purpose treat guest as registered user
			guestMode = true;
		}	
	}
	
	private void ClientRegister(String username, String password, String email){
		if (parent.Register(username,password,email)){
			sendToClient("LOGININFO&&REGISTERSUCCESS");
		}else{
			sendToClient("LOGININFO&&REGISTERFAILED");
		}
	}
	
	private void toSendPublicMessage(String[] msg){
		String combinedMsg = "";
		for (int i=1;i<msg.length;i++){
			combinedMsg = combinedMsg + msg[i];
		}
		parent.sendMessageToClients(this, combinedMsg);
	}
	
	private void toSendPrivateMessage(String [] msg){
		String combinedMsg = "";
		for (int i=2;i<msg.length;i++){
			combinedMsg = combinedMsg + msg[i];
		}
		parent.sendMessageToClient(this,msg[1], combinedMsg);
		
	}
	
	private void toSendRoomMessage(String [] msg){
		String combinedMsg = "";
		for (int i=1;i<msg.length;i++){
			combinedMsg = combinedMsg + msg[i];
		}
		parent.sendMessageToRoom(this,roomIndex,combinedMsg);
	}
	
	public void sendGameInfoToPlayer(String msg){
		sendToClient("GAMEINFO&&"  + msg);
	}
	
	
	public void sendUserStats(String msg){
		System.out.println("USERDATA&&"  + msg);
		sendToClient("USERDATA&&"  + msg);
	}
	/*
	public void sendGameInfoToAll(Vector<ClientThread> players, String msg){
		String combinedMsg = "GAMEINFO&&"  + msg;
		for (ClientThread ctIter : players) {
				ctIter.sendToClient(combinedMsg);
		}
	}
	
	*/
	
	private void handleLine(String line){
		waiting = false;
		String [] subStr = line.split("&&");
		
		switch (subStr[0]){
		case "heaven_king_covers_ground_tiger":
			handShake();
			break;
		case "UP_SUBMIT":
			ClientLogin(subStr[1],subStr[2]);
			break;
		case "REGISTER":
			ClientRegister(subStr[1],subStr[2],subStr[3]);
			break;
		case "PUBLICMSG":
			toSendPublicMessage(subStr);
			break;
		case "PRIVATEMSG":
			toSendPrivateMessage(subStr);
			break;
		case "ROOMMSG":
			toSendRoomMessage(subStr);
			break;
		case "REQUESTUSERINFO":
			toSendUserInfo();
			break;
		case "REQUESTROOMLIST":
			toSendRoomList();
			break;
		case "REQUESTUSERLIST":
			toSendUserList(Integer.parseInt(subStr[1]));
			break;
		case "JOINROOM":
			toJoinRoom(subStr);
			break;
		case "CREATEROOM":
			toCreateRoom(subStr);
			break;
		case "LEAVEROOM":
			toLeaveRoom(subStr);
			break;
		case "GAMEINFO":
			toTrasferGameInfo(subStr);
			break;
		case "USERDATA":
			toGetUserData();
			break;
		case "UPDATETILES":
			if(!guestMode){
				updateTiles(subStr[1], Integer.parseInt(subStr[2]));
			}
			break;
		}
		
		waiting = true;
		
	}
	
	private void updateTiles(String id, int move){
		parent.dbUpdateTiles(id, move);
	}
	
	public void updateDamageTaken(int dam){
		parent.updateDamageTaken(getUsername(), dam);
	}
	
	private void toTrasferGameInfo(String[] subStr) {
		
		parent.transferGameInfo(subStr[1],roomIndex);
		
	}

	private void toJoinRoom(String[] subStr) {
		roomIndex = Integer.parseInt(subStr[1]);
		parent.joinRoom(this, roomIndex);
	}
	
	private void toCreateRoom(String[] subStr){
		int index = Integer.parseInt(subStr[1]);
		parent.createRoom(this, index);
	}
	
	private void toLeaveRoom(String[] subStr){
		int index = Integer.parseInt(subStr[1]);
		parent.leaveRoom(this,index);
	}

	public void toSendRoomList(){
		sendToClient(parent.getRoomInfo());
	}
	
	public void toSendUserList(int index) {
		sendToClient(parent.getUserList(this,index));
			
	}
	
	private void toSendUserInfo(){
		String combinedMsg = "USERINFO&&";
		combinedMsg = combinedMsg + this.username;
		sendToClient(combinedMsg);
	}
	
	private void toGetUserData(){
		parent.getStats(this);
	}

	
	public void run() {
		try {
			while (true) {
			if (waiting){
				try {
					line = reader.readObject();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}	
				if (line!=null && line instanceof String){
					if (username == null){
						System.out.println("Message from " + s.getInetAddress() + ":" + s.getPort()  + ": " + line);
					}else{
						System.out.println("Message from " + username + ": " + line);
					}
					handleLine((String)line);
				}
			}
			}
			
		} catch (IOException ioe) {
			if (username == null){
				System.out.println(s.getInetAddress() + ":" + s.getPort()  + " disconnected.");
			}else{
				System.out.println(username + " disconnected.");
			}
			parent.removeClientThread(this);
			
		}
	
	}
}