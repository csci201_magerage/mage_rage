package Networking.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Vector;

import Gameplay.Server.GameSession;
import Networking.Client.RoomInfo;
import Networking.Client.UserInfo;
import Networking.Database.DBLink;



public class NetServer {
	private static final int maxRoom =4;
	private Vector<ClientThread> allUsers = new Vector<ClientThread>();
	private Vector<ClientThread> lobbyUsers = new Vector<ClientThread>();
	private GameRoom[] roomList;
	private GameSession[] gameSessions = new GameSession[4];
	private DBLink db;
	
	public NetServer() {
		ServerSocket ss = null;
		try {
			System.out.println("Starting Mage Rage Server");
			ss = new ServerSocket(10086);
			db = new DBLink();
			//db = null;
			
			roomList = new GameRoom[maxRoom];
			for (int i=0;i<maxRoom;i++){
				roomList[i] = new GameRoom();
			}
			
			while (true) {
				System.out.println("Waiting for client to connect...");
				Socket s = ss.accept();
				System.out.println("Client " + s.getInetAddress() + ":" + s.getPort() + " connected");
				ClientThread ct = new ClientThread(s, this);
				
			}
		} catch (IOException ioe) {
			System.out.println("IOE: " + ioe.getMessage());
		} finally {
			if (ss != null) {
				try {
					ss.close();
				} catch (IOException ioe) {
					System.out.println("IOE closing ServerSocket: " + ioe.getMessage());
				}
			}
		}
	}
	public void removeClientThread(ClientThread ct) {
		allUsers.remove(ct);
		if (lobbyUsers.contains(ct)){
			lobbyUsers.remove(ct);
		}
		for (int i=0;i<maxRoom;i++){
			if (roomList[i].getPlayers().contains(ct)){
				roomList[i].leaveRoom(ct);
			}
		}
		refreshLobbyUserList(null);
	}
	
	public void sendMessageToClients(ClientThread ct, String str) {
		String combinedMsg = "PUBLICMSG&&" + ct.getUsername() + "&&" + str;
		if (lobbyUsers.contains(ct)){
		for (ClientThread ctIter : lobbyUsers) {
				if (!ct.equals(ctIter)) {
					ctIter.sendToClient(combinedMsg);
				}
			}
		}
	}
	
	public void sendMessageToRoom(ClientThread ct, int roomIndex, String str) {
		System.out.println(roomIndex);
		String combinedMsg = "ROOMMSG&&" + ct.getUsername() + "&&" + str;
		for (ClientThread ctIter : roomList[roomIndex].getPlayers()){
			System.out.println(ctIter.getUsername());
			if (!ct.equals(ctIter)){
				ctIter.sendToClient(combinedMsg);
			}
		}
	}
	
	public void sendMessageToClient(ClientThread ct, String target, String msg) {
		String combinedMsg = "PRIVATEMSG&&" + ct.getUsername() + "&&" + msg;
		for (ClientThread ctIter : allUsers) {
			if (target.equals(ctIter.getUsername())){
				ctIter.sendToClient(combinedMsg);
				return;
			}
		}
		ct.sendToClient("PRIVATEMSG&&[SYSTEM]&&"+ target + " is not online or is not a valid user.");
		
	}
		
	
	public void Login(ClientThread ct, String username, String password){
		if (username.equals("guest") && username.equals("guest")){
			ct.setUsername("Guest" + getRandomID());
			ct.sendToClient("LOGININFO&&SUCCESS");
			allUsers.add(ct);
			lobbyUsers.add(ct);
			refreshLobbyUserList(ct);
		}else{	
			boolean success = false;	
		
			success = db.checkUser(username);
			if (!success){
				ct.sendToClient("LOGININFO&&FAILED");
			}else{
				success = db.checkPassword(username, password);
				ct.sendToClient("LOGININFO&&SUCCESS");
				ct.setUsername(username);
				allUsers.add(ct);
				lobbyUsers.add(ct);
				refreshLobbyUserList(ct);
			}
		}	
	}

	public void refreshLobbyUserList(ClientThread requester){
		for (ClientThread ctIter : lobbyUsers){
				ctIter.toSendUserList(-1);
			}
	}
	
	private void refreshRoomUserList(int index) {
		for (ClientThread ctIter : roomList[index].getPlayers()){
			ctIter.toSendUserList(index);
		}
	}
	
	public void refreshRoomList(ClientThread requester){
		for (ClientThread ctIter : lobbyUsers){
			ctIter.toSendRoomList();
		}
	}
	
	public boolean Register(String username, String password, String email){
		boolean userExists = false;	
		
		userExists = db.checkUser(username);

		if (userExists){
			return false;
		}else{
			db.registerUser(username, password, email);
			return true;
		}
	}
	
	public int getRandomID(){
		Random rand = new Random();
		int randomNum = rand.nextInt(10000) + 1;
		return randomNum;
	}
	
	
	public RoomInfo[] getRoomInfo(){
		RoomInfo [] ris = new RoomInfo[maxRoom];
		
		for (int i=0;i<maxRoom;i++){
			ris[i] = new RoomInfo(roomList[i].playerCount(),roomList[i].isActive());
		}
		
		return ris;
		
	}
	
	public void joinRoom(ClientThread ct, int index){
		if (!roomList[index].getPlayers().contains(ct)){
			if (roomList[index].isActive()){
				ct.sendToClient("LOBBYINFO&&ROOMACTIVE");
			}else if (roomList[index].playerCount() == 4){
				ct.sendToClient("LOBBYINFO&&ROOMFULL");
			}else{
				ct.sendToClient("LOBBYINFO&&SUCCESS");
				lobbyUsers.remove(ct);
				roomList[index].joinRoom(ct);
				refreshRoomUserList(index);
				refreshRoomList(ct);
				refreshLobbyUserList(ct);
		}
		}
		checkStartGame(index);
	}
	
	
	private void checkStartGame(int index) {
		/*2 for testing*/
		if (roomList[index].playerCount() == 4){
			for (ClientThread ctIter : roomList[index].getPlayers()){
				ctIter.sendToClient("LOBBYINFO&&GAMESTART");
			}
			
			System.out.println(roomList[index].getPlayers().get(0).getUsername());
			System.out.println(roomList[index].getPlayers().get(1).getUsername());
						
			gameSessions[index] = new GameSession(roomList[index].getPlayers());
		}
		
	}
	public void createRoom(ClientThread ct, int index) {
		if (!roomList[index].getPlayers().contains(ct)){
			if (roomList[index].playerCount() != 0){
				ct.sendToClient("LOBBYINFO&&ROOMEXISTS");
			}else{
				ct.sendToClient("LOBBYINFO&&SUCCESS");
				roomList[index].joinRoom(ct);
			}
		}
	}

	
	public void leaveRoom(ClientThread ct, int index) {
		roomList[index].leaveRoom(ct);
		lobbyUsers.add(ct);
		refreshRoomList(ct);
		refreshLobbyUserList(ct);
		refreshRoomUserList(index);
	}
	
	
	public void transferGameInfo(String gameInfo, int roomIndex) {
	
		gameSessions[roomIndex].messageFromClient = gameInfo;
	}
	
	public UserInfo[] getUserList(ClientThread ct, int roomIndex) {
		if (roomIndex==-1){
			return generateUserList(lobbyUsers);
		}else{
			return generateUserList(roomList[roomIndex].getPlayers());
		}
	}
	
	public UserInfo[] generateUserList(Vector<ClientThread> ctV){
		UserInfo[] userList = new UserInfo[ctV.size()];
		for (int i=0;i<ctV.size();i++){
			userList[i] = new UserInfo(ctV.get(i).getUsername());
		}
		return userList;
	}
	
	
	public void getStats(ClientThread ct) {
		ct.sendUserStats(db.getUserStats(ct.getUsername()));
	}
	
	public void dbUpdateTiles(String id, int move){
		int pID = db.returnUser(id);
		db.updateTilesMoved(pID, move);
	}

	public void updateDamageTaken(String username, int damage){
		db.updateDamage(db.returnUser(username), damage, false);
	}
	///////////Main for test
	public static void main(String [] args){
		new NetServer();
	}
	



}

	



