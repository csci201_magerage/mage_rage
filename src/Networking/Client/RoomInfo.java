package Networking.Client;

import java.io.Serializable;

//This class only contains the basic information about game rooms for display purpose. Both client and server have accces.
public class RoomInfo implements Serializable {
	private static final long serialVersionUID = -1501728558126544670L;
	private boolean active;
	private int playerCount; 
	
	public RoomInfo(int players, boolean active){
		this.playerCount = players;
		this.active = active;
	}
	
	public boolean isActive(){
		return active;
	}
	
	public int getPlayerCount(){
		return playerCount;
	}
	
	public String toString(){
		return "Players: " + playerCount + " Active: " + active;
	}
	
	
}
