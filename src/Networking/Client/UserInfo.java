package Networking.Client;

import java.io.Serializable;

public class UserInfo implements Serializable{
	private static final long serialVersionUID = 7810477578159637831L;
	private String username;
	
	public UserInfo(String username){
		this.username = username;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public String toString(){
		return "Username:" + this.username;
	}
}
