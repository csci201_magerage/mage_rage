package Networking.Client;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import Gameplay.Client.Client;

public class NetClient extends Thread{
	private Socket s;
	private Object line;
	private boolean active;
	
	private String username;
	private boolean guestMode;
	
	private ObjectInputStream reader;
	private ObjectOutputStream sender;
	
	private boolean waiting;
	private String infoStr;
	
	//User information
	private RoomInfo[] roomList;
	private UserInfo[] userList;
	
	Client c;
	
	private int roomIndex;
	
	public NetClient(Client c){
		this.c = c;
		String hostAdd = null;
		try{
			FileReader fr = new FileReader("serverAddress.txt");
			BufferedReader br = new BufferedReader(fr);
			hostAdd = br.readLine();
			br.close();
		}catch(IOException e){
			e.printStackTrace();
			System.exit(0);
		}		
		guestMode = false;
		
		try {
			s = new Socket(hostAdd,10086);
			active = false;

			sender = new ObjectOutputStream(s.getOutputStream());
			reader = new ObjectInputStream(s.getInputStream());
			//initialize values
			roomList = null;
			userList = null;
			
			//handshake
			sender.writeObject("heaven_king_covers_ground_tiger");
			sender.flush();
			next();
			if (line!=null && line.equals("treasure_tower_controls_river_monster")){
				active= true;
			}
			waiting = true;
			roomIndex = -1;
			this.start();
			
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public void next(){
		try {
			
			line = reader.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void run(){	
		try{
			while(active){
				if (waiting){
					next();
					if (line instanceof String){
						System.out.println("Message from server: " + line);
						handleLine((String)line);
					}
					if (line instanceof RoomInfo[]){
						receiveRoomList((RoomInfo[])line);
					}
					if (line instanceof UserInfo[]){
						receiveUserList((UserInfo[])line);
					}
				}
			}
		}finally{
			closeConnection();
		}
	
	}
	


	public String getUsername(){
		return username;
	}
	
	public boolean isGuest(){
		return guestMode;
		
	}
	
	public boolean isActive(){
		return active;
	}
	

	
	public void sendToServer(String str){
		try {
			sender.writeObject(str);
			sender.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Sending 
	public void sendUP(String user,String pass){
		username = null;
		sendToServer("UP_SUBMIT&&" + user + "&&" + pass);
		if (user.equals("guest") && pass.equals("guest")){
			guestMode = true;
		}
		
		while (this.username == null){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if (this.username.equals("SUCCESS")){
			requestUserInfo();
			requestRoomList();
			c.gotoLobbyWindow();
			c.lobbyWindow.updateAll();
		}else{
			c.loginWindow.showFail();
		}
		
	}
	
	public void sendReg(String usr, String password, String email){
		this.username = null;
		sendToServer("REGISTER&&" + usr + "&&" + password + "&&" + email);
	
		while (this.username == null){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if (this.username.equals("REGISTERSUCCESS")){
			c.registrationWindow.registerSuccess();
		}else{
			c.registrationWindow.showFailDialog();
		}
	}
	
	public void sendTilesMoved(String id, int move){
		sendToServer("UPDATETILES&&" + id + "&&" + move);
	}

	public void sendPublicMessage(String msg){
		c.lobbyWindow.addPublicMessage(this.username,msg);
		sendToServer("PUBLICMSG&&" + msg);
	}

	public void sendPrivateMessage(String target, String msg){
		c.lobbyWindow.addPrivateMessage(this.username,msg);
		sendToServer("PRIVATEMSG&&" + target + "&&" + msg);
		
	}
	
	public void sendRoomMessage(String msg){
		c.gameWindow.addRoomMessage(this.username, msg);
		sendToServer("ROOMMSG&&" + msg);
	}
	
	public void sendGameInfo(String msg){
		sendToServer("GAMEINFO&&" + msg);
	}
	
	public void requestUserInfo(){
		sendToServer("REQUESTUSERINFO");
	}
	
	public void requestRoomList(){
		
		sendToServer("REQUESTROOMLIST&&");
	}
	
	public void requestUserList(){
		sendToServer("REQUESTUSERLIST&&" + roomIndex);
	}
	
	public String joinRoom(int index){
		this.roomIndex = index;
		infoStr = null;
		sendToServer("JOINROOM&&" + index);
		
		while (infoStr == null){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//sendToServer("JOINROOM&&" + index);
		}
		return infoStr;
	}
	
	public String createRoom(int index) {
		infoStr = null;
		sendToServer("CREATEROOM&&" + index);
		
		while (infoStr == null){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			sendToServer("CREATEROOM&&" + index);
		}
		return infoStr;
	}
	
	public void leaveRoom(int index){
		sendToServer("LEAVEROOM&&" + index);
	}
	
	public void requestUserData(){
		sendToServer("USERDATA");
	}
	
	//Receiving 
	private void handleLine(String str){
		String [] subStr = str.split("&&");
		switch (subStr[0]){
		case "LOGININFO":
			receiveLoginMessage(subStr[1]);
			break;
		case "PUBLICMSG":
			receivePublicMessage(subStr);
			break;
		case "PRIVATEMSG":
			receivePrivateMessage(subStr);
			break;
		case "ROOMMSG":
			receiveRoomMessage(subStr);
			break;
		case "USERINFO":
			receiveUserInfo(subStr);
			break;	
		case "LOBBYINFO":
			receiveLobbyInfo(subStr);
			break;
		case "USERDATA":
			receiveUserData(subStr[1]);
			break;
		case "GAMEINFO":	
			c.clientGame.receiveMessage(subStr[1]);
			break;
		}
	}
	
	private void receiveUserData(String str){
		c.lobbyWindow.addLineToDialog(str);
	}
	
	private void receiveLoginMessage(String  str){
		username = str;
	}
	
	private void receiveLobbyInfo(String[] subStr){
		infoStr = subStr[1];
		if (infoStr.equals("GAMESTART")){
			if (!c.isGameStarted()){
				c.lobbyWindow.startGame();
			}else{
				c.GameStart();
			}	
		}
	}
	
	private void receivePublicMessage(String[] msg){
		String combinedMsg = "";
		for (int i=2;i<msg.length;i++){
			combinedMsg = combinedMsg + msg[i];
		}
		if (c.lobbyWindow!=null){
		c.lobbyWindow.addPublicMessage(msg[1],combinedMsg);
		}
	}

	private void receivePrivateMessage(String[] msg){
		String combinedMsg = "";
		for (int i=2;i<msg.length;i++){
			combinedMsg = combinedMsg + msg[i];
		}
		if (c.lobbyWindow!=null && c.lobbyWindow.isVisible()){
			c.lobbyWindow.addPrivateMessage(msg[1],combinedMsg);
		}else{
			c.gameWindow.addPrivateMessage(msg[1],combinedMsg);
		}
	}
	
	private void receiveRoomMessage(String [] msg){
		String combinedMsg = "";
		for (int i=2;i<msg.length;i++){
			combinedMsg = combinedMsg + msg[i];
		}
		if (c.gameWindow!=null){
			c.gameWindow.addRoomMessage(msg[1],combinedMsg);
		}
		
	}
	
	private void receiveUserInfo(String[] msg){
		this.username = msg[1];
		
	}

	private void receiveRoomList(RoomInfo[] roomList) {
		this.roomList = roomList;
		/*
		System.out.println("Room List");
		for (int i=0;i<roomList.length;i++){
			System.out.println(roomList[i]);
		}
		*/
		if (c.lobbyWindow!=null){
			c.lobbyWindow.updateRoomListGUI(roomList);
		}
	}

	private void receiveUserList(UserInfo[] userList) {
		this.userList = userList;
		/*
		for (int i=0;i<userList.length;i++){
			System.out.println(userList[i]);
		}
		*/
		if (c.lobbyWindow!=null){
			c.lobbyWindow.updateUserListGUI(userList);
		}
	}
	
	public void closeConnection(){
		System.out.println("Closing connection");
		active = false;
		waiting = false;
		try{
			if (s!=null){
				s.close();
			}
			if (reader!=null){
				reader.close();
			}
			if (sender!=null){
				sender.close();
			}
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
	}

	
}
