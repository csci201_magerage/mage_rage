package Networking.Client;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

//this class will simulate the behavior of sending stuff from a GUI
public class NetClientTest extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private NetClient nc;
	private JTextField jtf;
	private JButton send;
	
	public NetClientTest(){
		setSize(300,200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		nc = new NetClient(null);
		
		
		jtf = new JTextField();
		send = new JButton("Send");
		add(jtf,BorderLayout.CENTER);
		add(send,BorderLayout.SOUTH);
		this.setVisible(true);
		
		send.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				nc.sendToServer(jtf.getText());
			}			
		});
		
		
		nc.sendUP("guest", "guest");
		nc.joinRoom(0);
	}
	
	
	public static void main(String [] args){
		new NetClientTest();
	}
	
}
