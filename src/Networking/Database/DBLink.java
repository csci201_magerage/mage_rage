package Networking.Database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBLink {
	private Connection conn;
	private PreparedStatement ps;
	private ResultSet rs;
	
	
	public DBLink(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//address, username, password
			conn = DriverManager.getConnection("jdbc:mysql://localhost/mage_rage","mage","Niconiconi");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e){
			e.printStackTrace();
		}
		
	}
	
	//Checks if the user exists on the database
	public boolean checkUser(String username){
		boolean result = false;
		try {
			ps = conn.prepareStatement("SELECT * FROM wp_users WHERE user_login=?");
			ps.setString(1, username);
			rs = ps.executeQuery();
			result = rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	//Gets the password from the player database
	private String getHashedPassword(String username) {
		String hashed = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM wp_users WHERE user_login=?");
			ps.setString(1, username);
			rs = ps.executeQuery();
			rs.next();
			hashed =  rs.getString("user_pass");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
			return hashed;
	}
	
	//Calls the hash function to hash the given password
	public String hashPassword(String password){
		String hashPass = null;
		String url = "http://localhost/phppass.php?plain=" + password;
		try{
			URL phppass = new URL(url);
			BufferedReader reader = new BufferedReader(new InputStreamReader(phppass.openStream()));
			hashPass = reader.readLine();
			reader.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return hashPass;
	}
	
	
	//Password is verified by running a slightly modified version of wordpress/include/class-phppass.php
	//install wordpress and copy the php file into the root folder
	//will have it implemented on a webserver so that we can test it without installing a lot of stuff
	public boolean checkPassword(String username, String password){
		String url = "http://localhost/phppass.php?plain=" + password + "&hashed=" + getHashedPassword(username);
		//System.out.println(url);
		String inputLine = null;
		try{
		URL phppass = new URL(url);
        BufferedReader reader = new BufferedReader(new InputStreamReader(phppass.openStream()));
        inputLine = reader.readLine();
        
        reader.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println(inputLine);
		if (inputLine != null && inputLine.equals("ok")){
    		return true;
    	}else{
    		return false;
    	}
	}
	
	//Returns the unique ID of the specified username in the player.
	//ID is what references the player in the stat database.
	public int returnUser(String username){
		int id = 0;;	
		try{
			ps=conn.prepareStatement("SELECT ID FROM wp_users WHERE user_login =? ");
			ps.setString(1, username);
			rs=ps.executeQuery();
			rs.next();
			id = rs.getInt("ID");
			//System.out.println(id);
		}catch (SQLException e){
			e.printStackTrace();
		}
		return id;
	}
	
	//this adds one game played to the database for specified userID and adds a win or loss depending on bool
	public void updateGamesPlayed(int id, boolean won){
		try{
			ps=conn.prepareStatement("Select * FROM wp_user_stats WHERE user_ID =?");
			ps.setInt(1, id);
			rs = ps.executeQuery();
			rs.next();
			int played = rs.getInt("games_played");
			played++;
			ps = conn.prepareStatement("UPDATE wp_user_stats set games_played = " + played +" Where user_ID = " + id);
			ps.executeUpdate();		
			if(won){
				played = rs.getInt("games_won");
				played++;
				ps = conn.prepareStatement("UPDATE wp_user_stats set games_won = " + played + " Where user_ID =" + id);
				ps.executeUpdate();
			}else{
				played = rs.getInt("games_lost");
				played++;
				ps = conn.prepareStatement("UPDATE wp_user_stats set games_lost = " + played + " Where user_ID =" + id);
				ps.executeUpdate();
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	//calculates winrate, will return a float.
		public float calculateWinrate(int id){
			float rate = 0.f;
			//System.out.println(rate);
			try{
				ps=conn.prepareStatement("SELECT * FROM wp_user_stats WHERE user_ID=?");
				ps.setInt(1, id);
				rs= ps.executeQuery();
				rs.next();
				float win = rs.getInt("games_won");
				float total = rs.getInt("games_played");
				if(win== 0 || total == 0){
					rate = 0;
				}else{
				rate = win/total;
				//System.out.println(win);
				//System.out.println(total);
				rate= rate * 100;
				//System.out.println(rate);
				//checking for format
			//	System.out.printf("%.2f", rate);
			//	System.out.print("%");
				}
			}catch (SQLException e){
				e.printStackTrace();
			}
			return rate;
		}
	
	//Use this method to keep track of how many tiles a player moves for database tracking
	public void updateTilesMoved(int id, int move){
		try{
			ps = conn.prepareStatement("Select* FROM wp_user_stats WHERE user_ID = ?");
			ps.setInt(1, id);
			rs = ps.executeQuery();
			rs.next();
			int moved = rs.getInt("tiles_moved");	
			moved+=move;
			ps = conn.prepareStatement("UPDATE wp_user_stats set tiles_moved = " + moved + " WHERE user_ID = " + id);
			ps.executeUpdate();
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	//Registers a Player
	public void registerUser(String username, String password, String email ){
		try{
			ps = conn.prepareStatement("INSERT INTO wp_users(user_login, user_email, user_pass) Values(?,?,?)");
			ps.setString(1,username);
			//System.out.println(password);
			ps.setString(2, email);
			String hashedPass = hashPassword(password);
			//System.out.println(hashedPass);
			ps.setString(3, hashedPass);
			ps.executeUpdate();
			int user = returnUser(username);
			ps = conn.prepareStatement("INSERT INTO wp_user_stats(user_ID) Values(?)");
			ps.setInt(1,user);
			ps.executeUpdate();
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	//updates the total damage dealt and taken fields of stat database for user
	public void updateDamage(int id, int damage, boolean dealt ){
		
		try{	
			ps=conn.prepareStatement("Select * FROM wp_user_stats WHERE user_ID =" + id);
			rs = ps.executeQuery();
			rs.next();
			int dbDamage;
			if(dealt){
				dbDamage=rs.getInt("dam_dealt");
				dbDamage+= damage;
				ps = conn.prepareStatement("UPDATE wp_user_stats set dam_dealt = " + dbDamage + " Where user_ID = " + id);
				ps.executeUpdate();
			}
			else{
				dbDamage = rs.getInt("dam_taken");
				dbDamage += damage;
				ps= conn.prepareStatement("UPDATE wp_user_stats set dam_taken = " + dbDamage + " Where user_ID = " + id);
				ps.executeUpdate();
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public String getUserStats(String username){
		int id = returnUser(username);
		String result = null;
		try{	
			ps = conn.prepareStatement("SELECT * FROM wp_user_stats WHERE user_ID =" + id );
			rs = ps.executeQuery();
			if(rs.next()){
				result = username + "\n\nGames Played: ";
				result+= rs.getInt("games_played");
				result+= "\nGames Won: " + rs.getInt("games_won");
				result+= "\nGames Lost: " + rs.getInt("games_lost");
				result+= "\nWin Rate: " + String.format("%.2f", calculateWinrate(id)) + "%";
				result+= "\nDamage Dealt To Other Players: " + rs.getInt("dam_dealt");
				result+= "\nDamage Taken From Other Players: " + rs.getInt("dam_taken");
				result += "\nTotal Tiles Moved: " + rs.getInt("tiles_moved");
				return result;
			}else{
				ps = conn.prepareStatement("INSERT INTO wp_user_stats(user_ID) Values(?)");
				ps.setInt(1,id);
				ps.executeUpdate();
				result = getUserStats(username);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return result;
	}
	//For DB Testing Purposes	
/*
	public void test(){
		//System.out.println(checkPassword("http://localhost/phppass.php?plain=" + "201mages." + "&hashed=" + getHashedPassword("mage")));
	//	registerUser("al", "al","al");
		//System.out.println(getUserStats("y"));
		//updateDamage(returnUser("Hen"), 3, true);
	//	System.out.println(hashPassword("Hen"));
		//System.out.println(checkPassword("your", "is"));
		//updateTilesMoved(2, 5);
		//System.out.println( checkUser("H"));
	//	registerUser("y","y","y");
		
	}
	
	
	//Database link test
	public static void main (String [] args){
		DBLink dl = new DBLink();
		dl.test(); 
	} 
*/	
}
